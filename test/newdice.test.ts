const isNumber = (val: string): boolean => /^\d+$/.test(val);
const signInt = (sign: string): number => {
    if (sign === '-') {
        return -1;
    }
    return 1;
};

const rollRegex = /(\d{1,5})d(\d{1,10})/i;
const parenRegex = /(\(.+\))/i;
const expRegex = /(\d{1,7})\^(\d{1,3})/i;
const multRegex = /(\d+)([*\/])(\d+)/i;
const addRegex = /(\d+)([+-])(\d+)/i;

function parseEntireRoll(roll: string): { result: number, rolls: number[] } {

}

function resolveParen(str: string): string {

}

function resolveExp(str: string): string {

}

function resolveMulDiv(str: string): string {

}

function resolveAddSub(str: string): string {

}