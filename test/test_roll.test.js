import { getEntryFromRollTable, rollDiceToInt, rollDiceToString } from '../src/util/diceutil';
import DragonsBreath from '../src/classes/weather/supernatural/DragonsBreath';
import { ClimateProps } from '../src/classes/weather/WeatherBaselines';
import { WindType } from '../src/classes/weather/wind/AbstractBaseWind';

const JSON = require('circular-json');

test('should roll random dice', () => {

    console.log(rollDiceToString('2d20+3+6d60'));
    let regex = /^(\d{1,3})(-(\d{1,3}))?$/;
    console.log(regex.exec('2-70'));
    console.log(regex.exec('100'));
    let snow = new DragonsBreath();
    for (let i = 0; i < 300; i++) {
        getEntryFromRollTable(ClimateProps[0].temp_variance, '1d100');
        //console.log('');
    }
});

test('should pick from a key-value object', () => {
    const wind_table = {
        '1-50': WindType.LIGHT,
        '51-80': WindType.MODERATE,
        '81-90': WindType.STRONG,
        '91-95': WindType.SEVERE,
        '96-100': WindType.WINDSTORM,
    };
    console.log(`Table Output: ${JSON.stringify(getEntryFromRollTable(wind_table, '1d100'), null, 4)}`);
});

test('should correctly return an array object', () => {
    const array_table = {
        '1-50': [WindType.LIGHT],
        '51-80': [WindType.MODERATE],
        '81-90': [WindType.STRONG],
        '91-95': [WindType.SEVERE],
        '96-100': [WindType.WINDSTORM],
    };
    let result = getEntryFromRollTable(array_table, '1d100');
    console.log(`Is Array: ${Array.isArray(result)}`);
    expect(result instanceof Array).toBe(true);
});

test('should roll decimal dice', () => {
    console.log(rollDiceToString('2.2d1.3'));
});

test('should roll negative', () => {
    expect(rollDiceToInt('-1d10') < 0).toBe(true);
});