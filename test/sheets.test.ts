import { close, connect } from '../src/db/database';
import SheetAPI from '../src/db/SheetAPI';
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import JSON from 'circular-json';
import Profile from '../src/db/schema/profiles';
import Encounter from '../src/db/schema/encounter';
import { now } from 'moment';

const testDatabase = async () => {
    await connect()
        .catch(err => {
            console.error(err);
        });

    await Profile.deleteOne({ username: 'Abs0rbed' });
    const profile = new Profile({
        discord: '173923836054470656',
        username: 'Abs0rbed',
    });
    await profile.save()
        .then(result => {
            console.log(result);
        });

    console.log(`Has enjin for 446704418600124427: ${ profile.hasEnjin('446704418600124427') }`);
    profile.guilds.push({
        guild: '446704418600124427',
        enjin: '4360912',
    });

    await profile.save()
        .then(result => console.log(result));
    console.log(`Has enjin for 446704418600124427: ${ profile.hasEnjin('446704418600124427') }`);
    console.log(`Enjin ID: ${ profile.getEnjin('446704418600124427') }`);

    await profile.getProfileData('4360912');
    await profile.addPoints('446704418600124427', 6);

    const encounterDoc = {
        type: 0,
        guild: '446704418600124427',
        channel: '448518932358365185',
        members: [],
        startTime: now(),
        lastMessage: now(),
    };

    await Encounter.deleteOne({
        guild: '446704418600124427',
        channel: '448518932358365185',
    });

    const encounter = new Encounter(encounterDoc);
    encounter.members.push({
        userId: '81976043287609344',
        joinedAt: now() + 2000,
        inactiveHours: 0,
    });
    encounter.members.push({
        userId: '173923836054470656',
        joinedAt: now() + 2000,
        inactiveHours: 0,
    });
    await encounter.save();

    const otherE = await Encounter.findOne({
        guild: '446704418600124427',
        channel: '448518932358365185',
        members: { $elemMatch: { id: '81976043287609344' } },
    });
    if (otherE) {
        console.log('Found inserted doc');
    }
    console.log(otherE);

    console.log('Closing DB Connection');
    await close();
};

// eslint-disable-next-line no-undef
test('Test Sheets', async () => {
    const sheetApi = new SheetAPI('1h2gvgZdmsIj7DPWAEajiEhqqcC3Sstj4fuQevMS5k-U');
    await sheetApi.getItems()
        .then(async res => {
            console.log('Got Results');
            console.log(JSON.stringify(res, null, 4));

            // await testSubmit();
            // await testDatabase();
        })
        .catch(console.error);
});

