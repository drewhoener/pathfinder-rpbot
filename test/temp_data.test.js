import { ClimateType, Elevation, Times } from '../src/classes/weather/WeatherBaselines';
import TempData from '../src/classes/weather/climate/TemperatureData';

test('Test Temp Chain', () => {
	let data = new TempData(ClimateType.TEMPERATE, Elevation.SEA);
	let temp = data.getTemp(Times.DUSK);
	let temp2 = data.getTemp(Times.DUSK);
	expect(temp).toBe(temp2);
});