import superData from './supernatural.json';
import fs from 'fs';
import cheerio from 'cheerio';

const data = fs.readFileSync('./test/gen/data.html').toString('utf8');
const path = './src/classes/weather/supernatural/';

test('Create Supernatural', () => {
    const $ = cheerio.load(data);
    console.log('Loaded Data');
    for (const weatherType in superData.supernatural) {
        // console.log(weatherType);
        for (const superType of superData.supernatural[weatherType]) {
            // console.log(`${superType.name.replace(/\s+/g, '')}.js`);
            const className = superType.name.replace(/[\s+']/g, '');
            const filePath = `${ path }${ className }.ts`;
            // console.log(superType);
            // console.log(`console.log("Hello World");{}()`);
            let weatherProp = `WeatherType.${ weatherType.toUpperCase() }`;
            let props = '';
            if (weatherType.toLowerCase() === 'windy') {
                weatherProp = 'WeatherType.CLEAR';
                props += 'this._windType = WindType.MODERATE;\n';
            }
            for (const elem in superType) {
                if (elem === 'name') {
                    continue;
                }
                const processed = /^\d+$/.test(superType[elem]) ? superType[elem] : `\'${ superType[elem] }\'`;
                props += `this.${ elem } = ${ processed };\n`;
            }

            const descr = [];
            $(`#desc-${ superType.name.toLowerCase().replace(/\s/g, '_').replace(/'/g, '') }`).children('p').each((id, elem) => {
                let text = $(elem).text();
                console.log(text);
                text = text.slice(text.indexOf(':') + 1);
                console.log(text);
                text = text.replace(/(?:\r\n|\r|\n|\t)/gm, ' '); // Replace Extra long spaces, new lines, tabs with a single space
                text = text.replace(/\s+/g, ' ');// Replace long spaces
                text = text.replace(/'/g, '\\\'');// Escape single quote
                descr.push(`'${ text.trim() }'`);
            });

            fs.writeFileSync(filePath,
                `import SpecialWeather from '../precipitation/SpecialWeather';
                    import { ISpecialWeather } from '../../types/weather';
                    import { WeatherType, WindType } from '../WeatherConstants';
					
					const description = [${ descr }];
					
					class ${ className } extends SpecialWeather implements ISpecialWeather{
					constructor(windType: WindType, temp: number){
					super(${ weatherProp }, '${ superType.name.replace(/'/g, '\\\'') }', description, windType);
					${ props }
					this.enabled = false;
					}
					}
					
					export default ${ className };`,
            );
        }
    }
});

