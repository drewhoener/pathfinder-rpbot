import img from './images.json';
import supernatural from './supernatural.json';
import { describe } from 'mocha';

const https = require('https');
const fs = require('fs');
const expect = require('chai').expect;

const path = './src/img/weather/';

describe('Fetch Images', () => {
	it('Should fetch all images related to the weather', () => {
		let url = 'https://donjon.bin.sh/d20/weather/images/';
		let num = 0;
		img.image_list.forEach((val, index) => {
			if (!val.image) {
				return;
			}
			setTimeout(() => {
				console.log(`Downloading image ${val.image}`);
				const file = fs.createWriteStream(`${path}${val.image}`);
				file.on('open', (fd) => {
					https.get(`${url}${val.image}`, res => res.pipe(file));
					num++;
				});
			}, 1500 * index);
		});
		Object.keys(supernatural.supernatural).forEach((keyVal) => {
			supernatural.supernatural[keyVal].forEach((val, index) => {
				if (!val.image) {
					return;
				}
				setTimeout(() => {
					console.log(`Downloading image ${val.image}`);
					const file = fs.createWriteStream(`${path}${val.image}`);
					file.on('open', (fd) => {
						https.get(`${url}${val.image}`, res => res.pipe(file));
						num++;
					});
				}, 1500 * index);
			});
		});
		console.log(`Downloaded ${num} file(s)`);
	});
});

