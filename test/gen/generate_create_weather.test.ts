// @ts-nocheck
import super_data from './supernatural.json';
import { capitalize } from '../../src/util/utils';
import fs from 'fs';
import _ from 'lodash';
import cheerio from 'cheerio';

const data = fs.readFileSync('./test/gen/data.html').toString('utf8');
const path = './src/classes/weather/';

/*
///////////
This is probably bad code, I'm sorry
Just something quick and dirty to generate my classes from a giant HTML file
///////////
 */


const createWind = false;
const createWeather = true;
const createTemp = true;

const weatherNames = _.flatMap(Object.values(super_data.supernatural)
    .map((cur) => Object.values(cur)
        .map(o => o.name)))
    .map(name => name.toLowerCase().replace(/\s/g, '_').replace(/'/g, ''));
const prefixRegex = /(prec|desc|wind|temp)-(\w+)/;

const createWindClass = ($, elem, className) => {
    const arr: string[] = [];
    $(elem).children('p').each((id, e) => {
        let text = $(e).text();
        text = text.slice(text.indexOf(':') + 1);
        // Replace Extra long spaces, new lines, tabs with a single space
        text = text.replace(/(?:\r\n|\r|\n|\t)/gm, ' ');
        // Replace long spaces
        text = text.replace(/\s+/g, ' ');
        // Escape single quote
        text = text.replace(/'/g, '\\\'');
        arr.push(`'${ text.trim() }'`);
    });
    fs.writeFileSync(`${ path }wind/${ className }.ts`,
        `import { IWindBase } from '../../types/weather';
        import { WindType } from '../WeatherConstants';
        import AbstractBaseWind from './AbstractBaseWind';
		const description = [${ arr }];
		
		class ${ className } extends AbstractBaseWind {
			constructor(){
				super(WindType.${ className.toUpperCase().replace(/wind/i, '') }, description);
			}
		}
		
		export default ${ className };`,
    );
};

const create_precip = ($, elem, className) => {
    const arr = [];
    $(elem).children('p').each((id, e) => {
        let text = $(e).text();
        text = text.slice(text.indexOf(':') + 1);
        // Replace Extra long spaces, new lines, tabs with a single space
        text = text.replace(/(?:\r\n|\r|\n|\t)/gm, ' ');
        // Replace long spaces
        text = text.replace(/\s+/g, ' ');
        // Escape single quote
        text = text.replace(/'/g, '\\\'');
        arr.push(`'${ text.trim() }'`);
    });
    fs.writeFileSync(`${ path }precipitation/${ className }.ts`,
        `import AbstractBasePrecip from './AbstractBasePrecip';
import { doesRollSucceed, rollDiceToInt } from '../../../util/diceutil';
import { getWeatherInstance } from '../../factory/WeatherCache';
import { WeatherType, WindType } from '../WeatherConstants';
		
		const description = [${ arr }];
		
		class ${ className } extends AbstractBasePrecip {
			constructor(windType: WindType, temp: number){
				super(WeatherType.${ className.toUpperCase().replace(/precip/i, '') }, description, windType);
				this.imageName = '${ className.toLowerCase().replace(/precip/i, '') }';
			}
		}
		
		export default ${ className };`,
    );
};

// eslint-disable-next-line no-undef
test('Create Weather', () => {
    const $ = cheerio.load(data);
    console.log('Loaded Data');
    const arr: string[] = [];
    $('div').children('div').each((id, elem) => {
        if (!$(elem).attr('id')) {
            console.log('no userId');
            return;
        }
        const match = prefixRegex.exec($(elem).attr('id'));
        const prefix = match[1];
        const name = match[2];
        const capitalName = capitalize(name.split('_'));
        if (weatherNames.find(i => i === name)) {
            return;
        }
        arr.push($(elem).attr('id'));
        switch (prefix) {
            case 'wind':
                if (createWind) {
                    createWindClass($, elem, `Wind${ capitalName.replace(/\s/, '') }`);
                }
                break;
            case 'desc':
            case 'prec':
                if (createWeather) {
                    create_precip($, elem, `Precip${ capitalName.replace(/\s/, '') }`);
                }
                break;
            case 'temp':
                break;
        }
    });
    console.log(arr);
});

