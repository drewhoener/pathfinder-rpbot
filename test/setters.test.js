class Tester {
	constructor() {
		console.log(`In constructor`);
		this.a = 0;
	}

	get thing() {
		console.log(`In getter field`);
		return this.a;
	}

	set thing(val) {
		console.log(`In setter field with value ${val}`);
		this.a = val;
	}
}

test('Test setter values', () => {
	const test = new Tester();
	expect(test['thing']).toBe(0);
	test['thing'] = 100;
	expect(test['thing']).toBe(100);
});