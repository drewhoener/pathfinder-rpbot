import { describe } from 'mocha';
import moment from 'moment';
import { calc_hours } from '../src/commands/end.js';

const expect = require('chai').expect;

describe('calc_hours()', () => {
	it('should calculate hours with varying times', () => {

		const now = moment();
		const values = [
			now.clone().valueOf(),
			now.clone().add(2, 'm').valueOf(),
			now.clone().add(5, 'm').valueOf(),
			now.clone().add(15, 'm').valueOf(),
			now.clone().add(30, 'm').valueOf(),
			now.clone().add(40, 'm').valueOf(),
			now.clone().add(55, 'm').valueOf(),
			now.clone().add(75, 'm').valueOf(),
			now.clone().add(76, 'm').valueOf(),
			now.clone().add(80, 'm').valueOf(),
			now.clone().add(82, 'm').valueOf(),
			now.clone().add(85, 'm').valueOf(),
			now.clone().add(100, 'm').valueOf(),
			now.clone().add(220, 'm').valueOf(),
			now.clone().add(230, 'm').valueOf(),
			now.clone().add(235, 'm').valueOf(),
			now.clone().add(240, 'm').valueOf(),
		];

		let hours = calc_hours(values);
		console.log(hours);
		expect(1).to.be.equal(1);
	});
});