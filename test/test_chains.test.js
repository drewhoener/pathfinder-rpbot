import Forecast from '../src/classes/weather/climate/Forecast';
import logger from '../src/util/logging';
import { getWeatherInstance, populateWeatherCache } from '../src/classes/factory/WeatherCache';
import AbstractBasePrecip from '../src/classes/weather/precipitation/AbstractBasePrecip';
import {
    ClimateType,
    CloudType,
    PrecipIntensity,
    Times,
    WeatherType,
    WindType,
} from '../src/classes/weather/WeatherConstants';
import { ClientContext } from '../src/handler/ClientContext';
import { ElevationData } from '../src/classes/weather/WeatherBaselines';
import PrecipThunderstorm from '../src/classes/weather/precipitation/PrecipThunderstorm';

const JSON = require('circular-json');
const _ = require('lodash');

test('Chains Equal', async () => {
    await populateWeatherCache();
    let weather = new Forecast(ClientContext, ClimateType.TEMPERATE, ElevationData.SEA);
    let chainOne = weather.getOrGeneratePrecip(Times.DAWN, false);
    console.log(``);
    logger.info('Testing duration the first time');
    expect(chainOne.duration).toBeGreaterThan(0);
    let chainTwo = weather.getOrGeneratePrecip(Times.DAWN);
    logger.info('Testing duration the second time');
    expect(chainOne.duration).toBeGreaterThan(0);
    logger.info(JSON.stringify(chainOne, null, 4));
    logger.info(JSON.stringify(chainTwo, null, 4));
    expect(chainOne.duration).toBeGreaterThan(0);
    for (let i = 0; i < 10; i++) {
        console.log(``);
    }
});

test('Chains Output', () => {
    const chain = new PrecipThunderstorm(WindType.SEVERE, 62)
        .apply({
            weatherType: WeatherType.THUNDERSTORM,
            weatherIntensity: PrecipIntensity.HEAVY,
            durationHours: 1,
            clouds: CloudType.OVERCAST,
            imageName: 'thunderstorm',
        });
    console.log(chain.getEmbedDescription());
    for (const val of chain.getEmbedDescription()) {
        console.log(val.value);
    }
});

test('New Chains', async () => {
    await populateWeatherCache();
    console.log('Creating new forecast instance');
    const weather = new Forecast(ClientContext, ClimateType.TEMPERATE, ElevationData.SEA);
    console.log('Getting precip setup data');
    const template = weather.getPrecipSetupData(Times.DAWN);
    console.log(template);
    const windType = AbstractBasePrecip.rollGenericWindType();
    console.log(`Generating base chain from generator`);
    const chain = getWeatherInstance(template.weatherType, windType, weather.getTemperature(Times.DAWN), weather.intensity);
    console.log(JSON.stringify(chain, null, 4));
});