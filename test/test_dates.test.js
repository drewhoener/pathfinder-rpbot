import { getSeasonFromDate } from '../src/classes/weather/WeatherBaselines';
import moment from 'moment';
import { Season } from '../src/classes/weather/WeatherConstants';

test('Test Dates', () => {
    let season = getSeasonFromDate();
    console.log(season);
    expect(season).toBe(Season.SPRING);

    season = getSeasonFromDate(moment('09-23-2018', 'MM-DD-YYYY'));
    expect(season).toBe(Season.FALL);
    season = getSeasonFromDate(moment('12-18-2018', 'MM-DD-YYYY'));
    expect(season).toBe(Season.FALL);

    season = getSeasonFromDate(moment('12-19-2018', 'MM-DD-YYYY'));
    expect(season).toBe(Season.WINTER);
    season = getSeasonFromDate(moment('12-21-2018', 'MM-DD-YYYY'));
    expect(season).toBe(Season.WINTER);

    season = getSeasonFromDate(moment('03-19-2019', 'MM-DD-YYYY'));
    expect(season).toBe(Season.SPRING);
    season = getSeasonFromDate(moment('06-20-2019', 'MM-DD-YYYY'));
    expect(season).toBe(Season.SPRING);

    season = getSeasonFromDate(moment('06-21-2019', 'MM-DD-YYYY'));
    expect(season).toBe(Season.SUMMER);
    season = getSeasonFromDate(moment('09-22-2019', 'MM-DD-YYYY'));
    expect(season).toBe(Season.SUMMER);
});