import { describe } from 'mocha';
import Forecast from '../src/classes/weather/climate/Forecast';
import { ClimateType, Elevation, PrecipIntensity, WeatherType } from '../src/classes/weather/WeatherBaselines';
import { assignField } from '../src/util/utils';

const _ = require('lodash');
const expect = require('chai').expect;
const JSON = require('circular-json');

const test_assign = (type, obj) => {
	Object.assign(obj, { weather_type: type });
};

describe('Test Weather', () => {
	it('Should test weather object creation', () => {
		let weather = new Forecast(ClimateType.TEMPERATE, Elevation.SEA);
		assignField(weather, '_baseIntensity', 100);
		expect(weather.intensity).to.be.equal(PrecipIntensity.TORRENTIAL);

		let new_weather = Forecast.fromJSON(JSON.stringify(weather));
		let edited = _.reduce(new_weather, function(result, value, key) {
			return _.isEqual(value, weather[key]) ? result : result.concat({ [key]: value });
		}, []);
		console.log(JSON.stringify(edited, null, 4));
		expect(_.isEqual(new_weather, weather)).to.be.equal(true);

		let sea_weather = new Forecast(ClimateType.TEMPERATE, Elevation.SEA);
		let lo_weather = new Forecast(ClimateType.TEMPERATE, Elevation.LOW);
        expect(_.isEqual(sea_weather, lo_weather)).to.be.equal(false);
        sea_weather.elevation = Elevation.LOW;
        //expect(sea_weather).to.be.equal(lo_weather);
        expect(_.isEqual(sea_weather, lo_weather)).to.be.equal(true);

        sea_weather.climateType = ClimateType.COLD;
        expect(_.isEqual(sea_weather, lo_weather)).to.be.equal(false);
        lo_weather.climateType = ClimateType.COLD;
        expect(_.isEqual(sea_weather, lo_weather)).to.be.equal(true);

        console.log(`Should Generate Precip: ${sea_weather.willHavePrecipitation()}`);
        sea_weather.generatePrecipChain();

        console.log(JSON.stringify(sea_weather, null, 4));

    });

	it('should assign objects properly', () => {
		let weather = {
			weather_type: WeatherType.RAIN,
		};
		test_assign(WeatherType.SNOW, weather);
		console.log(JSON.stringify(weather));
		expect(WeatherType.SNOW).to.be.equal(weather.weather_type);
	});
});