// eslint-disable-next-line no-undef
import SheetAPI from '../src/db/SheetAPI';
import { ClientContext } from '../src/handler/ClientContext';
import JSON from 'circular-json';
import logger from '../src/util/logging';

const loadConfig = async () => {
    // console.log(process.cwd());
    return ClientContext.config.loadConfig('./src/config.json')
        .then((partials) => {
            logger.info('Loaded the following partial config data:');
            logger.info(JSON.stringify(partials, null, 4));
        })
        .then(() => ClientContext.config.loadObjects())
        .catch(err => {
            logger.error(err);
            process.exit(-1);
        });
}

test('Test Creating Sheet', async () => {

    await loadConfig();
    // console.log(ClientContext.config);
    const sheetApi = new SheetAPI('1JLCBf-JlIwnBnUQYcPAac_n4rEiGiYGnwR8bTakzkOw', ClientContext);
    await sheetApi.createSheet('Testing Sheet')
        .then(async res => {
            if (res) {
                console.log(res);
                await sheetApi.setData('Testing Sheet!A5', [['Boo Motherfucker', '2'], ['Boo Hoo', 'you', 'too']])
            }
        })
        .catch(console.error);
});