// eslint-disable-next-line no-undef
import { Collection } from 'discord.js';
import Command from '../src/classes/base/Command';
import fs from 'fs';
import path from 'path';
import { FILE_REGEX } from '../src/util/utils';
import { WindType } from '../src/classes/weather/WeatherConstants';

// eslint-disable-next-line no-undef
test('Test Collections', async () => {
    const commands = new Collection<string, Command>();
    const commandFiles = fs.readdirSync(path.resolve(__dirname, '../src/commands')).filter(file => FILE_REGEX.test(file));
    console.log(__dirname);
    const failed = [];
    console.log(commandFiles);
    for (const file of commandFiles) {
        try {
            const { default: DynamicCommand } = await import(`../src/commands/${ file }`);
            console.log(`Imported file ${ file }`);
            const commandInstance: Command = new DynamicCommand();

            commands.set(commandInstance.name.toLowerCase(), commandInstance);
            console.log(`Registered command: ${ commandInstance.name }`);
        } catch (err) {
            failed.push(file);
            console.log(err);
        }
    }
    commands.forEach((val, key, map) => {
        console.log(`Key: ${ key }`);
        console.log('Value:');
        console.log(val);
    });

    console.log(`Registered: ${ commands.keyArray() }`);
    console.log(`Failed Commands: ${ failed }`);
    console.log(WindType[0]);
    const type: WindType = WindType.LIGHT - 1;
    console.log(WindType.LIGHT - 1);
    console.log(type);
});