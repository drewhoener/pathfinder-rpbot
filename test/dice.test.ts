import { getEntryFromRollTable } from '../src/util/diceutil';

const diceTable = {
    '1-10': 'A',
    '11-20': 'B',
    '21-30': 'C',
    '31-40': 'D',
    '41-50': 'E',
    '51-60': 'F',
    '61-70': 'G',
    '71-80': 'H',
    '81-90': 'I',
    '91-1000': 'J',

};

test('Test Dice', () => {
    console.log(getEntryFromRollTable(diceTable, '1d100'));
});