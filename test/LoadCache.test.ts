import { populateWeatherCache } from '../src/classes/factory/WeatherCache';

test('Load all weather elements', async () => {
    await populateWeatherCache();
});