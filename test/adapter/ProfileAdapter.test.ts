import { connect } from '../../src/db/database';
import { disconnect } from 'mongoose';
import fs from 'fs';
import readline from 'readline';
import path from 'path';
import Profile from '../../src/db/schema/profiles';
import Encounter from '../../src/db/schema/encounter';
import History, { IUserPlay } from '../../src/db/schema/history';
import { ObjectId } from 'mongodb';
import JSON from 'circular-json';
import moment from 'moment';

const shouldAdaptProfiles = false;
const shouldAdaptEncounters = false;
const shouldAdaptHistory = true;
const shouldAdaptSettings = false;

async function adaptProfiles() {
    const fileStream = fs.createReadStream(path.resolve('/opt/discord-bot/data/profile_backup.json'));
    const readLine = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    for await (const line of readLine) {
        const jsonObj = JSON.parse(line);
        console.log('--------------');
        console.log(jsonObj);
        const profile = new Profile({
            _id: new ObjectId(jsonObj._id['$oid']),
            discord: jsonObj.discord,
            username: jsonObj.username
        });
        Object.keys(jsonObj.guilds).forEach(key => {
            profile.guilds.push({
                guild: key,
                enjin: jsonObj.guilds[key],
            })
        });
        console.log(profile);
        console.log('--------------');
        await profile.save();
    }
}

async function adaptEncounters() {
    const fileStream = fs.createReadStream(path.resolve('/opt/discord-bot/data/encounter_backup.json'));
    const readLine = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    for await (const line of readLine) {
        const jsonObj = JSON.parse(line);
        console.log('--------------');
        console.log(JSON.stringify(jsonObj, null, 4));
        const encounter = new Encounter({
            _id: jsonObj._id['$oid'],
            type: jsonObj.type['$numberInt'],
            guild: jsonObj.guild,
            channel: jsonObj.channel,
            startTime: jsonObj.start_time['$numberLong'],
            lastMessage: jsonObj.last_message['$numberLong'],
        });
        for (const member of jsonObj.members) {
            encounter.members.push({
                userId: member.id,
                joinedAt: member.joinedAt['$numberLong'],
                inactiveHours: jsonObj.hour_mod['$numberInt'],
                ignoreInResults: false
            })
        }
        console.log(JSON.stringify(encounter, null, 4));
        console.log('--------------');
        await encounter.save();
    }
}

async function adaptHistory() {
    const fileStream = fs.createReadStream(path.resolve('/opt/discord-bot/data/history_backup.json'));
    const readLine = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    for await (const line of readLine) {
        const jsonObj = JSON.parse(line);
        console.log('--------------');
        console.log(JSON.stringify(jsonObj, null, 4));
        const history = new History({
            _id: jsonObj._id['$oid'],
            channel: jsonObj.channel,
            guild: jsonObj.guild,
            channelName: jsonObj.channelName
        });
        for (const date of jsonObj.dates) {
            const users: IUserPlay[] = [];
            const stamp = parseInt(date.stamp['$numberLong']);
            let time = stamp;
            if (date.hasOwnProperty('time')) {
                time = parseInt(date.time['$numberLong']);
            }
            const dateStr = moment(Date.parse(date.date));
            Object.entries(date).forEach(([key, value]) => {
                if (key === 'time' || key === 'date' || key === 'stamp') {
                    return;
                }
                // @ts-ignore
                const times = value['$numberDouble'];
                if (times < 0) {
                    console.log(`Skipping ${ key }/${ times }`);
                    return;
                }
                users.push({
                    userId: key,
                    time: times
                });
            });
            history.dates.push({
                members: users,
                stamp,
                date: dateStr.toString(),
                time
            });

        }
        console.log(JSON.stringify(history, null, 4));
        console.log('--------------');
        await history.save();
    }
}

async function adaptData() {
    await connect();
    console.log(`Connected`);

    console.log(__dirname);
    if (shouldAdaptProfiles) {
        console.log('Adapting Profiles');
        await adaptProfiles();
    }
    if (shouldAdaptEncounters) {
        console.log('Adapting Encounters');
        await adaptEncounters();
    }
    if (shouldAdaptHistory) {
        console.log('Adapting History');
        await adaptHistory();
    }

    await disconnect();
}

test('Adapt Profiles', async () => {
    await adaptData();
}, 20000);
