class A {
	thing(a, b, c = false) {
		throw new Error('Implement');
	}
}

class B extends A {
	thing(a, b, c) {
		console.log(a);
		console.log(b);
		console.log(c);
	}
}

test('Test Inheritance', () => {
	const obj = new B();
	obj.thing('Hello', 'World');
});