module.exports = {
    'presets': [
        ['@babel/preset-env', { 'targets': { 'node': 'current' } }],
        ['minify', { 'builtIns': false, 'mangle': false }],
        '@babel/preset-typescript',
    ],
    'plugins': [
        '@babel/plugin-proposal-class-properties',
    ],
};