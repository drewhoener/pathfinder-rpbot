import WeatherCommand from '../classes/base/WeatherCommand';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import { WindType } from '../classes/weather/WeatherConstants';
import { getWindInstance } from '../classes/factory/WeatherCache';
import logger from '../util/logging';

class CommandSummonWind extends WeatherCommand {

    constructor() {
        super({
            name: 'SummonWind',
            commandName: CommandNames.SUMMONWIND,
            description: 'Changes the type of wind',
            usage: '!summonwind [wind type]',
            preventDM: true,
            helpCategory: HelpCategory.NONE,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (message.author.id !== '173923836054470656') {
            logger.info('Bad user for summon wind');
            return;
        }
        const type: WindType = parseInt(args[0]);
        if (!type) {
            logger.info(`Wind Name: ${ WindType[type] }`);
            return;
        }

        const holder = WeatherCommand.getWeatherHolder();
        if (!holder.weather.currentPrecipitation) {
            logger.info('No current precipitation');
            return;
        }
        holder.weather.currentPrecipitation.windInstance = getWindInstance(type);
        holder.sendEmbedCallback(false, false);
    }

}

export default CommandSummonWind;