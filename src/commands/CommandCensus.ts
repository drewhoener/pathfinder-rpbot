import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, IRawCharacterSheet, Reacts } from '../classes/types/context';
import { Message, MessageAttachment, Snowflake } from 'discord.js';
import { MW_API_TIME, requestMythSheetsBulk } from '../util/mythweaversutil';
import moment from 'moment';
import { writeToBuffer } from '@fast-csv/format';
import { keys } from 'ts-transformer-keys';
import logger from '../util/logging';
import fs from 'fs';
import { promisify } from 'util';
import { getSettings } from '../db/schema/settings';
import SheetAPI from '../db/SheetAPI';
import { CharacterSheet, ICharacterSheet } from '../db/schema/charactersheets';

const writeFile = promisify(fs.writeFile);

class CommandCensus extends Command {

    static CUR_USER: Snowflake | undefined;
    public lastTime: number;
    private roles: Snowflake[] = [];

    constructor() {
        super({
            name: 'Census',
            commandName: CommandNames.CENSUS,
            description: 'Takes a census. Debug Only',
            usage: '!census',
            preventDM: true,
            helpCategory: HelpCategory.NONE,
        });
        this.lastTime = -1;
        this.roles = [
            '446705480367079446',
            '747970767081308161',
            '448642903531847680',
            '560464175680192512',
            '448620296136687616',
        ];
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        if (!message.member) {
            return;
        }
        const member = message.member;
        if (!message.member.hasPermission('ADMINISTRATOR') && !this.roles.some(role => member.roles.cache.has(role))) {
            return message.channel.send('You don\'t have permission to request census data!');
        }
        if (CommandCensus.CUR_USER) {
            return message.channel.send('Currently busy, please try again later!');
        }

        if (message.guild && args && args.length >= 2 && args[0].toLowerCase() === 'data') {
            const settings = await getSettings(message.guild.id);
            if (settings) {
                settings.censusData = args[1];
                await settings.save();
                await message.react(Reacts.CHECK);
                return;
            }
        }

        let datasheetApiId: SheetAPI | undefined;
        if (message.guild) {
            datasheetApiId = await getSettings(message.guild.id)
                .then(settings => {
                    return settings?.censusData ? new SheetAPI(settings.censusData) : undefined;
                })
                .catch((e) => {
                    logger.error(e);
                    return undefined;
                });
        }

        const approvedSheets: ICharacterSheet[] = await CharacterSheet.find({}).catch(() => []);
        if (!approvedSheets || !approvedSheets.length) {
            return message.channel.send('There aren\'t any sheets to take a census from!');
        }
        const duration = moment.duration(Math.max(MW_API_TIME, approvedSheets.length * MW_API_TIME));

        CommandCensus.CUR_USER = message.author.id;
        const time = new Date().valueOf();
        const sheetURLs: string[] = approvedSheets.map((entry: ICharacterSheet) => entry.mythweavers);
        requestMythSheetsBulk(sheetURLs)
            .then(async (results) => {
                if (!results.length) {
                    const reply = 'An Error occurred generating census data, no results can be produced. Please try again later.';
                    return Promise.all([
                        message.channel.send(reply),
                        message.author.send(reply),
                    ]);
                }

                // Add in Discord Names
                // noinspection DuplicatedCode
                results.forEach(entry => {
                    const userSheet: ICharacterSheet | undefined = approvedSheets.find((sheet: ICharacterSheet) => sheet.mythweavers === entry.URL);
                    if (!userSheet) {
                        return;
                    }
                    entry.DiscordName = userSheet.playerName;
                });

                const rawKeys = keys<IRawCharacterSheet>();
                const bufferData = await writeToBuffer(results, {
                    headers: rawKeys,
                });

                const date = moment().format('YYYY-MM-DD-HH-mm');

                // Write to spreadsheet
                if (datasheetApiId) {
                    try {
                        const newSheetId = await datasheetApiId.createSheet(date);
                        if (newSheetId !== undefined) {
                            await datasheetApiId.setData(`${ date }!A1`, [rawKeys]);
                            const rawData = results.map((entry: IRawCharacterSheet) => {
                                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                                const res: any[] = [];
                                rawKeys.forEach((key) => res.push(entry[key]));
                                return res;
                            });
                            await datasheetApiId.setData(`${ date }!A2`, rawData);
                        }
                    } catch (e) {
                        logger.error(e);
                    }
                }

                const link = `${ date }.csv`;
                const attachment: MessageAttachment = new MessageAttachment(bufferData, link);
                const replySuccess =
                    'I\'ve generated a report based on your data request!' +
                    `\n>>> Requests sent: ${ approvedSheets.length }` +
                    `\nResponses received: ${ results.length } ( ${ approvedSheets.length - results.length } lost due to missing sheets/404 or other )` +
                    `\nTime Taken: ${ moment.duration(new Date().valueOf() - time).humanize() }`;

                const path = ctx.config.censusData;
                if (path && fs.existsSync(path)) {
                    const fullPath = `${ path }/${ link }`;
                    await writeFile(fullPath, bufferData, { encoding: 'utf8' });

                    return message.channel.send(
                        replySuccess +
                        `\nLink: https://drewhoener.com${ fullPath.replace('/var/www/html', '') }`
                    );
                } else {
                    return message.channel.send(
                        replySuccess
                        , attachment
                    );
                }

            })
            .catch(err => {
                logger.error(err);
                const reply = 'An Error occurred generating census data, no results can be produced. Please try again later.';
                return Promise.all([
                    message.channel.send(reply),
                    message.author.send(reply),
                ]);
            })
            .then(() => {
                logger.info('Freeing Cur_User in CommandCensus');
                CommandCensus.CUR_USER = undefined;
            });

        return message.channel.send(`> Retrieved ${ approvedSheets.length } sheets to gather data from. Estimated time is ${ duration.humanize() }`);
    }
}

export default CommandCensus;