import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, Reacts } from '../classes/types/context';
import { Message, MessageEmbed, Snowflake } from 'discord.js';
import { CharacterSheet, PendingSheet } from '../db/schema/charactersheets';

class CommandNumChars extends Command {

    private roles: Snowflake[];

    constructor() {
        super({
            name: 'NumChars',
            commandName: CommandNames.NUMCHARS,
            description: 'Gets Number of characters for a player',
            usage: '!numchars',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
        this.roles = [
            '448642903531847680',
            '446705480367079446',
            '448620296136687616',
            '467584509311975445',
        ];
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (args.length < 1) {
            return;
        }
        const userId = args[0].trim();
        if (!message.member || !message.guild) {
            return;
        }
        const member = message.member;
        if (!message.member.hasPermission('ADMINISTRATOR') && !this.roles.some(role => member.roles.cache.has(role))) {
            return message.channel.send('You don\'t have permission to add RP to users, if you think this message may be in error please contact @Abs0rbed or a staff member.');
        }

        const userMember = await message.guild.members.fetch(userId);
        if (!userMember) {
            await message.react(Reacts.ERROR);
            return;
        }

        const characterCountApproved = await CharacterSheet.find({
            userId: userMember.user.id,
            guild: userMember.guild.id,
        }).then(results => results.length).catch(() => 0);

        const characterCountPending = await PendingSheet.find({
            userId: userMember.user.id,
            guild: userMember.guild.id,
        }).then(results => results.length).catch(() => 0);

        let countStr = `${ characterCountApproved }`;
        if (characterCountPending > 0) {
            countStr += ` (${ characterCountPending } pending)`;
        }

        const embed = new MessageEmbed();
        embed.setTitle(`User: ${ userMember.user.tag }`)
            .addField('Approved Characters', countStr);
        return message.channel.send(embed);
    }

}

export default CommandNumChars;