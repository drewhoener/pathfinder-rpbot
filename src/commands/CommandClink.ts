import { Message } from 'discord.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

class CommandClink extends Command {
    constructor() {
        super({
            name: 'Clink',
            commandName: CommandNames.CLINK,
            description: 'Am I working? Who knows...',
            usage: '!clink',
            preventDM: false,
            helpCategory: HelpCategory.GENERAL,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message> {
        return message.channel.send('Clonk!');
    }
}

export default CommandClink;