import logger from '../util/logging';

import JSON from 'circular-json';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { GuildChannel, Message } from 'discord.js';
import { getSettings, ISettings } from '../db/schema/settings';
import WeatherCommand from '../classes/base/WeatherCommand';

class CommandForceWeather extends WeatherCommand {

    constructor() {
        super({
            name: 'ForceWeather',
            commandName: CommandNames.FORCEWEATHER,
            description: 'Force Updates the weather',
            usage: '!forceweather',
            preventDM: true,
            helpCategory: HelpCategory.NONE,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {

        if (!message.guild) {
            return;
        }

        if (args[0]) {
            await this.bindChannel(ctx, message, args);
            return;
        }

        const weatherHolder = WeatherCommand.getWeatherHolder();

        if (weatherHolder) {
            if (weatherHolder.weatherJob != null) {
                weatherHolder.weatherJob.cancel();
            }
            logger.info('Showing client weather holder');
            logger.info(JSON.stringify(weatherHolder.printable(), null, 4));
            logger.info('Cancelled Job');
            weatherHolder.sendEmbedCallback(true, true);
        }
    }

    private async bindChannel(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {

        if (!message.guild) {
            return;
        }

        let channel: GuildChannel | undefined;
        let settings: ISettings | null;
        const guildMember = message.guild.members.cache.get(message.author.id);
        switch (args[0].toLowerCase()) {
            case 'bind':
                if (!guildMember || !guildMember.hasPermission('MANAGE_CHANNELS')) {
                    if (guildMember?.id !== '173923836054470656') {
                        return message.channel.send(`You don't have permission to run this command ${ message.author.toString() }`);
                    }
                }
                if (!message.mentions.channels.size) {
                    return message.channel.send('Tag a channel for weather messages to be sent to');
                }
                channel = message.mentions.channels.first();
                if (!channel) {
                    return message.channel.send('Unable to bind channel for weather messages, try again!');
                }
                settings = await getSettings(message.guild.id);
                if (!settings) {
                    return;
                }
                settings.weather = channel.id;
                return settings.save()
                    .then(() => {
                        return message.channel.send(`Weather messages for all sessions will now be sent to ${ channel }. To unbind type \`!forceweather reset\``);
                    })
                    .catch((err) => {
                        logger.error(err);
                        return message.channel.send('Unable to bind channel for weather messages, try again!');
                    });
            case 'reset':
                if (!guildMember || !guildMember.hasPermission('MANAGE_CHANNELS')) {
                    return message.channel.send(`You don't have permission to run this command ${ message.author.toString() }`);
                }
                settings = await getSettings(message.guild.id);
                if (!settings) {
                    return;
                }
                settings.weather = undefined;
                return settings.save()
                    .then(() => {
                        return message.channel.send('Weather messages for all sessions will now be sent to their own channels.');

                    })
                    .catch((err) => {
                        logger.error(err);
                        return message.channel.send('Unable to unbind channel for weather messages, try again!');
                    });
        }
    }
}

export default CommandForceWeather;