import logger from '../util/logging';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import JSON from 'circular-json';
import _ from 'lodash';
import History, { IHistory } from '../db/schema/history';

const sumChannelHours = (channel: IHistory): number => {
    let total = 0;
    for (const historyEntry of channel.dates) {
        const max = _.max(historyEntry.members.map(memberEntry => memberEntry.time)) ?? 0;
        logger.info(`Object: \n${ JSON.stringify(historyEntry, null, 4) }\nMax: ${ max }`);
        total += max;
    }

    return total;
};

class CommandPlay extends Command {
    constructor() {
        super({
            name: 'Play',
            commandName: CommandNames.PLAY,
            description: 'Lists Play times and other statistics',
            usage: '!play',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        if (!message.guild) {
            return;
        }
        const allHistory = await History.find({ guild: message.guild.id });

        let hoursPlayed = 0;
        for (const channelDocument of allHistory) {
            hoursPlayed += sumChannelHours(channelDocument);
        }
        return message.channel.send(`Total Hours logged by clonk: ${ hoursPlayed.toFixed(2) }`);
    }
}

export default CommandPlay;