import { Random } from 'random-js';
import logger from '../util/logging.js';
import { generateEmbed } from '../util/embeds';
import { Message } from 'discord.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

const random = new Random();

const groupPattern = /((?<sign>(?:[+-]?))((?<die>(\d{0,4}d\d{1,4}))|(?<num>(\d{1,4}))))+/;
const groupGlobal = /((?<sign>(?:[+-]?))((?<die>(\d{0,4}d\d{1,4}))|(?<num>(\d{1,4}))))+/g;
const other = /(([+-])?((\d{0,4}d\d{1,4})|(\d{1,4})))/;
const rollPattern = /((?<sign>(?:[+-]?))((?<die>(\d{0,4}d\d{1,4}))|(?<num>(\d{1,4}))))/;
const rollGlobal = /((?<sign>(?:[+-]?))((?<die>(\d{0,4}d\d{1,4}))|(?<num>(\d{1,4}))))/g;

const isNumber = (val: string): boolean => /^\d+$/.test(val);
const signInt = (sign: string): number => {
    if (sign === '-') {
        return -1;
    }
    return 1;
};

class CommandRoll extends Command {
    constructor() {
        super({
            name: 'Roll',
            commandName: CommandNames.ROLL,
            description: 'Rolls some dice\n!pr {args} may be used for the roll to be sent in PM',
            usage: '!roll (!r) (num)d(sides)+(mods [optional]) {e.g. !r 2d12+1+3}.',
            aliases: ['r', 'pr', 'privateroll'],
            preventDM: false,
            helpCategory: HelpCategory.GENERAL,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.client.user) {
            return;
        }
        const content = message.content.replace(`!${ command } `, '').replace(/\s+/g, '');
        if (!groupPattern.test(content)) {
            logger.silly(`Invalid roll expression ${ content.toString() }`);
            return message.channel.send('`Invalid Roll Expression!`');
        }
        logger.info(content);
        const rollGroups = content.match(groupGlobal);
        if (!rollGroups) {
            logger.silly(`Invalid roll expression ${ content.toString() }`);
            return message.channel.send('`Invalid Roll Expression!`');
        }
        let entries = 0;
        const embed = generateEmbed(message.client.user, null, null, null, 'Dice Roll');
        embed.attachFiles([{ attachment: './img/dice.png', name: 'dice.png' }]);
        embed.setThumbnail('attachment://dice.png');
        let inline = true;
        for (let groupIndex = 0; groupIndex < rollGroups.length; groupIndex++) {
            const rollGroup: string = rollGroups[groupIndex];
            // logger.info(`Group: ${ JSON.stringify(rollGroup.toString(), null, 4) }`);
            if (!rollPattern.test(rollGroup)) {
                continue;
            }
            const rolls = rollGroup.toString().match(rollGlobal);
            let input = '';
            const rollArr = [];
            const rollSums = [];
            const modArr = [];
            for (const rollIndex in rolls) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                const roll = rolls[rollIndex];
                // logger.info(`Roll: ${JSON.stringify(roll.toString(), null, 4)}`);
                // logger.info(JSON.stringify(rollPattern.exec(roll).groups));
                // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
                // @ts-ignore
                const groups = rollPattern.exec(roll).groups;
                if (!groups) {
                    continue;
                }
                const sign = signInt(groups.sign);
                if (groups.die) {
                    const diePattern = /(?<num>\d{0,4})d(?<sides>\d{1,4})/;
                    const dieMatch = groups.die.toString().match(diePattern);
                    // logger.info(dieMatch);
                    if (dieMatch) {
                        const dieGroups = dieMatch.groups;
                        if (!dieGroups) {
                            continue;
                        }
                        // logger.silly(JSON.stringify(dieGroups, null, 4));
                        if (!dieGroups.sides || !isNumber(dieGroups.sides)) continue;
                        const arr = [];
                        const num = dieGroups.num && isNumber(dieGroups.num) ? dieGroups.num : 1;
                        const int = parseInt(dieGroups.sides);
                        for (let i = 0; i < num; i++) {
                            const rand = random.integer(1, int);
                            arr.push(sign * rand);
                            // logger.info(`Pushing ${sign} * ${rand}`);
                        }
                        rollSums.push(arr.reduce((prev, cur) => prev + cur));
                        rollArr.push(arr.length ? `[${ arr.toString() }]` : `${ arr.toString() }`);
                        input += roll;
                        continue;
                    }
                }
                if (groups.num) {
                    const num = groups.num && isNumber(groups.num) ? parseInt(groups.num) : 0;
                    if (num) {
                        modArr.push(sign * num);
                    }
                }
            }
            const sum = rollSums.length ? rollSums.reduce((prev, cur) => prev + cur) : 0;
            const modSum = modArr.length ? modArr.reduce((prev, cur) => prev + cur) : 0;
            if (!sum && !modSum) {
                continue;
            }
            if (sum) {
                let arrStr = rollSums.length > 1 ? `[${ rollArr.toString() }]` : rollArr.toString();
                let sumStr = rollSums.length > 1 ? `→\n**[${ rollSums.toString() }]**` : '';
                if (modSum) {
                    const modStr = modArr.length > 1 ? `[${ modArr.toString() }]` : `${ modSum }`;
                    if (sumStr.length) {
                        sumStr = `→\n**[${ rollSums.toString() }] + ${ modStr }**`;
                    } else {
                        arrStr += ` + ${ modStr }`;
                    }
                }
                embed.addField(`**Input: [${ input }]**`, `_${ arrStr } ${ sumStr }_`, true);
                inline = !inline;
            }
            // embed.addField(`**Result: [${sum} + ${modSum}]**`, `	_**    »»    ${sum + modSum}**_`, true);
            embed.addField('**Result:**', `	_**    »»    ${ sum + modSum }**_`, true);
            // embed.addField('\u200b', '\u200b', true);
            /*
			if (groupIndex < rollGroups.length - 2) {
				embed.addField('\u200b', '\u200b', true);
			}*/
            entries++;
        }
        if (!entries) {
            return;
        }
        if (command.toLowerCase() === 'pr' || command.toLowerCase() === 'privateroll') {
            return message.author.send(embed);
        }
        return message.channel.send(embed);
    }

}

export default CommandRoll;