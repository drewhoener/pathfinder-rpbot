import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, intenseBap } from '../classes/types/context';
import { Message } from 'discord.js';

class CommandNoHorny extends Command {
    constructor() {
        super({
            name: 'NoHorny',
            commandName: CommandNames.NOHORNY,
            description: 'Bad.',
            usage: '!nohorny',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        if (!message.guild || !message.member) {
            return;
        }
        if (!message.member.hasPermission('ADMINISTRATOR')) {
            return;
        }
        let baps = '';
        for (let i = 0; i < 5; i++) {
            baps += intenseBap;
        }
        Promise.resolve(message.delete());
        return message.channel.send(`${ baps }`, { files: ['./img/nohorny.png'] });
    }
}

export default CommandNoHorny;