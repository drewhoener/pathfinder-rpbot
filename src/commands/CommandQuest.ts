import logger from '../util/logging.js';
import { getSettings } from '../db/schema/settings';
import { Guild, GuildMember, Message } from 'discord.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

class CommandQuest extends Command {
    constructor() {
        super({
            name: 'Quest',
            commandName: CommandNames.QUEST,
            description: 'Starts a quest (usually in a quest channel) for a roll20 session',
            usage: '!quest `Message with users tagged`',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    setRoles(guildMember: GuildMember, guild: Guild, message: Message): void {
        if (!guildMember || !guildMember.hasPermission('MANAGE_ROLES')) {
            if (message.author.id !== '173923836054470656') {
                logger.error(`${ message.author.id } doesn't have permission to set quest roles`);
                // message.channel.send(`You don't have permission to run this command ${message.author.toString()}`);
                return;
            }
        }
        const roles = message.mentions.roles;
        if (!roles.size) {
            message.channel.send(`You need to specify roles to use this command ${ message.author.toString() }`);
            return;
        }

        const roleKeys = roles.keyArray().map(key => key.toString());
        getSettings(guild.id)
            .then(async settings => {
                roleKeys.forEach(role => settings?.quest_roles.push(role));
                return settings?.save();
            })
            .then(() => {
                // noinspection JSIgnoredPromiseFromCall
                message.channel.send(`Set the quest roles to ${ roleKeys.map(obj => `<@&${ obj.toString() }>`).join(' ') }`);
                logger.info(`Roles for guild ${ message.guild } is now ${ roleKeys.toString() }`);
            })
            .catch(err => {
                // noinspection JSIgnoredPromiseFromCall
                message.channel.send('An error occurred setting the quest roles, you might have to try again or contact a mod');
                logger.info('Role setting error: ');
                logger.info(err);
            });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        logger.info('Started in quest command');
        if (!message.guild) {
            return;
        }
        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!guildMember) {
            throw new Error('Guild Member is null');
        }
        if (args[0] && args[0].toLowerCase() === 'roles') {
            this.setRoles(guildMember, message.guild, message);
            return;
        }

        const settings = await getSettings(message.guild.id);
        if (!settings) {
            return message.channel.send('Couldn\'t find the server settings to see if you have permission! This is probably a bug');
        }
        const roles = settings.quest_roles;

        if (!roles || !roles.find(role => guildMember.roles.cache.has(role))) {
            if (message.author.id !== '173923836054470656') {
                return message.channel.send(`You don't have permission to start a quest ${ message.author.toString() }`);
            }
        }

        const commandStart: Command | undefined = ctx.commands.get(CommandNames.START);
        if (!commandStart) {
            return;
        }
        await commandStart.execute(ctx, message, args, 'quest');
    }
}


export default CommandQuest;