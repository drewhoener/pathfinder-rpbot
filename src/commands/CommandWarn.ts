import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message, MessageAttachment } from 'discord.js';

const roles = [
    '560464175680192512',
    '448642903531847680',
    '446705480367079446',
    '747970767081308161',
    '461043344932470805',
];

class CommandWarn extends Command {
    constructor() {
        super({
            name: 'Warn',
            commandName: CommandNames.WARN,
            description: 'Bad.',
            usage: '!warn',
            preventDM: true,
            helpCategory: HelpCategory.NONE,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        if (!message.guild || !message.member) {
            return;
        }
        if (!message.member.roles.cache.some((role, key) => roles.includes(key))) {
            return;
        }
        const attachment = new MessageAttachment('./img/stfu.png');
        return message.channel.send(attachment);
    }
}

export default CommandWarn;