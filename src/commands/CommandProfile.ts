import { generateEmbed } from '../util/embeds';
import logger from '../util/logging.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message, Snowflake, TextChannel } from 'discord.js';
import Profile, { IProfile, IWebsiteProfile } from '../db/schema/profiles';

interface IProfileResponse {
    guild: Snowflake;
    data: IWebsiteProfile;
}

class CommandProfile extends Command {
    constructor() {
        super({
            name: 'Profile',
            commandName: CommandNames.PROFILE,
            description: 'Sends you a DM with your profile information',
            usage: '!profile',
            preventDM: false,
            helpCategory: HelpCategory.REGISTER,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.client.user) {
            return;
        }
        if (message.channel instanceof TextChannel) {
            await message.channel.send(`I've sent you a DM with your profile information, ${ message.author }`);
        }
        const profile: IProfile | null = await Profile.findOne({ discord: message.author.id });
        if (!profile) {
            return message.author.send('You don\'t have any Enjin Profile data! Try running \'!register\' in the bot commands channel on your server');
        }
        const profileResponses: IProfileResponse[] = [];
        for (const enjinProfile of profile.guilds) {
            profileResponses.push({
                guild: enjinProfile.guild,
                data: await profile.getProfileData(enjinProfile.enjin),
            });
        }

        for (const profileResponse of profileResponses) {
            const serverName = message.client.guilds.cache.get(profileResponse.guild)?.name ?? profileResponse.guild;
            const date = new Date(0);
            date.setUTCSeconds(parseInt(profileResponse.data.last_activity));
            const response = generateEmbed(message.client.user, `Profile Information for User: ${ message.author.username }`, null, null)
                .setThumbnail(profileResponse.data.avatar)
                .addField('Server Name', serverName, true)
                .addField('Enjin Username', profileResponse.data.username, true)
                .addField('Enjin ID', profileResponse.data.user_id, true)
                .addField('Last Activity', `${ date.toDateString() } ${ date.toLocaleTimeString() }`, true)
                .addField('Total Points', await profile.getPoints(profileResponse.guild))
                .setImage(profileResponse.data.cover_image)
                .setFooter(`${ profileResponses.length } server(s) found`, message.author.avatarURL() ?? undefined);
            await message.author.send(response);
        }
        logger.silly(`Sent profile data to ${ message.author.username }`);
    }
}

export default CommandProfile;