import logger from '../util/logging';
import { Message } from 'discord.js';
import Profile from '../db/schema/profiles';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

class CommandUnregister extends Command {
    constructor() {
        super({
            name: 'Unregister',
            commandName: CommandNames.UNREGISTER,
            description: 'Removes your Enjin ID from a server',
            usage: '!unregister',
            preventDM: true,
            helpCategory: HelpCategory.REGISTER,
        });
    }


    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        try {
            const profile = await Profile.findOne({ discord: message.author.id });
            const guild = message.guild;
            if (!profile || !guild || !profile.hasEnjin(guild.id)) {
                return message.channel.send('Your enjin profile has not been linked to this channel! If you\'d like to register one, type `!register`.');
            }
            profile.guilds = profile.guilds.filter(elem => elem.guild !== guild.id);
            return profile.save()
                .then(() => {
                    logger.silly(`Deleted Enjin data for user ${ message.author.username }`);
                    logger.silly(profile);
                    return message.channel.send(`Your Enjin profile data for this server has been deleted <@${ message.author.id }>`);
                });
        } catch (err) {
            logger.error(`Couldn't delete profile data for ${ message.author.username }`);
            logger.error(err);
            return message.channel.send('Ran into an error deleting your data! Try again or contact a mod');
        }
    }
}

export default CommandUnregister;