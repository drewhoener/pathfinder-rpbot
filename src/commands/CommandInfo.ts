import { generateEmbed } from '../util/embeds';
import moment, { now } from 'moment';
import logger from '../util/logging.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message, TextChannel } from 'discord.js';
import { getSettings, ISettings } from '../db/schema/settings';
import Encounter, { IEncounter } from '../db/schema/encounter';

class CommandInfo extends Command {
    constructor() {
        super({
            name: 'Info',
            commandName: CommandNames.INFO,
            description: 'Provides info on a current Encounter/RP session in a channel',
            usage: '!info',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });

    }


    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.guild) {
            return;
        }
        if (!(message.channel instanceof TextChannel)) {
            return;
        }
        const settings: ISettings | null = await getSettings(message.guild.id);

        if (!settings) {
            return;
        }
        if (args[0]) {
            await this.handleBind(ctx, message, args[0], settings);
            return;
        }

        let infoChannel = null;
        if (settings.info) {
            infoChannel = message.guild.channels.cache.get(settings.info);
        }
        const encounter: IEncounter | null = await Encounter.findOne({ channel: message.channel.id });
        if (!encounter) {
            return message.channel.send('There isn\'t currently a session in this channel! To start one type `!start` and tag some users to play with you');
        }
        if (!ctx.client.user) {
            return;
        }


        const curTime = now();
        const response = generateEmbed(ctx.client.user, `Current Session in ${ message.channel.name }`, null, `Session Info - Started ${ moment(encounter.startTime).calendar() }`);

        for (const member of encounter.getTimes(curTime)) {
            // Get name for pretty display formatting
            let name = `${ member.userId }`;
            try {
                const guildMember = await message.guild.members.cache.get(member.userId);
                name = guildMember?.displayName ?? name;
            } catch (err) {
                logger.error('', err);
            }
            // Duration of play calculation
            const duration = moment.duration(member.end - member.join);
            const hours = duration.asHours();
            const playTime = moment.duration(member.playTime);
            const inactiveHours = moment.duration(member.inactive, 'ms').asHours().toFixed(2);

            response.addField(
                `${ name }${ member.ignoreInResults ? ' (Removed)' : '' }`,
                `Played ${ hours > 1 ? `${ hours.toFixed(2) } hour(s) / Inactive ${ inactiveHours } hour(s)` : `${ duration.asMinutes().toFixed(2) } minute(s)` }
                Credited: ${ playTime.asHours() > 1 ? playTime.asHours().toFixed(2) : 0 } hour(s)`
            );
        }
        const start = moment.duration(curTime - encounter.startTime);
        response.addField('\u200b', '\u200b');
        response.addField('Hours Total', `${ start.asHours().toFixed(2) }`);

        response.setFooter(`Last Message Time: ${ moment(encounter.lastMessage).calendar() }`);
        if (infoChannel && infoChannel instanceof TextChannel) {
            return infoChannel.send(`<@${ message.author.id }>`)
                .then(result => {
                    return result.channel.send(response);
                });
        } else {
            return message.channel.send(response);
        }
    }

    private async handleBind(ctx: IClientContext, message: Message, bindMethod: string, settings: ISettings): Promise<Message | void> {
        let channel: TextChannel | undefined = undefined;
        if (!message.guild) {
            return;
        }
        const mentionedChannels = message.mentions.channels;
        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!settings) {
            return;
        }
        if (!guildMember || !guildMember.hasPermission('MANAGE_CHANNELS')) {
            return message.channel.send(`You don't have permission to run this command ${ message.author.toString() }`);
        }
        switch (bindMethod) {
            case 'bind':
                if (!mentionedChannels) {
                    return message.channel.send('Tag a channel for info messages to be sent to');
                }
                channel = mentionedChannels.first();
                if (!channel) {
                    return message.channel.send('Unable to bind channel for info messages, try again!');
                }
                settings.info = channel.id;
                return settings.save()
                    .then(saved => {
                        return message.channel.send(`Info messages for all sessions will now be sent to ${ saved.info }. To unbind type \`!info reset\``);
                    })
                    .catch(err => {
                        logger.error(err);
                        return message.channel.send('Unable to bind channel for info messages, try again!');
                    });
            default:
            case 'reset':
                settings.info = undefined;
                return settings.save()
                    .then(() => {
                        return message.channel.send('Info messages for all sessions will now be sent to their own channels.');
                    })
                    .catch(err => {
                        logger.error(err);
                        return message.channel.send('Unable to unbind channel for info messages, try again!');
                    });
        }
    }
}

export default CommandInfo;