import { blankEmbed } from '../util/embeds';
import { ClientUser, Message, MessageEmbed } from 'discord.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, HelpCategoryInfo, IClientContext } from '../classes/types/context';

class CommandHelp extends Command {

    constructor() {
        super({
            name: 'Help',
            commandName: CommandNames.HELP,
            description: 'Displays a help menu',
            usage: '!help',
            preventDM: false,
            helpCategory: HelpCategory.GENERAL,
        });
    }


    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        const bot = ctx.client.user;
        if (!bot) {
            return;
        }
        const avatar = bot.avatarURL();

        if (args.length > 0) {
            const category: HelpCategory | undefined = Object.values(HelpCategory).find(val => val.toLowerCase() === args[0].toLowerCase());
            if (!category) {
                return;
            }
            return this.sendHelpMenu(ctx, message, bot, category);
        }

        const embed: MessageEmbed = blankEmbed()
            .setTitle('Clonk Help Categories')
            .setDescription('To view the commands in a category, use `!help [category]`');

        Object.values(HelpCategory).sort().forEach((val, idx, arr) => {
            if (val === HelpCategory.NONE) {
                return;
            }
            const info = HelpCategoryInfo[val];
            const commands = ctx.commands
                .filter(command => command.helpCategory === val)
                .array()
                .sort((o1, o2) => o1.commandName.localeCompare(o2.commandName))
                .map(o => o.name)
                .join(', ');
            embed.addField(
                `${ info.name } (!help ${ val.toLowerCase() })`,
                `${ info.description }\nProvides: ${ commands }`,
                false
            );
        });

        embed.setFooter('To delete a clonk response, click on the Trashcan emote beneath the message', avatar ? avatar : undefined);
        return message.channel.send(embed);
    }

    async sendHelpMenu(ctx: IClientContext, message: Message, bot: ClientUser, helpCategory: HelpCategory): Promise<Message | undefined> {
        const info = HelpCategoryInfo[helpCategory];
        const embed: MessageEmbed = blankEmbed()
            .setTitle(info.name)
            .setDescription(info.description);
        ctx.commands
            .filter(command => command.helpCategory === helpCategory)
            .array()
            .sort((o1, o2) => o1.commandName.localeCompare(o2.commandName))
            .forEach(command => {
                embed.addField(`${ command.name } → ${ command.usage }`, `${ command.description }`);
            });


        const avatar = bot.avatarURL();
        embed.setFooter('To delete a clonk response, click on the Trashcan emote beneath the message', avatar ? avatar : undefined);
        return message.channel.send(embed);
    }

}

export default CommandHelp;