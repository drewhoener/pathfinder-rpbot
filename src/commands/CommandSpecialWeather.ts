import { SPECIAL_WEATHER } from '../classes/factory/WeatherCache';
import JSON from 'circular-json';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';

class CommandSpecialWeather extends Command {
    constructor() {
        super({
            name: 'SpecialWeather',
            commandName: CommandNames.SPECIALWEATHER,
            description: 'Lists types of special weather in a channel',
            usage: '!specialweather',
            preventDM: true,
            helpCategory: HelpCategory.NONE,
        });
    }


    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        for (const index in SPECIAL_WEATHER) {
            if (!SPECIAL_WEATHER.hasOwnProperty(index)) {
                continue;
            }
            await message.channel.send(`\`\`\`${ index }\`\`\``);
            for (const supernatural of SPECIAL_WEATHER[index]) {
                const m = JSON.stringify(supernatural, null, 4);
                await message.channel.send(`\`\`\`json\n${ m }\`\`\``);
            }
        }
    }
}

export default CommandSpecialWeather;