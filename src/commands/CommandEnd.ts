import { generateEmbed } from '../util/embeds';
import moment, { duration, now } from 'moment';
import { GuildMember, Message, MessageEmbed, Snowflake, TextChannel, User } from 'discord.js';
import logger from '../util/logging.js';
import Encounter, { IEncounter, ITimeResult } from '../db/schema/encounter';
import Profile, { IProfile } from '../db/schema/profiles';
import History, { IHistoryEntry, IUserPlay } from '../db/schema/history';

import JSON from 'circular-json';
import { getSettings } from '../db/schema/settings';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import GMCommentHandler from '../handler/GMCommentHandler';

const pattern = /<@!?(?<user>\d+)>\s(?<bonus>\d+)/;
const globalRegex = /<@!?(?<user>\d+)>\s(?<bonus>\d+)/g;
const oneHour = 1000 * 60 * 60;

class CommandEnd extends Command {

    private overrides: Snowflake[];

    constructor() {
        super({
            name: 'End',
            commandName: CommandNames.END,
            description: 'Ends a current Encounter / RP session' +
                '\nGMs may add quest bonus RP by tagging users with point values' +
                '\n`!end @user1 [points] @user2 [points]` (Only during quests)',
            usage: '!end (in the channel you\'re playing in)',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });

        this.overrides = [
            '448642903531847680',
            '560464175680192512',
            '446705480367079446',
        ];
    }

    async awardPoints(message: Message, timeResults: ITimeResult[], response: MessageEmbed, endTime: number): Promise<IHistoryEntry | undefined> {
        if (!message.guild) {
            response.addField('ERROR', 'Reached awardPoints with null guild. Report points manually');
            logger.error('For some reason we\'ve reached awardPoints and message guild is null');
            return;
        }
        for (const timeResult of timeResults) {
            // Get name for display formatting
            let name = `${ timeResult.userId }`;
            const guildMember = message.guild.members.cache.get(name);
            if (guildMember) {
                name = guildMember.displayName;
            }
            // Start time formatting
            const playDuration = duration(timeResult.playTime);
            if (playDuration.asHours() < 1) {
                response.addField(`${ name } → ${ Math.abs(playDuration.asMinutes()).toFixed(2) } minute(s)`, '0 Points, > 1 hour played');
                continue;
            }
            const points = Math.round(Math.abs(playDuration.asHours()) * 5);
            // ID checking
            const profile = await Profile.findOne({ discord: timeResult.userId });
            if (!profile || !profile.hasEnjin(message.guild.id)) {
                response.addField(`${ name } → ${ playDuration.asHours() } hour(s)`, 'No Enjin Profile linked, manual entry required. Make a forum post to claim.');
                continue;
            }
            const hourStr = playDuration.asHours().toFixed(2);
            const enjin = profile.getEnjin(message.guild.id);

            await profile.addPoints(message.guild.id, points)
                .then(() => {
                    response.addField(`${ name } → ${ hourStr } hour(s)`, `${ points } point(s)`);
                    // history[`dates.$[elem].${ elem.userId }`] = duration.asHours();
                    if (enjin) {
                        logger.info(`Credited user ${ enjin.enjin } (${ timeResult.userId }) ${ points } point(s) for duration of ${ hourStr } hours`);
                    }
                }).catch(() => {
                    response.addField(`${ name } → ${ hourStr } hour(s)`, 'Failed to credit, talk to a mod');
                    if (enjin) {
                        logger.error(`Failed to credit user ${ enjin.enjin } (${ timeResult.userId }) ${ points } point(s) for duration of ${ hourStr } hours`);
                    }
                });
        }
        return this.processHistory(message, timeResults, endTime);
    }

    async processHistory(message: Message, timeResults: ITimeResult[], endTime: number): Promise<IHistoryEntry | undefined> {
        if (!message.guild) {
            return;
        }
        let history = await History.findOne({ guild: message.guild.id, channel: message.channel.id });
        const date = moment().hour(0).minute(0).second(0).millisecond(0);
        if (!history) {
            history = new History({
                guild: message.guild.id,
                channel: message.channel.id,
                channelName: (message.channel instanceof TextChannel) ? message.channel.name : message.channel.id,
                dates: [],
            });
        }
        const entry: IHistoryEntry | undefined = history.dates.find((elem: IHistoryEntry) => elem.time === endTime);
        let ret: IHistoryEntry | undefined = undefined;
        if (entry) {
            timeResults.forEach(user => {
                const historyUser: IUserPlay | undefined = entry.members.find((userPlay: IUserPlay) => userPlay.userId === user.userId);
                const playTime = duration(user.playTime);
                if (historyUser) {
                    historyUser.time += playTime.asHours();
                } else {
                    entry.members.push({
                        userId: user.userId,
                        time: playTime.asHours(),
                    });
                }
            });
            ret = entry;
        } else {
            const members: IUserPlay[] = timeResults.map(user => ({
                userId: user.userId,
                time: duration(user.playTime).asHours(),
            }));

            const last = history.dates.push({
                members,
                stamp: date.valueOf(),
                date: date.toString(),
                time: endTime,
            });
            /*
            const obj = {
                members,
                stamp: date.valueOf(),
                date: date.toString(),
                time: endTime,
            };
            logger.info('Just pushed data:');
            logger.info(JSON.stringify(obj, null, 4));
            */
            ret = history.dates[last - 1];
            // logger.warn('We think it is:');
            // logger.info(JSON.stringify(ret, null, 4));
        }

        return history.save()
            .then(result => {
                logger.info('Saved History: ');
                // logger.info(result);
                return ret;
            });
    }

    async awardBonus(message: Message, response: MessageEmbed, isQuest: boolean, external = false): Promise<Message | void> {
        const bonuses = message.content.match(globalRegex);
        if (bonuses) {
            logger.info(JSON.stringify(bonuses));
            logger.info(message.content.toString());
        }

        if (!message.guild) {
            response.addField('ERROR', 'Reached awardBonus with null guild. Report bonus points manually or try again');
            logger.error('For some reason we\'ve reached awardBonus and message guild is null');
            return;
        }

        if (!bonuses) {
            return;
        }
        if (!external) {
            response.addField('\u200b', '\u200b');
        }

        const settings = await getSettings(message.guild.id);
        if (!settings) {
            response.addField('ERROR', 'Bonuses were listed, but the database ran into an error getting the quest roles. Try again or report manually');
            return;
        }

        const roles = settings.quest_roles;
        const authorMember = message.guild.members.cache.get(message.author.id);

        if (!isQuest) {
            response.addField('Bonuses', 'Bonuses were listed but will not be applied. Bonuses can be applied in a Quest');
            return;
        }

        if (!authorMember || !roles || !roles.some(role => authorMember.roles.cache.has(role))) {
            if (message.author.id !== '173923836054470656') {
                response.addField('Bonuses', 'You don\'t have permission to add bonuses to this Quest');
                return;
            }
        }

        logger.silly('Starting Bonus Points');
        let val = '';
        for (let index = 0; index < bonuses.length; index++) {
            // noinspection JSUnfilteredForInLoop
            const bonusMatch = bonuses[index].match(pattern);
            if (!bonusMatch) {
                continue;
            }

            const groups = bonusMatch.groups;
            if (!groups) {
                continue;
            }
            const bonusGuildMember = message.guild.members.cache.get(groups.user);
            let displayName = `<@${ groups.user }>`;
            if (bonusGuildMember) {
                displayName = bonusGuildMember.displayName;
            }
            const newline = index < bonuses.length - 1 ? '\n' : '';
            const profile: IProfile | null = await Profile.findOne({ discord: bonusGuildMember ? bonusGuildMember.id : '-1' });
            if (!profile || !profile.hasEnjin(message.guild.id)) {
                val += `No Enjin profile for ${ displayName }. Bonus of ${ groups.bonus } will not be applied${ newline }`;
                logger.info(`No enjin profile for ${ bonusGuildMember ? bonusGuildMember.id : 'NULL' }, bonus of ${ groups.bonus } points not applied`);
                continue;
            }
            const enjin = profile.getEnjin(message.guild.id);

            await profile.addPoints(message.guild.id, parseInt(groups.bonus))
                .then(() => {
                    val += `${ displayName }: ${ groups.bonus }${ newline }`;
                    if (enjin) {
                        logger.info(`Credited user ${ enjin.enjin } (${ profile.discord }) ${ groups.bonus } bonus point(s)`);
                    }
                })
                .catch(() => {
                    val += `Failed to credit ${ displayName }: ${ groups.bonus } bonus points${ newline }`;
                    if (enjin) {
                        logger.error(`Failed to credit user ${ enjin.enjin } (${ profile.discord }) ${ groups.bonus } bonus point(s)`);
                    }
                });
        }
        if (external) {
            response.setDescription(val);
        } else {
            response.addField('Bonuses', val);
        }
    }

    attemptSendCommentForm(ctx: IClientContext, message: Message, participants: ITimeResult[], history: IHistoryEntry | undefined, gm: { id: Snowflake; name: string }): void {
        if (participants.length > 3 && history) {
            for (const user of participants) {
                if (user.userId === gm.id) {
                    continue;
                }
                try {
                    const handler = new GMCommentHandler(history, user, message.guild, gm);
                    handler.start().then();
                } catch (err) {
                    logger.error(`Failed sending follow-up comments to user ${ user.userId }`);
                    logger.error(err);
                }
            }
            logger.info('Follow up comments sent');
        } else if (!history) {
            logger.error('Couldn\'t send the comment form because we didn\'t have a valid history entry');
        }
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        if (!message.guild) {
            return;
        }
        const encounter: IEncounter | null = await Encounter.findOne({ channel: message.channel.id });
        if (!encounter) {
            return message.channel.send('There isn\'t an Encounter/RP going on in this channel.\nTo start one, run `!start [users]`');
        }
        // Check to make sure they're either in the session or they have permission to end it
        const members = encounter.members;
        if (!members || !members.some(elem => elem.userId === message.author.id)) {
            const guildMember = message.guild.members.cache.get(message.author.id);
            const abilityFlag = guildMember && (guildMember.hasPermission('MANAGE_CHANNELS') || this.overrides.some(role => guildMember.roles.cache.has(role)));
            if (!abilityFlag) {
                return message.channel.send(`You aren't part of this session ${ message.author.toString() }! Have someone participating end it.`);
            }
        }

        const endTime = now();
        const isQuest = encounter.type === 1;

        const response: MessageEmbed = generateEmbed(message.client.user as User, 'Session Results', null, null, 'Point Totals');

        const participatingMembers: ITimeResult[] = encounter.getTimes(endTime).filter(result => !result.ignoreInResults);

        if (participatingMembers.every((elem) => elem.playTime < oneHour) && isQuest) {
            await encounter.remove();
            return message.channel.send(
                '**Incorrect Usage**\n' +
                'It seems like no one in this quest has been in the encounter long enough to gain RP!\n' +
                'If you\'re trying to use this encounter specifically for bonuses because you forgot or something, try the new !addrp command.\n' +
                'Bonuses listed will not be applied, and this option will not be supported for the function it\'s been used for in the past.\n' +
                '*If it turns out this was an actual encounter and there was a mistake, please tag @Abs0rbed directly, sorry for the issue!*'
            );
        }

        const history: IHistoryEntry | undefined = await this.awardPoints(message, participatingMembers, response, endTime);

        const gm = {
            id: encounter.getGM(),
            name: 'UNDEFINED (Couldn\'t get name)',
        };
        try {
            const gmUser: GuildMember | undefined = await message.guild.members.fetch(gm.id);
            if (gmUser) {
                gm.name = gmUser.displayName;
            }
        } catch (err) {
            logger.error('Couldn\'t fetch the member for an encounter. They might not be in the server anymore');
            logger.error(err);
        }
        await encounter.remove();

        /*
        if (Object.keys(history).length) {
            add_history(message.guild.userId, message.channel.userId, date, end_time, history, message.channel.name).then(result => {
                logger.info('Added user history: ', history);
                logger.info(JSON.stringify(result));
            }).catch(logger.error);
        }
         */

        await this.awardBonus(message, response, isQuest);
        // this.attemptSendCommentForm(ctx, message, participatingMembers, history, gm);
        return message.channel.send(response);
    }
}

export default CommandEnd;