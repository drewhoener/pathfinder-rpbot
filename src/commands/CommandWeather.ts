import logger from '../util/logging';

import JSON from 'circular-json';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import WeatherCommand from '../classes/base/WeatherCommand';

class CommandForceWeather extends WeatherCommand {

    constructor() {
        super({
            name: 'Weather',
            commandName: CommandNames.WEATHER,
            description: 'Asks for the current weather',
            usage: '!weather',
            preventDM: true,
            helpCategory: HelpCategory.GENERAL,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {

        if (!message.guild) {
            return;
        }

        const weatherHolder = WeatherCommand.getWeatherHolder();

        if (weatherHolder) {
            if (weatherHolder.weatherJob != null) {
                weatherHolder.weatherJob.cancel();
            }
            logger.info('Showing client weather holder');
            logger.info(JSON.stringify(weatherHolder.printable(), null, 4));
            logger.info('Cancelled Job');
            weatherHolder.sendEmbedCallback(false, false);
        }
    }
}

export default CommandForceWeather;