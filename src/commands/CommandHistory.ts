import { generateEmbed } from '../util/embeds';
import moment from 'moment';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message, MessageEmbed } from 'discord.js';
import History, { IHistoryEntry, IUserPlay } from '../db/schema/history';

class CommandHistory extends Command {
    constructor() {
        super({
            name: 'History',
            commandName: CommandNames.HISTORY,
            description: 'Lists all recent Encounters/RP and point values',
            usage: '!history [#channel tags]',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.client.user) {
            return;
        }
        let response = generateEmbed(message.client.user, 'Recent Play', null, null);
        if (!message.mentions) {
            response.addField('No Data', 'No data requested');
            return message.channel.send(response);
        }
        const channelMentions = message.mentions.channels;
        if (!channelMentions || !channelMentions.size) {
            response.addField('No Channels', 'No channels requested');
            return message.channel.send(response);
        }

        let charCount = 0;
        let fields = 0;
        for (const channelId of channelMentions.keyArray()) {
            const history = await History.findOne({ channel: channelId });
            const channel = channelMentions.get(channelId);
            if (!channel) continue;

            if (!history || !history.dates || !history.dates.length) {
                fields++;
                const length = `**${ channel.name }** No Recent Data`.length;
                if ((charCount + length) >= 6000) {
                    this.sendResponse(message, response);
                    charCount = length;
                    response = generateEmbed(message.client.user, 'Recent Play', null, null);
                }
                response.addField(`**${ channel.name }**`, 'No Recent Data');
                continue;
            }
            const sortedDates = history.dates.sort((e1, e2) => e2.stamp - e1.stamp);
            sortedDates.forEach((entry: IHistoryEntry) => {
                fields++;
                if (!message.client.user) {
                    return;
                }
                const dateStr = moment(entry.time ?? entry.stamp, 'x');
                // logger.info(`Date is ${ dateStr }`);
                const times: string[] = [];
                entry.members.forEach((member: IUserPlay) => {
                    if (!message.guild) return;
                    const guildPlayer = message.guild.members.cache.get(member.userId);
                    const name = guildPlayer?.displayName ?? member.userId;

                    times.push(`**${ name }**: _${ member.time.toFixed(2) } / ${ Math.round(Math.abs(member.time) * 5) } RP_`);
                });

                // noinspection JSUnusedAssignment
                let timeStr = dateStr.calendar(undefined, {
                    lastDay: '[Yesterday]',
                    sameDay: '[Today]',
                    nextDay: '[Tomorrow]',
                    lastWeek: '[Last] dddd',
                    nextWeek: 'dddd',
                    sameElse: 'L',
                });
                timeStr = dateStr.calendar();
                const length = `**${ channel.name } : ${ timeStr }** ${ times.join('\n') }\n`.length;
                if (fields >= 25 || charCount + length >= 6000) {

                    this.sendResponse(message, response);
                    charCount = length;
                    fields = 0;
                    response = generateEmbed(message.client.user, 'Recent Play', null, null);
                } else {
                    charCount = length;
                }
                response.addField(`**${ channel.name } : ${ timeStr }**`, `${ times.join('\n') }\n`);
            });
        }

        return message.channel.send(response);
    }

    async sendResponse(message: Message, response: MessageEmbed): Promise<void> {
        if (!message.client.user) {
            return;
        }

        await message.channel.send(response);
    }
}

export default CommandHistory;