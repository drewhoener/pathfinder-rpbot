import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import GMCommentHandler from '../handler/GMCommentHandler';
import { ITimeResult } from '../db/schema/encounter';
import moment from 'moment';

class CommandTestSurvey extends Command {

    private roles: string[];

    constructor() {
        super({
            name: 'TestSurvey',
            commandName: CommandNames.TESTSURVEY,
            description: 'Bad.',
            usage: '!testsurvey',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
        this.roles = [
            '448642903531847680',
            '446705480367079446',
            '448620296136687616',
            '467584509311975445',
        ];
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        if (!message.member || !message.guild) {
            return;
        }
        const member = message.member;
        if (!message.member.hasPermission('ADMINISTRATOR') && !this.roles.some(role => member.roles.cache.has(role))) {
            return message.channel.send('You don\'t have permission to add use this command, if you think this message may be in error please contact @Abs0rbed or a staff member.');
        }
        const thing: ITimeResult = {
            userId: message.author.id,
            ignoreInResults: false,
            join: moment().valueOf() - 1000000,
            end: moment().valueOf(),
            playTime: 100000,
            inactive: 0,
        };

        const GM = {
            id: '173923836054470656',
            name: 'Abs0rbed',
        };

        return new GMCommentHandler(null, thing, message.guild, GM).start();
    }
}

export default CommandTestSurvey;