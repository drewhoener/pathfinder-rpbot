import { now } from 'moment';
import logger from '../util/logging.js';
import { Collection, GuildMember, Message, Snowflake, TextChannel } from 'discord.js';
import Encounter, { IEncounter, IEncounterMember } from '../db/schema/encounter';
import Profile, { IProfile } from '../db/schema/profiles';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

export function getUsernameString(members: Collection<Snowflake, GuildMember>): string {
    let cur = 1;
    let str = '';
    for (const key of members.keyArray()) {
        const user = members.get(key);
        if (!user) continue;

        str += user.displayName;
        if (++cur === members.size) {
            str += ' and ';
        } else if (cur < members.size) {
            str += ', ';
        }
    }
    return str;
}

class CommandStart extends Command {
    constructor() {
        super({
            name: 'Start',
            commandName: CommandNames.START,
            description: 'Starts a new Encounter or RP session',
            usage: '!start `message with users tagged`',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    async parsePlayers(message: Message): Promise<IEncounterMember[] | undefined> {
        const mentions = message.mentions;
        const noIdUsers: string[] = [];

        if (!mentions.members || mentions.members.size < 1) {
            await message.channel.send(`You need to be playing with at least 1 other person ${ message.author.toString() }! Find a friend and try again`);
            return;
        }
        const curTime = now();
        const players: IEncounterMember[] = [
            {
                userId: message.author.id,
                joinedAt: curTime,
                inactiveHours: 0,
            },
        ];

        for (const user of mentions.members.keyArray()) {
            if (user === message.author.id || players.some(obj => obj.userId === user)) {
                continue;
            }
            players.push({
                userId: user,
                joinedAt: curTime,
                inactiveHours: 0,
            });
            const profile: IProfile | null = await Profile.findOne({ discord: user });
            if (!message.guild) {
                continue;
            }
            if (!profile || !profile.hasEnjin(message.guild.id)) {
                noIdUsers.push(user);
            }
        }

        if (noIdUsers.length) {
            let userMessage = 'The following players will not automatically receive points for this session since their enjin profile is not linked. To link your enjin profile, type `!register` in the bot commands channel. If you link before this session is over, you will receive your points automatically. Once registered, all future sessions will automatically be credited';
            noIdUsers.forEach(id => userMessage += `\n<@${ id }>`);
            await message.channel.send(userMessage);
        }
        return players;
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        try {
            let encounter: IEncounter | null = await Encounter.findOne({ channel: message.channel.id });
            if (message.mentions.everyone) {
                await message.reply('A start command cannot tag everyone! Try again and tag the users you\'re starting with or use !add [user] to add one');
                return;
            }
            if (encounter) {
                await message.channel.send('There is already an Encounter/RP going on in this channel.\nTo end the current one, run `!end`');
                return;
            }
            if (!message.guild) {
                return;
            }
            encounter = new Encounter({
                guild: message.guild.id,
                channel: message.channel.id,
                gm: message.author.id,
            });
            const startTime = now();
            const players: IEncounterMember[] | undefined = await this.parsePlayers(message);
            if (!players) {
                return;
            }

            encounter.type = command === 'quest' ? 1 : 0;
            encounter.members = players;
            encounter.startTime = startTime;
            encounter.lastMessage = startTime + 1;

            encounter.save()
                .then(() => {
                    const members = message.mentions.members;
                    if (!(message.channel instanceof TextChannel) || !message.guild || !members) {
                        return;
                    }
                    logger.info(`Started encounter in channel ${ message.channel.name } (${ message.channel.id }) of server ${ message.guild.name } (${ message.guild.id }) with ${ players.length } player(s)`);
                    message.channel.send(
                        `**A session has been started with ${ getUsernameString(members) }. Let the adventure begin**!
To view the status of the current session, type \`!info\`.To end the session, type \`!end\`. You can add more players at any time, type \`!add\` and tag them in the message`
                    );
                });

        } catch (err) {
            await message.channel.send('An error occured starting your session! Contact a mod about this.');
            logger.error(err);
        }
    }
}

export default CommandStart;