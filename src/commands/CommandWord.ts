import Command, { ICommand } from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, Reacts } from '../classes/types/context';
import { Collection, Message, Snowflake, TextChannel } from 'discord.js';
import WatchedWord, {
    getPartialCacheFor,
    getWatchedWords,
    IWatchedWord,
    IWordPartial,
    loadPartialCaches,
    PartialWordCache,
} from '../db/schema/watchedword';
import { isNumber } from '../util/diceutil';
import moment from 'moment';
import logger from '../util/logging';
import { isEqual } from 'lodash';
import { getSettings } from '../db/schema/settings';

export interface ICommandWord extends ICommand {
    executeChannels(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void>;

    executeWord(ctx: IClientContext, message: Message, watchedWords: IWatchedWord[], word: string, args: string[]): Promise<Message | void>;
}

const updatePartials = (word: string, wordTracker: IWatchedWord, guildPartialMap: PartialWordCache): void => {
    wordTracker.toPartials().forEach((partial: IWordPartial) => {
        guildPartialMap.words.set(partial.word, Object.assign({}, partial));
    });
};

class CommandWord extends Command implements ICommandWord {

    constructor() {
        super({
            name: 'Word',
            commandName: CommandNames.WORD,
            description: 'Tracks watched words',
            usage: '!word [(some word) {track, untrack, settings, info}, list, channels {add, remove, list}]',
            preventDM: true,
            helpCategory: HelpCategory.GENERAL,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        if (args.length < 1 || !message.guild) {
            return;
        }

        const watchedWords: IWatchedWord[] = await getWatchedWords(message.guild.id);

        switch (args[0].toLowerCase()) {
            default:
                return this.executeWord(ctx, message, watchedWords, args[0], args.slice(1));
            case 'rebuild':
                if (!message.member?.hasPermission('MANAGE_CHANNELS')) {
                    return;
                }
                ctx.words.clear();
                await loadPartialCaches(ctx);
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'list':
                if (!message.member?.hasPermission('MANAGE_CHANNELS')) {
                    return;
                }
                return message.channel.send(`\`\`\`${ watchedWords.filter(word => word.tracked).map(word => word.word).join(', ') }\`\`\``);
            case 'partials':
                return message.channel.send(`\`\`\`${ JSON.stringify(ctx.words.array(), null, 4) }\`\`\``.replace(/\\\\/g, '\\'));
            case 'channel':
            case 'channels':
                if (args.length < 2) {
                    return;
                }
                if (!message.member?.hasPermission('MANAGE_CHANNELS')) {
                    return;
                }
                return this.executeChannels(ctx, message, args.slice(1));
        }
    }

    async createNewWord(ctx: IClientContext, guild: string, word: string): Promise<IWordPartial[]> {
        let watchedWord: IWatchedWord | null = await WatchedWord.findOne({ guild, word });
        if (watchedWord) {
            watchedWord.tracked = true;
        } else {
            watchedWord = new WatchedWord({ guild, word });
        }
        return watchedWord.save().then(result => result.toPartials());
    }

    async executeWord(ctx: IClientContext, message: Message, watchedWords: IWatchedWord[], word: string, args: string[]): Promise<Message | void> {
        if (!args.length || !message.guild) {
            return;
        }
        word = word.toLowerCase();

        if (args[0].toLowerCase() !== 'info' && !message.member?.hasPermission('MANAGE_CHANNELS')) {
            return;
        }

        let guildPartialMap: PartialWordCache | undefined = ctx.words.get(message.guild.id);
        if (!guildPartialMap) {
            guildPartialMap = {
                words: new Collection<string, IWordPartial>(),
                watchedChannels: [],
            };
            ctx.words.set(message.guild.id, guildPartialMap);
        }

        const wordTracker: IWatchedWord | undefined = watchedWords.find((_word: IWatchedWord) => _word.word.toLowerCase() === word);
        if (!wordTracker && args[0].toLowerCase() !== 'track') {
            return;
        }

        const lastMentionDuration = moment.duration(moment(wordTracker?.lastTime).diff(moment(), 'ms'));

        switch (args[0].toLowerCase()) {
            default:
            case 'info':
                return message.channel.send('```md' +
                    `\n# ${ wordTracker?.word }` +
                    `\n+ ${ wordTracker?.count } mention(s)` +
                    `\n+ ${ wordTracker?.users.length } user(s)` +
                    `\n+ Last mention: ${ lastMentionDuration.humanize(true) }` +
                    `\n+ Partial:\n${ JSON.stringify(wordTracker?.toPartials(), null, 4).replace(/\\\\/g, '\\') }` +
                    '\n```'
                );
            case 'track':
                if (wordTracker && wordTracker.tracked) {
                    logger.info(`Word ${ word } already exists and will not be tracked`);
                    return;
                }
                // Inline push word and assign partial
                (await this.createNewWord(ctx, message.guild.id, word)).forEach((partial: IWordPartial) => {
                    logger.info('Loading new partial:');
                    logger.info(JSON.stringify(partial, null, 4));
                    if (!guildPartialMap) {
                        logger.error('Couldn\'t load partial');
                        return;
                    }
                    guildPartialMap.words?.set(partial.word, partial);
                });
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'untrack':
                if (!wordTracker || !wordTracker.tracked) {
                    return;
                }
                // Do something to remove this from a local copy of words
                wordTracker.tracked = false;
                updatePartials(word, wordTracker, guildPartialMap);
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'addalias':
                if (!wordTracker || args.length < 2 || wordTracker?.aliases.some(alias => alias.toString() === args.slice(1).join(' ').trim().toLowerCase())) {
                    return;
                }
                wordTracker?.aliases.push(args.slice(1).join(' ').trim().toLowerCase());
                updatePartials(word, wordTracker, guildPartialMap);
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'removealias':
                if (!wordTracker || args.length < 2) {
                    return;
                }
                wordTracker.aliases = wordTracker.aliases.filter(alias => {
                    const allowed = alias.toString() !== args.slice(1).join(' ').trim().toLowerCase();
                    if (!allowed && guildPartialMap) {
                        guildPartialMap.words = guildPartialMap.words.filter(partial => partial.word !== alias.toString().trim());
                    }
                    return allowed;
                });
                updatePartials(word, wordTracker, guildPartialMap);
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'settings':
                if (args.length === 1) {
                    const setting = wordTracker?.setting ?? 0;
                    return message.channel.send(`\`Settings Bits: ${ (setting >>> 0).toString(2) }\``);
                }
                if (!isNumber(args[1]) || !wordTracker) {
                    return;
                }
                wordTracker.setting = parseInt(args[1]);
                updatePartials(word, wordTracker, guildPartialMap);
                break;
        }
        // Watch my back
        await wordTracker?.save();
        logger.info('Checking equality for partial cache');
        const newCache = await getPartialCacheFor(message.guild.id);
        if (!isEqual(newCache, guildPartialMap)) {
            for (let i = 0; i < 20; i++) {
                logger.info('WATCHED WORDS != PARTIAL MAP');
                logger.error('WATCHED WORDS != PARTIAL MAP');
            }
            ctx.logger.log(`\`\`\`Old Cache:\n${ JSON.stringify(guildPartialMap, null, 4) }\nNew Cache:\n${ JSON.stringify(newCache, null, 4) }\`\`\``);
            ctx.logger.log(null);
        }
    }

    async executeChannels(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        if (args[0].toLowerCase() !== 'list' && args.length < 2) {
            return;
        }
        if (!message.guild) {
            return;
        }

        const settings = await getSettings(message.guild.id);
        if (!settings) {
            return;
        }
        const responseStr = ['Clonk is tracking the following channels:'];
        switch (args[0].toLowerCase()) {
            default:
            case 'list':
                settings.watchedChannels.forEach((channel: string, index: number) => {
                    responseStr.push(`${ index === 0 ? '>>> ' : '' }<#${ channel }>`);
                });
                return message.channel.send(responseStr.join('\n'));
            case 'add':
                if (message.mentions.channels.size === 0) {
                    return message.channel.send('Invalid Channel Name!');
                }
                message.mentions.channels.forEach((__: TextChannel, key: Snowflake) => {
                    settings.pushChannel(key);
                });
                await settings.save();
                Promise.resolve(message.react(Reacts.CHECK));
                break;
            case 'remove':
                if (message.mentions.channels.size === 0) {
                    return message.channel.send('Invalid Channel Name!');
                }
                settings.removeChannel(Array.from(message.mentions.channels.keys()));
                await settings.save();
                Promise.resolve(message.react(Reacts.CHECK));
                break;
        }
        return settings.save().then();
    }

}

export default CommandWord;