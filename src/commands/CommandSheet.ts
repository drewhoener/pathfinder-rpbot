import logger from '../util/logging';
import { Guild, GuildMember, Message, TextChannel, User } from 'discord.js';
import Command, { ICommand } from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, PendingSheetType, Reacts } from '../classes/types/context';
import { getSettings, ISettings } from '../db/schema/settings';
import {
    CharacterSheet,
    ICharacterSheetDoc,
    IMythweaversSheetData,
    IPendingSheet,
    PendingSheet,
} from '../db/schema/charactersheets';
import { generateEmbed } from '../util/embeds';
import { getMythWeaversRaw, MW_SHEET_REGEX, ONLY_DIGITS, SNOWFLAKE_REGEX } from '../util/mythweaversutil';

export interface ICommandSheet extends ICommand {
    approveSheet(ctx: IClientContext, sheet: IPendingSheet): Promise<boolean>;
}

class CommandSheet extends Command implements ICommandSheet {

    constructor() {
        super({
            name: 'Sheet',
            commandName: CommandNames.SHEET,
            description: 'Command to process sheets',
            usage: '!sheet [(userID, username, @mention), submit, retrain, retire, bind, setmax]',
            preventDM: true,
            helpCategory: HelpCategory.REGISTER,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        if (!message.guild) {
            return;
        }

        if (!args.length) {
            return this.displayPlayerSheets(ctx, message, args, undefined, message.guild);
        }

        let user: User | string | undefined;
        switch (args[0].toLowerCase()) {
            default:
                if (!message.guild) {
                    break;
                }
                if (message.mentions.members && message.mentions.members.size) {
                    logger.info('I think a user was mentioned');
                    user = message.mentions.members.first()?.user;
                } else {
                    user = args[0];
                    logger.info(`Using args ${ args[0] }`);
                }

                return this.displayPlayerSheets(ctx, message, args, user, message.guild);
            case 'submit':
                return this.submitSheet(ctx, message, args[1].trim());
            case 'retrain':
                return this.setupRequestRetrain(ctx, message, args[1].trim(), PendingSheetType.RETRAIN);
            case 'retire':
                return this.setupRequestRetrain(ctx, message, args[1].trim(), PendingSheetType.RETIRE);
            case 'bind':
                return this.bindChannel(message);
            case 'setmax':
                return this.setMaxSheets(ctx, message, args);
        }
    }

    async displayPlayerSheets(ctx: IClientContext, message: Message, args: string[], user: User | string | undefined, guild: Guild): Promise<Message | void> {

        if (user) {
            logger.info(`User exist: ${ user }`);
        }

        if (typeof user === 'string') {
            logger.info('User is string');
        }

        if (typeof user === 'string' && SNOWFLAKE_REGEX.test(user)) {
            logger.info('User matches regex');
        }

        if (user && (typeof user === 'string') && SNOWFLAKE_REGEX.test(user)) {
            const member: GuildMember | undefined = await guild.members.fetch(user).catch(() => undefined);
            if (!member) {
                logger.info('Member undefined');
                user = undefined;
            } else {
                logger.info('Replacing user with member');
                user = member.user;
            }
        }

        if (!user) {
            logger.info('No user, using author');
            user = message.author;
        }

        const userParameter: User | string = user;

        if (!(userParameter instanceof User)) {
            logger.info('Not instanceof');
            return;
        }

        const sheets = await CharacterSheet.find({
            guild: guild.id,
            userId: userParameter.id,
        });

        if (!sheets || !sheets.length) {
            return message.channel.send(`**Located 0 sheet(s) for \`${ userParameter.tag }\`**`);
        }

        logger.info(`${ sheets.length } sheets.`);

        const charStrs = sheets
            .sort((o1: ICharacterSheetDoc, o2: ICharacterSheetDoc) => o1._id.getTimestamp().valueOf() - o2._id.getTimestamp().valueOf())
            .map((sheet: ICharacterSheetDoc, idx: number) => {
                return `>>> ${ idx + 1 }: ${ sheet.characterName }\n\t${ sheet.race }\n\t${ sheet.class }`;
            }).join('\n');

        return message.channel.send(`**Located ${ sheets.length } sheet(s) for \`${ userParameter.tag }\`**\n${ charStrs }`);
    }

    async submitSheet(ctx: IClientContext, message: Message, sheet: string): Promise<Message | void> {
        const matches = MW_SHEET_REGEX.exec(sheet);
        if (!matches || !message.guild) {
            console.log(matches);
            console.log(!!message.guild);
            return;
        }
        const channel = await getSettings(message.guild.id)
            .then((result: ISettings | null) => {
                if (!result || !message.guild || !result.sheets) {
                    return;
                }
                return message.guild.channels.cache.get(result.sheets);
            });

        if (!channel || !(channel instanceof TextChannel) || !ctx.client.user) {
            return message.channel.send('Failed to submit your sheet automatically! Manually submit it to a mod');
        }

        const id = matches[1];
        const mythResult: IMythweaversSheetData | undefined = await this.getMythWeaversSheet(message, message.guild, sheet, id);

        if (!mythResult) {
            return;
        }

        const sheetURLPendingCount = await PendingSheet.find({
            mythweavers: mythResult.mythweavers,
            requestType: PendingSheetType.REQUEST,
        }).then(results => results.length).catch(() => -1);

        const sheetURLApprovedCount = await CharacterSheet.find({
            mythweavers: mythResult.mythweavers,
        }).then(results => results.length).catch(() => -1);

        const characterCountApproved = await CharacterSheet.find({
            userId: message.author.id,
            guild: message.guild.id,
        }).then(results => results.length).catch(() => 0);

        const characterCountPending = await PendingSheet.find({
            userId: message.author.id,
            guild: message.guild.id,
        }).then(results => results.length).catch(() => 0);

        if (sheetURLPendingCount < 0 || sheetURLApprovedCount < 0) {
            return message.reply('Couldn\'t contact the DB to see if this sheet is in use. Try again later or contact staff.');
        }

        if (sheetURLPendingCount > 0) {
            return message.reply(`The sheet for your character ${ mythResult.characterName } already is awaiting approval. Go bug a mod`);
        }
        if (sheetURLApprovedCount > 0) {
            return message.reply(`The sheet for your character ${ mythResult.characterName } is already in use!`);
        }

        let countStr = `${ characterCountApproved }`;
        if (characterCountPending > 0) {
            countStr += ` (${ characterCountPending } pending)`;
        }

        const maxCharacterCount = await getSettings(message.guild.id)
            .then((result: ISettings | null) => {
                if (!result) {
                    return -1;
                }
                return result.maxCharacterSheets;
            })
            .catch(() => -1);

        if (maxCharacterCount >= 0 && (characterCountApproved + characterCountPending) >= maxCharacterCount) {
            return message.reply(
                'You\'ve used up all of your available character slots.\n' +
                `Your characters: \`${ countStr }\`\n` +
                `Maximum number of characters: \`${ maxCharacterCount }\`.\n` +
                '> To retire an old character: **!sheet retire**\n' +
                '> To use RP to retrain a character: **!sheet retrain**'
            );
        }

        const response = generateEmbed(ctx.client.user, 'Sheet Submission', null, `${ message.author } is requesting approval for their sheet!`, undefined);
        response.addField('Character Name', mythResult.characterName)
            .addField('Race', mythResult.race, true)
            .addField('Class', mythResult.class, true)
            .addField('MythWeavers Link', mythResult.mythweavers)
            .addField('Approved Characters', countStr);

        const approvalMessage = await channel.send(response);
        const pendingSheet = new PendingSheet({
            guild: message.guild.id,
            userId: message.author.id,
            playerName: mythResult.playerName,
            characterName: mythResult.characterName,
            race: mythResult.race,
            class: mythResult.class,
            mythweavers: mythResult.mythweavers,
            messageId: message.id,
            approveMessageId: approvalMessage.id,
        });

        pendingSheet.save()
            .then(() => {
                if (!mythResult.playerName) {
                    throw new Error('Sheet must have player name!');
                }
                return Promise.all([
                    approvalMessage.react(Reacts.APPROVE),
                    approvalMessage.react(Reacts.DENY),
                    approvalMessage.react(Reacts.LOOKING),
                    message.channel.send(`The sheet for your character ${ mythResult.characterName } has been whisked away to the mods! You'll be notified of its status`),
                ]).catch((err: Error) => {
                    logger.error('One of the reactions for approve/deny failed to apply');
                    logger.error(err);
                });
            })
            .catch(async () => {
                try {
                    await pendingSheet.remove();
                } catch (e) {
                    // Ignored
                }
                return Promise.all([
                    message.author.send('An error occurred submitting your sheet! Make sure your link is correct and the \'Player\', \'Name\', \'Race\', and \'Class\' fields are all filled!\nYou may have to resubmit your sheet'),
                    approvalMessage.react(Reacts.ERROR).then(() => approvalMessage.delete()),
                ]);
            });
    }

    async getMythWeaversSheet(message: Message, guild: Guild, sheetLink: string, sheetId: string): Promise<IMythweaversSheetData | undefined> {
        return getMythWeaversRaw(sheetLink, sheetId)
            .then((sheet) => {
                if (!sheet) {
                    return undefined;
                }
                return {

                    guild: guild.id,
                    userId: message.author.id,
                    playerName: message.author.tag,
                    characterName: sheet.Name,
                    race: sheet.Race,
                    class: sheet.Class,
                    mythweavers: sheetLink,
                    notes: [],
                };
            })
            .catch(err => {
                console.error(err);
                return undefined;
            });
    }

    async setupRequestRetrain(ctx: IClientContext, message: Message, sheetLink: string, requestType: PendingSheetType): Promise<Message | void> {
        return message.channel.send('This feature is not yet implemented, sorry!');
    }

    async approveSheet(ctx: IClientContext, sheet: IPendingSheet): Promise<boolean> {

        const newSheet = new CharacterSheet(sheet.getApproved());
        return newSheet.save()
            .then(() => true)
            .catch(() => false);
    }

    async bindChannel(message: Message): Promise<Message | void> {
        if (!message.guild) {
            return;
        }
        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!guildMember || !guildMember.hasPermission('MANAGE_CHANNELS')) {
            if (message.author.id !== '173923836054470656') {
                logger.error(`${ message.author.id } doesn't have permission to bind the sheet channel`);
                // message.channel.send(`You don't have permission to run this command ${message.author.toString()}`);
                return;
            }
        }
        if (!message.mentions.channels.size) {
            return;
        }
        const channel = message.mentions.channels.first();
        if (channel == null) {
            return;
        }

        return getSettings(message.guild.id)
            .then(async (result: ISettings | null) => {
                if (!result) {
                    return;
                }
                result.sheets = channel.id;
                await result.save();
                await message.react(Reacts.CHECK);
            })
            .catch(error => {
                logger.error('Failed to get settings');
                logger.error(error);
                return message.channel.send('Unable to bind channel for sheets messages, try again!');
            });
    }

    async setMaxSheets(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        if (!message.guild || args.length < 2) {
            return;
        }

        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!guildMember || !guildMember.hasPermission('ADMINISTRATOR')) {
            if (message.author.id !== '173923836054470656') {
                logger.error(`${ message.author.id } doesn't have permission to set the max sheets`);
                return;
            }
        }

        let maxSheets = NaN;

        if (ONLY_DIGITS.test(args[1])) {
            maxSheets = parseInt(args[1]);
        } else {
            logger.info(`Invalid Arg: ${ args[1] }`);
        }
        if (isNaN(maxSheets)) {
            return message.channel.send('Please provide a number to set as the maximum, or -1 to remove the limit.');
        }

        return getSettings(message.guild.id)
            .then(async (result: ISettings | null) => {
                if (!result) {
                    return;
                }
                result.maxCharacterSheets = maxSheets;
                await result.save();
                await message.react(Reacts.CHECK);
            })
            .catch(error => {
                logger.error('Failed to get settings');
                logger.error(error);
                return message.channel.send('Unable to set the max number of sheets!');
            });
    }
}

export default CommandSheet;