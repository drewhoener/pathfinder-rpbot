import { blankEmbed } from '../util/embeds';
import { pollEmoji } from './CommandPoll';
import { fetchPollMessage } from '../util/utils';
import Command from '../classes/base/Command';
import { Message } from 'discord.js';
import Poll, { IPoll } from '../db/schema/polls';
import { CommandNames, HelpCategory, IClientContext, Reacts } from '../classes/types/context';

class CommandEndPoll extends Command {
    constructor() {
        super({
            name: 'EndPoll',
            commandName: CommandNames.ENDPOLL,
            description: 'Ends a poll',
            usage: '!endpoll [poll userId]',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.guild) {
            throw new Error('Failed to find guild');
        }

        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!guildMember || !guildMember.hasPermission('MANAGE_MESSAGES')) {
            if (message.author.id !== '173923836054470656') {
                return message.channel.send('You don\'t have permission to end polls!');
            }
        }
        if (!args || !args.length) {
            return;
        }
        const pollObj: IPoll | null = await Poll.findOneAndDelete({ pollId: args[0] });
        if (!pollObj) {
            return;
        }
        const pollMessage = await fetchPollMessage(message.client, pollObj.guild, pollObj.channel, pollObj.messageId, pollObj.pollId);
        if (!pollMessage) {
            return message.channel.send('Unable to close this poll, the reacts from the original poll should be used. Sorry');
        }
        if (!message.author.bot) {
            await message.delete();
        }
        const reacts = pollMessage.reactions.cache.filter(messageReact => pollEmoji.includes(messageReact.emoji.toString())).array();
        const votes = [];
        for (const messageReact of reacts) {
            if (!messageReact) {
                continue;
            }
            const index = pollEmoji.indexOf(messageReact.emoji.toString());
            votes.push({
                num: (messageReact.count ?? 1) - 1,
                option: pollObj.options[index],
            });
        }
        votes.sort((a, b) => b.num - a.num);
        const embed = blankEmbed().setTitle('Poll Results');
        let results = '';
        for (const vote of votes) {
            results += `**${ vote.num } ${ vote.num === 1 ? 'vote' : 'votes' }: ** ${ vote.option }\n`;
        }
        await pollMessage.delete();
        message.channel.send(embed.addField(pollObj.question, results)).then(async msg => await msg.react(Reacts.POLL));
    }
}

export default CommandEndPoll;