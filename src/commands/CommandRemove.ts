import { now } from 'moment';
import { generateEmbed } from '../util/embeds';
import CommandEnd from './CommandEnd';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import Encounter, { IEncounter, ITimeResult } from '../db/schema/encounter';

class CommandRemove extends Command {
    constructor() {
        super({
            name: 'Remove',
            commandName: CommandNames.REMOVE,
            description: 'Removes a player from an ongoing Encounter/RP session. No bonuses will be given',
            usage: '!remove `message with users tagged`',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }


    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.client.user || !message.guild) {
            return;
        }
        const encounter: IEncounter | null = await Encounter.findOne({ channel: message.channel.id });
        if (!encounter) {
            return message.channel.send('There isn\'t currently a session in this channel! To start one type `!start` and tag some users to play with you');
        }
        if (message.mentions.everyone) {
            return message.reply('A remove command cannot tag everyone! Try again and tag the user you\'re trying to remove.');
        }

        const mentions = message.mentions;
        if (!mentions.members || mentions.members.size < 1) {
            return message.channel.send(`You need to tag someone to remove ${ message.author.toString() }! If you're trying to remove yourself, tag yourself.`);
        }
        if (mentions.members.size >= encounter.members.length) {
            return message.channel.send('You can\'t remove everyone from an encounter. If you\'d like to end the session, type`!end`');
        }
        if (mentions.members.size + 1 >= encounter.members.length) {
            return message.channel.send('You need to have at least one other person in the encounter with you');
        }

        const curTime = now();
        const removedPlayers: ITimeResult[] = encounter.getTimes(curTime).filter(timeResult => {
            return message.mentions.members?.has(timeResult.userId) ?? false;
        });

        const response = generateEmbed(message.client.user, 'Removed Player Results', null, 'Point Totals');
        const endCommand: CommandEnd = ctx.commands.get(CommandNames.END) as CommandEnd;
        await endCommand.awardPoints(message, removedPlayers, response, curTime);

        encounter.members.filter((member) => {
            return removedPlayers.some(removed => removed.userId === member.userId);
        }).forEach(member => {
            member.ignoreInResults = true;
        });

        return await encounter.save()
            .then(() => {
                return message.channel.send(response);
            });
    }
}

export default CommandRemove;