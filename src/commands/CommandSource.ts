import { Message } from 'discord.js';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

class CommandSource extends Command {
    constructor() {
        super({
            name: 'Source',
            commandName: CommandNames.SOURCE,
            description: 'Display source information',
            usage: '!source',
            preventDM: false,
            helpCategory: HelpCategory.GENERAL,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        /*

                const handler = new LevelUpHandler();
                await handler.executeJob(ctx);
                handler.cancel();
        */

        return message.channel.send('I\'m open source! Check out my source code!\nhttps://gitlab.com/drewhoener/pathfinder-rpbot');
    }
}

export default CommandSource;