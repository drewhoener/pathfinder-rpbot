import { blankEmbed } from '../util/embeds';
import moment from 'moment';
import { scheduleJob } from 'node-schedule';
import shortid from 'shortid';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext, Reacts } from '../classes/types/context';
import { Message } from 'discord.js';
import Poll from '../db/schema/polls';

export const pollEmoji = [
    '\u{1F1E6}',
    '\u{1F1E7}',
    '\u{1F1E8}',
    '\u{1F1E9}',
    '\u{1F1EA}',
    '\u{1F1EB}',
    '\u{1F1EC}',
    '\u{1F1ED}',
    '\u{1F1EE}',
    '\u{1F1EF}',
    '\u{1F1F0}',
    '\u{1F1F1}',
    '\u{1F1F2}',
    '\u{1F1F3}',
    '\u{1F1F4}',
    '\u{1F1F5}',
    '\u{1F1F6}',
    '\u{1F1F7}',
    '\u{1F1F8}',
    '\u{1F1F9}',
];

class CommandPoll extends Command {
    constructor() {
        super({
            name: 'Poll',
            commandName: CommandNames.POLL,
            description: 'Creates a poll for users to react to',
            usage: '!poll [message]',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
    }


    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.guild) {
            return;
        }
        const guildMember = message.guild.members.cache.get(message.author.id);
        if (!guildMember || !guildMember.hasPermission('MANAGE_MESSAGES')) {
            if (message.author.id !== '173923836054470656') {
                return message.channel.send('You don\'t have permission to create polls!');
            }
        }
        if (!args || !args.length) {
            return;
        }
        args = message.content.replace(/^!\S+\s/, '').split('\n');
        if (args.length <= 2) {
            return message.channel.send('Can\'t create a poll with only one option. Reevaluate yourself');
        }

        const id = shortid.generate();
        const endTime = moment().add(12, 'hours');
        const poll = new Poll({
            guild: message.guild.id,
            channel: message.channel.id,
            messageId: message.id,
            question: args[0],
            options: [
                ...args.slice(1),
            ],
            pollId: id.toString(),
            end: endTime.valueOf(),
        });

        const numOptions = poll.options.length;
        const embed = blankEmbed().setFooter(`This poll will close at: ${ endTime.calendar() }. \nPoll ID: ${ id }`);
        let optionStr = '';
        for (let index = 0; index < poll.options.length; index++) {
            optionStr += `${ pollEmoji[index] }  ${ poll.options[index] }\n\n`;
        }
        embed.addField(poll.question, optionStr);
        message.channel.send(embed).then(async sentMessage => {
            await message.delete();
            await sentMessage.react(Reacts.POLL);
            for (let i = 0; i < numOptions; i++) {
                await sentMessage.react(pollEmoji[i]);
            }
            poll.messageId = sentMessage.id;
            await poll.save();
            scheduleJob(endTime.toDate(), () => {
                require('./endpoll.js').execute(sentMessage, [`${ poll.pollId }`]);
            });
        });
    }
}

export default CommandPoll;