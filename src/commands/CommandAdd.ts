import { now } from 'moment';
import { getUsernameString } from './CommandStart';
import logger from '../util/logging.js';
import { Message, MessageMentions, Snowflake } from 'discord.js';
import Encounter, { IEncounter, IEncounterMember } from '../db/schema/encounter';
import Profile, { IProfile } from '../db/schema/profiles';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

class CommandAdd extends Command {

    constructor() {
        super({
            name: 'Add',
            commandName: CommandNames.ADD,
            description: 'Adds a player to an ongoing Encounter/RP session',
            usage: '!add `message with users tagged`',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        const encounter: IEncounter | null = await Encounter.findOne({ channel: message.channel.id });
        if (!encounter) {
            return message.channel.send('There isn\'t currently a session in this channel! To start one type `!start` and tag some users to play with you');
        }
        if (message.mentions.everyone) {
            return message.reply('A start command cannot tag everyone! Try again and tag the users you\'re starting with or use !add [user] to add one');
        }
        const mentions = message.mentions;
        const players: IEncounterMember[] = await this.updatePlayers(message, args, mentions, encounter);
        if (!mentions || !mentions.members || mentions.members.size < 1) {
            return message.channel.send(`You need to tag someone to add ${ message.author.toString() }! If you're trying to add yourself, tag yourself.`);
        }

        if (players.length > encounter.members.length) {
            encounter.members = players;
            return encounter.save()
                .then(() => {
                    if (!mentions.members) {
                        return;
                    }
                    logger.info(`Added ${ mentions.members.size } user(s) to encounter in ${ message.channel.id }`);
                    return message.channel.send(`**Added ${ getUsernameString(mentions.members) } to the adventure!**`);
                });
        }
    }

    private updatePlayers = async (message: Message, args: string[], mentions: MessageMentions, encounter: IEncounter): Promise<IEncounterMember[]> => {
        const alreadyIn = [];
        const curTime = now();
        const players = [...encounter.members];
        const noIdPlayers: Snowflake[] = [];

        if (!mentions.members) {
            return players;
        }


        for (const user of mentions.members.keyArray()) {
            const existingUser: IEncounterMember | undefined = players.find(obj => obj.userId === user);
            if (existingUser) {
                if (existingUser.ignoreInResults) {
                    existingUser.ignoreInResults = false;
                    existingUser.joinedAt = curTime;
                    existingUser.inactiveHours = 0;
                } else {
                    alreadyIn.push(user);
                }
                continue;
            }

            players.push({
                userId: user,
                joinedAt: curTime,
                inactiveHours: 0,
            });
            const profile: IProfile | null = await Profile.findOne({ discord: user });
            if (!profile || !message.guild || !profile.hasEnjin(message.guild.id)) {
                noIdPlayers.push(user);
            }
        }

        if (alreadyIn.length) {
            let userMessage = 'The following users are already part of the current session and will not be added:';
            alreadyIn.forEach(id => userMessage += `\n<@${ id }>`);
            await message.channel.send(userMessage);
        }
        if (noIdPlayers.length) {
            let userMessage = 'The following players will not automatically receive points for this session since their enjin profile is not linked. To link your enjin profile, type `!register` in the bot commands channel. If you link before this session is over, you will receive your points automatically. Once registered, all future sessions will automatically be credited';
            noIdPlayers.forEach(id => userMessage += `\n<@${ id }>`);
            await message.channel.send(userMessage);
        }

        return players;
    };
}

export default CommandAdd;