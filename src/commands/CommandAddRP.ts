import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message, Snowflake } from 'discord.js';
import CommandEnd from './CommandEnd';
import { blankEmbed } from '../util/embeds';

class CommandAddRP extends Command {

    private roles: Snowflake[];

    constructor() {
        super({
            name: 'AddRP',
            commandName: CommandNames.ADDRP,
            description: 'Adds bonus RP to one or more players',
            usage: '!addrp @springsight 200 @user2 [bonus2]',
            preventDM: true,
            helpCategory: HelpCategory.ADMIN,
        });
        this.roles = [
            '448642903531847680',
            '446705480367079446',
            '448620296136687616',
            '467584509311975445',
        ];
    }

    async execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void> {
        if (!message.member || !message.guild) {
            return;
        }
        const member = message.member;
        if (!message.member.hasPermission('ADMINISTRATOR') && !this.roles.some(role => member.roles.cache.has(role))) {
            return message.channel.send('You don\'t have permission to add RP to users, if you think this message may be in error please contact @Abs0rbed or a staff member.');
        }
        const endCommand = ctx.commands.get(CommandNames.END) as CommandEnd;
        const bonuses = blankEmbed();
        bonuses.setTitle('Bonuses');
        await endCommand.awardBonus(message, bonuses, true, true);
        return message.channel.send(bonuses);
    }
}

export default CommandAddRP;