import { generateEmbed } from '../util/embeds';
import moment from 'moment';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';
import { Message } from 'discord.js';
import Encounter, { IEncounter } from '../db/schema/encounter';
import { max } from 'lodash';

class CommandOngoing extends Command {
    constructor() {
        super({
            name: 'Ongoing',
            commandName: CommandNames.ONGOING,
            description: 'Gets currently ongoing RP sessions',
            usage: '!ongoing',
            preventDM: true,
            helpCategory: HelpCategory.ENCOUNTER,
        });
    }

    async execute(ctx: IClientContext, message: Message): Promise<Message | void> {
        let processed = 0;
        if (!message.client.user) {
            return;
        }
        let response = generateEmbed(message.client.user, null, null, null, `Ongoing Sessions: ${ (processed % 25) + 1 }`);
        if (!message.guild) {
            response.addField('ERROR', 'No Server found for encounters');
            return message.channel.send(response);
        }
        const ongoingEncounters = await Encounter.find({ guild: message.guild.id });
        ongoingEncounters.forEach((encounter: IEncounter) => {
            if (!message.guild || !message.client.user) {
                return;
            }
            processed++;
            let users = '';
            for (const member of encounter.members) {
                const name = message.guild.members.cache.get(member.userId)?.displayName ?? member.userId;
                users += `${ name }\n`;
            }

            users = users.replace(/^\s+|\s+$/g, '');
            const channel = message.guild.channels.cache.get(encounter.channel);
            const channelName = channel?.name ?? encounter.channel;
            const startTime = moment(encounter.startTime).calendar();
            const lastMessage = moment(encounter.lastMessage).calendar();
            const modTime = max(encounter.members.map(member => member.inactiveHours)) ?? 0;

            response.addField(`**${ channelName }**`, `**Members**:\n${ users }\n**Start Time**: ${ startTime }\n**Last Message**: ${ lastMessage }\n**Max Hours Subtracted**: ${ modTime }\n`);

            if (processed > 25) {
                message.author.send(response);
                response = generateEmbed(message.client.user, null, null, null, `Ongoing Sessions: ${ (processed % 25) + 1 }`);
            }
        });

        return message.author.send(response);
    }
}

export default CommandOngoing;