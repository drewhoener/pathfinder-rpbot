import logger from '../util/logging.js';
import { Message } from 'discord.js';
import Profile, { IProfile } from '../db/schema/profiles';
import Command from '../classes/base/Command';
import { CommandNames, HelpCategory, IClientContext } from '../classes/types/context';

const isNumber = (val: string): boolean => /^\d+$/.test(val);

class CommandRegister extends Command {

    constructor() {
        super({
            name: 'Register',
            commandName: CommandNames.REGISTER,
            description: 'Use this to link your Enjin profile with your discord account',
            usage: '!register (in channel) / !register `profile_id` (in DM)',
            preventDM: false,
            helpCategory: HelpCategory.REGISTER,
        });
    }


    async execute(ctx: IClientContext, message: Message, args: string[]): Promise<Message | void> {
        try {
            let profile: IProfile | null = await Profile.findOne({ discord: message.author.id }).exec();
            if (!profile) {
                profile = new Profile({ discord: message.author.id, username: message.author.username });
            }

            // If they messaged a guild channel and they're already registered there
            if (message.guild && profile.hasEnjin(message.guild.id)) {
                message.channel.send('Your enjin profile has already been linked to this channel! If you\'d like to change your linked account, type `!unregister`. Your profile information will be sent in a DM for review')
                    .then(() => {
                        const profileCommand: Command | undefined = ctx.commands.get(CommandNames.PROFILE);
                        if (!profileCommand) {
                            return message.author.send('Unfortunately an error occured and your profile info couldn\'t be sent');
                        }
                        setTimeout(() => profileCommand.execute(ctx, message, [], CommandNames.PROFILE), 1000);
                    });
                return;
            }

            // If they're not yet pending
            if (!profile.isPending()) {
                await this.sendPendingMessages(ctx, message, args, profile);
                return;
            }

            // If register was called from a server channel
            await this.parseRegister(message, args, profile);

        } catch (err) {
            logger.error(err);
            return message.author.send('An error occurred when getting you ready to link your profile! You might have to try again.');
        }
    }

    async sendPendingMessages(ctx: IClientContext, message: Message, args: string[], profile: IProfile): Promise<IProfile | Message | void> {
        const { website } = ctx.config.auth.webAuth;
        if (message.guild) {
            profile.pending = {
                guild: message.guild.id,
                name: message.guild.name,
            };
            return profile.save()
                .then(() => {
                    return message.reply('I\'ve sent you a DM to finish your registration');
                })
                .then(async () => {
                    // I don't think I need this but I had it before sooooo
                    if (!message.guild) {
                        return;
                    }
                    await message.author.send(`Type \`!register profile_id\` to finish linking your enjin profile to the server \`${ message.guild.name }\``);
                    await message.author.send(`You can locate your profile id on your profile page at ${ website }. It should be a collection of numbers at the end of the URL`);
                    try {
                        await message.author.send('**Use the image below for reference.**', { files: ['./img/profile.png'] });
                    } catch (error) {
                        await message.author.send(`You can locate your profile id on your profile page at ${ website }`);
                    }
                });
        }

        await message.author.send(
            'You haven\'t requested to link your enjin profile to a server yet! Type `!register` in the bot commands channel of the server you wish to link to proceed.',
        );
    }

    async parseRegister(message: Message, args: string[], profile: IProfile): Promise<Message | undefined> {
        if (!args[0]) {
            return message.author.send('Please provide your profile userId number in order to finish registration: `!register profile_id`');
        }

        if (!isNumber(args[0])) {
            logger.info(`${ message.author.username } input an invalid id of ${ args[0] }`);
            return message.author.send(`The input ${ args[0] } isn't a number! Are you sure you entered the right thing? Check the screenshot again and make sure you copied the right part of your profile URL.`);
        }

        const pending = profile.pending;
        if (!pending) {
            return message.author.send('You don\'t have any pending registers');
        }

        profile.username = message.author.username;
        profile.guilds.push({ guild: pending.guild, enjin: args[0] });
        profile.pending = undefined;

        return profile.save()
            .then(async () => {
                logger.info(`Enjin id has been set for ${ message.author.username }. ID is ${ args[0] }. Unregister has been called successfully`);
                return message.author.send(`Your enjin profile id for the server \`${ pending.name }\` has been set to: \`${ args[0] }\``);
            })
            .catch((err) => {
                logger.error(err);
                return message.author.send('Ran into an error finalizing the data, everything is there but a value wasn\'t unset. Contact a mod about this');
            });
    }
}

export default CommandRegister;
