import {
    ClimateType,
    CloudType,
    PrecipFrequency,
    PrecipIntensity,
    Season,
    Times,
    WeatherType,
    WindType,
} from '../weather/WeatherConstants';
import { Moment } from 'moment';
import { IForecastTestData } from '../weather/climate/Forecast';
import { EmbedFieldData } from 'discord.js';

export interface IEmbedDescribed {
    description: string[];

    getEmbedDescription(additional?: IEmbedDescribed[]): EmbedFieldData[];
}

export interface IPrintable<T> {
    printable(): T;
}

export interface IForecast extends IPrintable<IForecast> {
    climateTypeData: IClimateBaseline;
    baseIntensity: PrecipIntensity;
    precipFrequency: PrecipFrequency;
    precipChance: IPrecipTypeFrequency;
    frequencyModifier: number;
    intensityModifier: number;
    intensity: PrecipIntensity;
    climateType: ClimateType;
    elevation: IElevationType;
    currentPrecipitation: MutatedPrecipResult;

    varianceObject(): IForecastTestData;

    dewPoint(): number;

    getCachedTemp(): number;

    getTemperature(curTime: Times): number;

    humidity(curTime: Times): number;

    getOrGeneratePrecip(curTime: Times, forceExpire: boolean): IBasePrecipitation;

    willHavePrecipitation(): boolean;

    generatePrecipChain(curTime: Times): IBasePrecipitation;

    getPrecipSetupData(curTime: Times): IPrecipSetupData;
}

export interface ITemperatureData extends IPrintable<ITemperatureData> {
    climateType: ClimateType;
    elevation: IElevationType;
    curClimateVarianceRoll: string;
    baseClimateTempMod: number;
    useClimateTempModUntil: Moment;
    useClimateVarRollUntil: Moment;
    lastRolledTemperature: number;
    dewPoint: number;

    humidity(time: Times): number;

    rollFinalTempModifiers(curTime: Times): void;

    shouldChooseNewVarianceTable(): boolean;

    rollClimateVarTable(): void;

    rollAllModifiers(curTime: Times): void;

    rollNightTempModifier(curTime: Times): number;

    getTemp(curTime: Times): number;

    getBaseTemp(season: Season): number;
}

// Precipitation Generator Def
export interface IPrecipitationGenerator {
    getPrecipitationForTemp(temp: number): IPrecipSetupData;

    getSpecificWeatherType(type: WeatherType, temperature: number): IPrecipSetupData;
}

// Wind Base Def
export interface IWindBase extends IEmbedDescribed {
    name: string;
    mphRoll: string;
    mph: number;
    strength: WindType;

    rollSpeed(): number;
}

export interface IPrecipitationSettings {
    canBeSleet?: boolean;
    canBeHail?: boolean;
}

// Precipitation Base Def
export interface IBasePrecipitation extends IEmbedDescribed {
    weatherType: WeatherType;
    name: string;
    windInstance: IWindBase;
    intensity: PrecipIntensity;
    imageName: string;
    duration: number;
    endTime: Moment;
    settings: IPrecipitationSettings;
    // Functions
    remainingHours: number;
    windSpeed: number;

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult;

    apply(applyReference: WeatherApplyReference): IBasePrecipitation;
}

export interface ISpecialWeather extends IBasePrecipitation {
    temperatureOffset: string;
    enabled: boolean;

    addExtra(header: string, text: string, inline: boolean): void;
}

export interface IPrecipitationHail extends IBasePrecipitation {
    hasThunderstormNext: boolean;
}

export interface IWeatherField {
    header: string;
    text: string;
    inline: boolean;
}

// Climte Definitions
export interface IClimateBaseline {
    frequencyModifier: number;
    intensityModifier: number;
    varianceTable: TemperatureVarRollTable;
}

export interface IPrecipTypeFrequency {
    precipChance: number;
}

export interface IElevationType {
    type: string;
    baseTemperatureModifier: number;
    basePrecipIntensity: PrecipIntensity;
    precipFrequencyModifier: number;
}

export interface IElevationData {
    SEA: IElevationType;
    LOW: IElevationType;
    HIGH: IElevationType;

    fromString(str: string): IElevationType;
}

// Dew Point Definitions
export interface IDewPointStats {
    month: string;
    monthId: number;
    average: number;
    stddev: number;
    averageDiff: number;
    averageStddev: number;
}


// Roll Table Definitions

export interface IWindTypeProps {
    mphRoll: string;
}

export interface ITemperatureVariation {
    variation: string;
    daysToUseVariationRoll: string;
}

export interface IPrecipRollData {
    canBeSleet?: boolean;
    canBeHail?: boolean;
    weatherType: WeatherType;
    weatherIntensity: PrecipIntensity;
    durationHoursRoll: string;
    alternateName?: string;
    imageName: string;
}

export interface IPrecipSetupData {
    weatherType: WeatherType;
    weatherIntensity: PrecipIntensity;
    durationHours: number;
    alternateName?: string;
    clouds: CloudType;
    imageName: string;
    canBeSleet?: boolean;
    canBeHail?: boolean;
}

export interface IHumidityRoll {
    humidity(table: IDewPointStats): number;
}

export type WeatherApplyReference = IPrecipSetupData | IBasePrecipitation;

export const isPrecipSetupData = (data: any): data is IPrecipSetupData => {
    return 'durationHours' in data;
};

export type WindTypeRollTable = { [range: string]: WindType };
export type TestingRollTable = { [range: string]: string | number };
export type CloudCoverRollTable = { [range: string]: CloudType };
export type TemperatureVarRollTable = { [range: string]: ITemperatureVariation };
export type PrecipitationRollTable = { [range: string]: IPrecipRollData };
export type HumidityRollTable = { [range: string]: IHumidityRoll };

export type MutatedPrecipitation = IBasePrecipitation | ISpecialWeather;
export type MutatedPrecipResult = MutatedPrecipitation | undefined;
export type RollTable =
    WindTypeRollTable
    | TestingRollTable
    | CloudCoverRollTable
    | TemperatureVarRollTable
    | PrecipitationRollTable
    | HumidityRollTable;
export type RollTableResult =
    WindType
    | CloudType
    | ITemperatureVariation
    | IPrecipRollData
    | IHumidityRoll
    | string
    | number
    | undefined;