import { Client, Collection } from 'discord.js';
import { ICommand } from '../base/Command';
import ChannelLogger from '../../util/channellogger';
import { PartialWordCache } from '../../db/schema/watchedword';

export interface IClientContext {
    client: Client;
    commands: Collection<string, ICommand>;
    words: Collection<string, PartialWordCache>;
    logger: ChannelLogger;
    config: IBotConfig;
}

export interface IBotConfig {
    paths: IPathCollection;
    auth: IAuthCollection;
    prefix: string;
    censusData?: string;

    loadConfig(path?: string): Promise<Partial<IPathCollection> | void>;

    loadObjects(): Promise<void>;
}

export interface IPathCollection {
    base: {
        linux: string;
        win32: string;
    };
    charSheetAuth: string;
    searchAuth: string;
    webAuth: string;
    clientAuth: string;
    dbAuth: string;
}

export interface IAuthCollection {
    charSheetAuth: IServiceAccountAuth;
    searchAuth: IServiceAccountAuth;
    webAuth: IWebsiteAuth;
    clientAuth: IClientAuth;
    dbAuth: IDatabaseAuth;
}

export type AuthType = 'charSheetAuth' | 'searchAuth' | 'webAuth' | 'clientAuth' | 'dbAuth';

export interface IClientAuth {
    token: string;
}

export interface IWebsiteAuth {
    website: string;
    webkey: string;
}

export interface IDatabaseAuth {
    user: string;
    pass: string;
    database: string;
    authSource: string;
}

export interface IServiceAccountAuth {
    type: string;
    project_id: string;
    private_key_id: string;
    private_key: string;
    client_email: string;
    client_id: string;
    auth_uri: string;
    token_uri: string;
    auth_provider_x509_cert_url: string;
    client_x509_cert_url: string;
}

export interface IExitOptions {
    cleanup?: boolean;
    exit?: boolean;
}

export interface IRawCharacterSheet {
    Player: string | undefined;
    Name: string;
    Race: string;
    Class: string;
    Gender: string | undefined;
    Alignment: string;
    Deity: string | undefined;
    Level: string;
    XPCurrent: string;
    CasterLevel: string;
    HP: string;
    AC: string;
    Age: string;
    Height: string;
    Weight: string;
    Str: string;
    StrMod: string;
    Dex: string;
    DexMod: string;
    Con: string;
    ConMod: string;
    Int: string;
    IntMod: string;
    Wis: string;
    WisMod: string;
    Cha: string;
    ChaMod: string;
    URL: string;
    DiscordName?: string;
}

export enum CommandNames {
    ADD = 'add',
    ADDRP = 'addrp',
    CENSUS = 'census',
    CLINK = 'clink',
    END = 'end',
    ENDPOLL = 'endpoll',
    FORCEWEATHER = 'forceweather',
    HELP = 'help',
    HISTORY = 'history',
    INFO = 'info',
    NOHORNY = 'nohorny',
    NUMCHARS = 'numchars',
    ONGOING = 'ongoing',
    PLAY = 'play',
    POLL = 'poll',
    PROFILE = 'profile',
    QUEST = 'quest',
    REGISTER = 'register',
    REMOVE = 'remove',
    ROLL = 'roll',
    SHEET = 'sheet',
    SOURCE = 'source',
    SPECIALWEATHER = 'specialweather',
    START = 'start',
    SUMMONWIND = 'summonwind',
    TESTSURVEY = 'testsurvey',
    UNREGISTER = 'unregister',
    WARN = 'warn',
    WEATHER = 'weather',
    WORD = 'word',
}

export enum HelpCategory {
    NONE = 'none',
    ADMIN = 'admin',
    ENCOUNTER = 'encounter',
    GENERAL = 'general',
    REGISTER = 'register',
}

export type HelpInfo = {
    name: string;
    description: string[];
    hidden?: boolean;
};

export const HelpCategoryInfo: { [T in HelpCategory]: HelpInfo } = {
    [HelpCategory.NONE]: {
        name: 'ABSOLUTELY FUCKING NONE',
        description: ['GO AWAY'],
        hidden: true,
    },
    [HelpCategory.ADMIN]: {
        name: 'Admin Commands',
        description: [
            'Server & Clonk related administrative commands.',
        ],
    },
    [HelpCategory.ENCOUNTER]: {
        name: 'Encounter Commands',
        description: [
            'Commands that relate to running, viewing, or modifying encounters & quests',
        ],
    },
    [HelpCategory.GENERAL]: {
        name: 'General Commands',
        description: [
            'Commands that don\'t really fit into another category',
        ],
    },
    [HelpCategory.REGISTER]: {
        name: 'Registration Commands',
        description: [
            'Commands related to registration of Enjin accounts or player sheets',
        ],
    },
};

export enum Reacts {
    TRASHCAN = '\u{1F5D1}',
    WEATHER = '\u{2601}',
    POLL = '\u{1F4F0}',
    CHECK = '\u{1F44C}',
    ERROR = '\u{26A0}',
    APPROVE = '\u{1F44D}',
    DENY = '\u{1F6AB}',
    LOOKING = '\u{1F440}',
    CLIP = '\u{1F4CE}',
}

export enum PendingSheetType {
    REQUEST,
    RETRAIN,
    RETIRE
}

export type AsyncForEachType = number | string | object;

export const intenseBap = '<a:intensebap:573232152322506760>';
// export const intenseBap = '<:flamingbap:572187367969128460>';
