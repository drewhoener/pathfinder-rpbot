import { AuthType, IAuthCollection, IBotConfig, IPathCollection } from './context';
import fs from 'fs';
import { promisify } from 'util';
import os from 'os';
import {
    validateClientAuth,
    validateDatabaseAuth,
    validateServiceAuth,
    validateWebAuth,
    ValidatorFunction,
} from '../../util/validators';
import logger from '../../util/logging';

const readFile = promisify(fs.readFile);
type basicKeyVal = { [key: string]: string | object };

class BotConfig implements IBotConfig {

    public auth: IAuthCollection;
    public paths: IPathCollection;
    public prefix: string;
    public censusData?: string;

    constructor() {
        this.auth = {} as IAuthCollection;
        this.paths = {} as IPathCollection;
        this.prefix = '!';
    }

    async loadConfig(configPath?: string): Promise<Partial<IPathCollection> | void> {
        logger.info('Loading config paths...');
        const requiredPaths = [
            'charSheetAuth',
            'searchAuth',
            'webAuth',
            'clientAuth',
            'dbAuth',
        ];
        return readFile(configPath ?? './config.json', 'utf8').then((data) => {
            const { paths: configData, prefix, censusData } = JSON.parse(data);
            if (!configData) {
                throw new Error('Need path data to continue!');
            }
            if (configData.base) {
                if (!(configData.base as basicKeyVal).linux || !(configData.base as basicKeyVal).win32) {
                    throw new Error('Both base types must be present!');
                }
            }
            for (const path of requiredPaths) {
                if (!configData[`${ path }`]) {
                    logger.error(JSON.stringify(configData, null, 4));
                    throw new Error(`Your config is missing the path ${ path }!`);
                }
            }
            this.paths = configData as IPathCollection;
            if (prefix) {
                this.prefix = `${ prefix }`;
            }
            if (censusData) {
                this.censusData = censusData;
            }
            return this.paths;
        });
    }

    getBase(): string {
        if (os.platform() === 'win32') {
            return this.paths.base.win32.replace('%appdata%', process.env.APPDATA as string);
        }
        return this.paths.base.linux;
    }

    loadObject(authType: AuthType, validator: ValidatorFunction): Promise<void> {
        const base = this.getBase();
        const path = `${ base }${ this.paths[authType] }`;
        return readFile(path, 'utf8').then((data) => {
            const dataObj = JSON.parse(data);
            const result = validator(dataObj);
            if (result) {
                this.auth[authType] = dataObj;
            }
        });
    }

    async loadObjects(): Promise<void> {
        await this.loadObject('charSheetAuth', validateServiceAuth)
            .then(() => logger.info('Loaded Character Sheet Auth!'))
            .catch(err => {
                logger.error('Failed to load the character sheet auth data!');
                logger.error(err);
            });
        await this.loadObject('searchAuth', validateServiceAuth)
            .then(() => logger.info('Loaded Search Engine Auth!'))
            .catch(err => {
                logger.error('Failed to load the search engine auth data!');
                logger.error(err);
            });
        await this.loadObject('webAuth', validateWebAuth)
            .then(() => logger.info('Loaded Web Token Auth!'))
            .catch(err => {
                logger.error('Failed to load the web auth data!');
                logger.error(err);
            });
        await this.loadObject('clientAuth', validateClientAuth)
            .then(() => logger.info('Loaded Client Auth!'))
            .catch(err => {
                logger.error('Failed to load the client auth data!');
                logger.error(err);
            });
        await this.loadObject('dbAuth', validateDatabaseAuth)
            .then(() => logger.info('Loaded Database Auth!'))
            .catch(err => {
                logger.error('Failed to load the db auth data!');
                logger.error(err);
            });
    }


}

export default BotConfig;