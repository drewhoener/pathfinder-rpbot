import { sample } from 'lodash';
import logger from '../../util/logging';
import { FILE_REGEX } from '../../util/utils';
import fs from 'fs';
import path from 'path';
import { clampWeatherType, clampWindType, PrecipIntensity, WeatherType, WindType } from '../weather/WeatherConstants';
import { IBasePrecipitation, ISpecialWeather, IWindBase } from '../types/weather';
import WindModerate from '../weather/wind/WindModerate';
import PrecipClear from '../weather/precipitation/PrecipClear';
import { getEntryFromRollTable } from '../../util/diceutil';
import { WindRollTable } from '../weather/WeatherBaselines';
import DraconicClouds from '../weather/supernatural/DraconicClouds';
// const JSON = require('circular-json');

export const GENERIC_WEATHER: { [key: string]: any } = {};
export const SPECIAL_WEATHER: { [key: string]: any[] } = {};
export const WIND: { [key: string]: any } = {};

// @formatter:off
export const getWindInstance = (windType: WindType): IWindBase => {
    windType = clampWindType(windType);
    const windTypeName: string = WindType[windType];
    logger.info(`Attempting to obtain instance of wind ${ windTypeName }`);
    if (!WIND.hasOwnProperty(windTypeName)) {
        return new WindModerate();
    }
    logger.info('\tInstance found, creating...');
    const elem = WIND[windTypeName];
    return new elem();
};

export const getWeatherInstance = (weatherType: WeatherType, windType: WindType = getEntryFromRollTable(WindRollTable, '1d100') as WindType, temp: number, intensity: PrecipIntensity): IBasePrecipitation => {
    weatherType = clampWeatherType(weatherType);
    const typeName = WeatherType[weatherType];
    logger.info(`Attempting to obtain instance of weather ${ typeName }`);
    if (!GENERIC_WEATHER.hasOwnProperty(typeName)) {
        return new PrecipClear(windType, temp);
    }
    logger.info('\tInstance found, creating...');
    const elem = GENERIC_WEATHER[typeName];
    const weatherInstance: IBasePrecipitation = new elem(windType, temp);
    weatherInstance.intensity = intensity;
    return weatherInstance;
};

export const getRandomSupernatural = (weatherType: WeatherType, windType: WindType, temp: number, intensity: PrecipIntensity): ISpecialWeather => {
    weatherType = clampWeatherType(weatherType);
    const typeName = WeatherType[weatherType];
    if (!SPECIAL_WEATHER.hasOwnProperty(typeName)) {
        return new DraconicClouds(windType, temp);
    }
    const arr = SPECIAL_WEATHER[typeName];
    const elem = sample(arr);
    const weatherInstance: ISpecialWeather = new elem(windType, temp);
    weatherInstance.intensity = intensity;
    return weatherInstance;
};
// @formatter:on

export const populateWeatherCache = async (): Promise<void> => {
    // Load wind instances
    logger.info('Populating Weather Cache');
    const windPath = path.resolve(__dirname, '../weather/wind');
    const windFiles = fs.readdirSync(windPath).filter(file => FILE_REGEX.test(file) && file.startsWith('Wind'));
    for (const file of windFiles) {
        try {
            const { default: WindModule } = await import(`${ windPath }/${ file }`);
            const wind = new WindModule;
            logger.info(`Populating Instance: ${ wind.name }`);
            const type = clampWindType(wind.strength);
            const typeName = WindType[type];
            if (!WIND.hasOwnProperty(typeName)) {
                console.log(`Loaded Wind Type ${ typeName }`);
                WIND[typeName] = WindModule;
            }
        } catch (err) {
            console.error(`Failed to load wind type ${ file }`);
            console.error(err);
        }
    }

    // Load weather files
    const precipPath = path.resolve(__dirname, '../weather/precipitation');
    // `${process.cwd()}/classes/weather/precipitation`;
    const precipWeather = fs.readdirSync(precipPath).filter(file => FILE_REGEX.test(file) && file.startsWith('Precip'));
    for (const file of precipWeather) {
        try {
            const { default: WeatherModule } = await import(`${ precipPath }/${ file }`);
            const weatherInstance = new WeatherModule(WindType.MODERATE, 55);
            const weatherName = WeatherType[weatherInstance.weatherType];
            if (!GENERIC_WEATHER.hasOwnProperty(weatherName)) {
                console.log(`Adding in weather type ${ weatherName }`);
                GENERIC_WEATHER[weatherName] = WeatherModule;
            }
        } catch (err) {
            console.error(`Failed to load weather type ${ file }`);
            console.error(err);
        }
    }

    // Load supernatural weather
    const supernaturalPath = path.resolve(__dirname, '../weather/supernatural');
    const specialWeatherFiles = fs.readdirSync(supernaturalPath).filter(file => FILE_REGEX.test(file));
    for (const file of specialWeatherFiles) {
        try {
            const { default: SupernaturalModule } = await import(`${ supernaturalPath }/${ file }`);
            const supernatural = new SupernaturalModule(WindType.MODERATE, 55);
            const weatherType = supernatural.weatherType;
            const typeName = WeatherType[weatherType];
            if (!SPECIAL_WEATHER.hasOwnProperty(typeName)) {
                console.log(`Creating array type for weather name ${ typeName }`);
                SPECIAL_WEATHER[typeName] = [];
            }
            console.log(`Pushing special weather: ${ supernatural.name }`);
            SPECIAL_WEATHER[typeName].push(SupernaturalModule);
        } catch (err) {
            console.error(`Failed to load supernatural type ${ file }`);
            console.error(err);
        }
    }
};