import { getEntryFromRollTable, rollDiceToInt } from '../../util/diceutil';
// eslint-disable-next-line @typescript-eslint/camelcase
import { CloudType, PrecipIntensity, WeatherType } from './WeatherConstants';
import { IPrecipitationGenerator, IPrecipRollData, IPrecipSetupData, PrecipitationRollTable } from '../types/weather';
import { ClientContext } from '../../handler/ClientContext';
import JSON from 'circular-json';

export const imagePath = './img/weather/{name}.jpg';
const RANDOM_ROLLS = 10;

export const LightUnfrozenPrecipTable: PrecipitationRollTable = {
    '1-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '21-40': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d6',
        imageName: 'fog',
    },
    '41-50': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.LIGHT,
        alternateName: 'Drizzle',
        durationHoursRoll: '1d4',
        imageName: 'rain',
    },
    '51-75': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.LIGHT,
        alternateName: 'Drizzle',
        durationHoursRoll: '2d12',
        imageName: 'rain',
    },
    '76-90': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1d4',
        imageName: 'rain',
    },
    '91-100': {
        canBeSleet: true,
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1',
        imageName: 'rain',
    },
};

export const LightFrozenPrecipTable: PrecipitationRollTable = {
    '1-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1d6',
        imageName: 'fog',
    },
    '21-40': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '41-50': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d4',
        imageName: 'fog',
    },
    '51-60': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1',
        imageName: 'snow',
    },
    '61-75': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '1d4',
        imageName: 'snow',
    },
    '76-100': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '2d12',
        imageName: 'snow',
    },
};

export const MediumUnfrozenPrecipTable: PrecipitationRollTable = {
    '1-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '11-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d12',
        imageName: 'fog',
    },
    '21-30': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d4',
        imageName: 'fog',
    },
    '31-35': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.MEDIUM,
        alternateName: 'Rain',
        durationHoursRoll: '1d4',
        imageName: 'rain',
    },
    '36-70': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.MEDIUM,
        alternateName: 'Rain',
        durationHoursRoll: '1d8',
        imageName: 'rain',
    },
    '71-90': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.MEDIUM,
        alternateName: 'Rain',
        durationHoursRoll: '2d12',
        imageName: 'rain',
    },
    '91-100': {
        canBeSleet: true,
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d4',
        imageName: 'rain',
    },
};

export const MediumFrozenPrecipTable: PrecipitationRollTable = {
    '1-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d6',
        imageName: 'fog',
    },
    '11-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '21-30': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d4',
        imageName: 'fog',
    },
    '31-50': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d4',
        imageName: 'snow',
    },
    '51-90': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d8',
        imageName: 'snow',
    },
    '91-100': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '2d12',
        imageName: 'snow',
    },
};

export const HeavyUnfrozenPrecipTable: PrecipitationRollTable = {
    '1-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '11-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'fog',
    },
    '21-50': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d12',
        imageName: 'rain',
    },
    '51-70': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d12',
        imageName: 'rain',
    },
    '71-85': {
        canBeSleet: true,
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d8',
        imageName: 'rain',
    },
    '86-90': {
        weatherType: WeatherType.THUNDERSTORM,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1',
        canBeHail: true,
        imageName: 'thunderstorm',
    },
    '91-100': {
        weatherType: WeatherType.THUNDERSTORM,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d3',
        canBeHail: true,
        imageName: 'thunderstorm',
    },
};

export const HeavyFrozenPrecipTable: PrecipitationRollTable = {
    '1-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '11-20': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'fog',
    },
    '21-60': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.LIGHT,
        durationHoursRoll: '2d12',
        imageName: 'snow',
    },
    '61-90': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.MEDIUM,
        durationHoursRoll: '1d8',
        imageName: 'snow',
    },
    '91-100': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d6',
        imageName: 'snow',
    },
};

export const TorrentialUnfrozenPrecipTable: PrecipitationRollTable = {
    '1-5': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '6-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'fog',
    },
    '11-30': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'rain',
    },
    '31-60': {
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d12',
        imageName: 'rain',
    },
    '61-80': {
        canBeSleet: true,
        weatherType: WeatherType.RAIN,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'rain',
    },
    '81-95': {
        weatherType: WeatherType.THUNDERSTORM,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d3',
        canBeHail: true,
        imageName: 'thunderstorm',
    },
    '96-100': {
        weatherType: WeatherType.THUNDERSTORM,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d6',
        canBeHail: true,
        imageName: 'thunderstorm',
    },
};

export const TorrentialFrozenPrecipTable: PrecipitationRollTable = {
    '1-5': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d8',
        imageName: 'fog',
    },
    '11-10': {
        weatherType: WeatherType.FOG,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d6',
        imageName: 'fog',
    },
    '21-50': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d4',
        imageName: 'snow',
    },
    '61-90': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '1d8',
        imageName: 'snow',
    },
    '91-100': {
        weatherType: WeatherType.SNOW,
        weatherIntensity: PrecipIntensity.HEAVY,
        durationHoursRoll: '2d12',
        imageName: 'snow',
    },
};

export const ClearWeather: IPrecipRollData = {
    weatherType: WeatherType.CLEAR,
    weatherIntensity: PrecipIntensity.LIGHT,
    durationHoursRoll: '2d10',
    imageName: 'clear',
};

class PrecipitationGenerator implements IPrecipitationGenerator {

    private frozenPrecipData: PrecipitationRollTable;
    private unfrozenPrecipData: PrecipitationRollTable;

    constructor(frozen: PrecipitationRollTable, unfrozen: PrecipitationRollTable) {
        this.frozenPrecipData = frozen;
        this.unfrozenPrecipData = unfrozen;
    }

    static generatePrecipitationForTemp(precipRollData: IPrecipRollData, temperature: number): IPrecipSetupData {

        let weatherType: WeatherType = precipRollData.weatherType;
        let imageName = precipRollData.imageName;
        let clouds: CloudType = CloudType.NONE;

        if (precipRollData.canBeSleet && temperature < 40) {
            weatherType = WeatherType.SLEET;
            imageName = 'snow';
        }

        if (weatherType !== WeatherType.CLEAR) {
            clouds = CloudType.OVERCAST;
        }

        return {
            weatherType,
            weatherIntensity: precipRollData.weatherIntensity,
            durationHours: rollDiceToInt(precipRollData.durationHoursRoll),
            alternateName: precipRollData.alternateName,
            clouds,
            imageName,
            canBeSleet: precipRollData.canBeSleet,
            canBeHail: precipRollData.canBeHail,
        };
    }

    static getClearWeather(temperature: number): IPrecipSetupData {
        return PrecipitationGenerator.generatePrecipitationForTemp(ClearWeather, temperature);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    static getWeatherName(obj: any): string {
        if (obj.hasOwnProperty('name')) {
            return obj.name;
        }
        const intensity = PrecipIntensity[obj.weatherIntensity];
        const type = WeatherType[obj.weatherType];
        return `${ intensity.charAt(0).toUpperCase() }${ intensity.slice(1) } ${ type.charAt(0).toUpperCase() }${ type.slice(1) }`;
    }

    getPrecipitationForTemp(temperature: number): IPrecipSetupData {
        const precipRollTable: PrecipitationRollTable = temperature > 32 ? this.unfrozenPrecipData : this.frozenPrecipData;
        const rollResult: IPrecipRollData = getEntryFromRollTable(precipRollTable, '1d100') as IPrecipRollData;
        if (rollResult) {
            ClientContext.logger.log(`Raw Data from generator:\n${ JSON.stringify(rollResult, null, 4) }`);
            ClientContext.logger.log(null);
            return PrecipitationGenerator.generatePrecipitationForTemp(rollResult, temperature);
        }
        return PrecipitationGenerator.getClearWeather(temperature);
    }

    getSpecificWeatherType(type: WeatherType, temperature: number): IPrecipSetupData {
        const precipRollTable: PrecipitationRollTable = temperature > 32 ? this.unfrozenPrecipData : this.frozenPrecipData;
        const matches = Object.values(precipRollTable).filter((val: IPrecipRollData) => val.weatherType === type);
        if (!matches) {
            return PrecipitationGenerator.getClearWeather(temperature);
        }
        let i = 0;
        while (++i < RANDOM_ROLLS) {
            const match = getEntryFromRollTable(precipRollTable, '1d100') as IPrecipRollData;
            if (match) {
                return PrecipitationGenerator.generatePrecipitationForTemp(match, temperature);
            }
        }
        return PrecipitationGenerator.generatePrecipitationForTemp(matches[Math.floor(Math.random() * matches.length)], temperature);
    }
}

export default PrecipitationGenerator;

