import WeatherFactory from '../../factory/WeatherFactory';
import ChainBase from './ChainBase';
import DewTemperateSea from '../dew/DewTemperateSea';
import { WeatherType } from '../WeatherBaselines';

class ChainRain extends ChainBase {
	constructor() {
		super(WeatherType.RAIN);
	}

	get curImage() {
		return 'rain';
	}

	mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll(), dew = new DewTemperateSea()) {
		if (temp < 32) {
			return WeatherFactory.newChain(WeatherType.SNOW, weather_generic).mutate(temp, weather_generic, wind);
		}
		if (weather_generic.can_sleet && temp < 40) {
			return WeatherFactory.newChain(WeatherType.SLEET, weather_generic).mutate(temp, weather_generic, wind);
		}

		this.weatherUntil = weather_generic.precip_duration;
		this.wind = wind;
		return this;
	}
}

export default ChainRain;