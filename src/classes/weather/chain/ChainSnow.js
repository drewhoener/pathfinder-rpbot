import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { PrecipIntensity, WeatherType } from '../WeatherBaselines';
import { WindType } from '../wind/AbstractBaseWind';
import { doesRollSucceed } from '../../../util/diceutil';

class ChainSnow extends ChainBase {
	constructor() {
		super(WeatherType.SNOW);
	}

	get curImage() {
		return 'snow';
	}

	mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll()) {
		if (temp > 40) {
			return WeatherFactory.newChain(WeatherType.RAIN, weather_generic).mutate(temp, weather_generic, wind);
		}
		if (weather_generic.can_sleet && temp > 32) {
			return WeatherFactory.newChain(WeatherType.SLEET, weather_generic).mutate(temp, weather_generic, wind);
		}

		if (weather_generic.weather_intensity >= PrecipIntensity.HEAVY && wind.strength >= WindType.SEVERE) {
            if (doesRollSucceed('1d100', 30)) {
                return WeatherFactory.newChain(WeatherType.BLIZZARD, weather_generic).mutate(temp, weather_generic, wind);
            }
        }

		this.weatherUntil = weather_generic.precip_duration;
		this.wind = wind;
		return this;
	}
}

export default ChainSnow;