import WeatherFactory from '../../factory/WeatherFactory';
import WindLight from '../wind/WindLight';
import { clampEnumType, PrecipIntensity, WeatherType } from '../WeatherBaselines';
import moment from 'moment';
import { PrecipGenerators } from '../climate/Forecast';
import DewTemperateSea from '../dew/DewTemperateSea';

class ChainBase {

	constructor(weatherType) {
		this._weatherType = weatherType;
		this._next = null;
		this._wind = new WindLight();
		this._weatherUntil = null;
		this._weatherObj = null;
	}

	get curImage() {
		return 'clear';
	}

	get weatherType() {
		return this._weatherType;
	}

	get next() {
		return this._next;
	}

	set next(obj) {
		this._next = obj;
	}

	get intensity() {
		if (this.weatherObj && this.weatherObj.weather_intensity) {
			return this.weatherObj.weather_intensity;
		}
		return PrecipIntensity.LIGHT;
	}

	set intensity(val) {
        if (!this.weatherObj) {
            this.weatherObj = {};
        }
        this.weatherObj.weather_intensity = clampEnumType(PrecipIntensity, val);
    }

	get weatherObj() {
		return this._weatherObj;
	}

	set weatherObj(obj) {
		this._weatherObj = Object.assign({}, this._weatherObj, obj);
	}

	get weatherUntil() {
		if (!this._weatherUntil) {
			console.log(`WeatherUntil is null`);
			this._weatherUntil = moment().subtract(10, 'seconds').valueOf();
		}
		return moment(this._weatherUntil);
	}

	set weatherUntil(duration) {
		if (isNaN(duration)) {
			return;
		}
		let end = moment().add(duration, 'hours');
		this._weatherUntil = end.valueOf();
	}

	get wind() {
		return this._wind;
	}

	set wind(wind) {
		this._wind = wind;
	}

	getPrecipObj(temp) {
		throw new Error('Must be implemented!');
	}

	static createNext(weather_obj, wind) {
		return {
			weather: weather_obj,
			wind,
		};
	}

	mutateInPlace(temp, dew) {
		return this;
	}

	mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll(), dew = new DewTemperateSea()) {
		throw new Error('Must be implemented');
	}

	mutateToNext(temp, intensity = PrecipIntensity.MEDIUM, wind = this._wind ? this._wind : WeatherFactory.getWindInstanceRoll(), generateNew) {
		let weather_generic = PrecipGenerators[intensity].getPrecip(temp);
		if (this.next) {
			return this.next.mutate(temp, weather_generic, wind);
		}
		return WeatherFactory.newChain(WeatherType.CLEAR, null).mutate(temp, weather_generic, wind);
	}
}

export default ChainBase;