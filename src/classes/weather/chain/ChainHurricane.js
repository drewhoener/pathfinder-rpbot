import ChainBase from './ChainBase';
import { WindType } from '../wind/AbstractBaseWind';
import WeatherFactory from '../../factory/WeatherFactory';
import { WeatherType } from '../WeatherBaselines';

class ChainHurricane extends ChainBase {
	constructor() {
		super(WeatherType.HURRICANE);
	}

	get curImage() {
		return 'hurricane';
	}

	mutate(temp, weather_generic, wind) {

		this.weatherUntil = weather_generic.precip_duration;
		this.wind = WeatherFactory.getWindInstanceType(WindType.HURRICANE);
		return this;
	}
}

export default ChainHurricane;