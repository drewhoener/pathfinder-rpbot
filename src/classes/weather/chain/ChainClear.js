import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import { rollDiceToInt } from '../../../util/diceutil';
import { WeatherType } from '../WeatherBaselines';

let jumps = 0;

class ChainClear extends ChainBase {
    constructor() {
        super(WeatherType.CLEAR);
    }

    mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll(), dewpoint) {
        if (weather_generic.weather_type !== this.weatherType) {
            return WeatherFactory.newChain(weather_generic.weather_type, weather_generic).mutate(temp, weather_generic, wind, dewpoint);
        }

        if (wind.strength >= WindType.WINDSTORM) {
            return WeatherFactory.newChain(WeatherType.WINDSTORM, weather_generic).mutate(temp, weather_generic, wind, dewpoint);
        }

        this.weatherUntil = weather_generic.precip_duration ? weather_generic.precip_duration : rollDiceToInt('2d10');
        this.wind = wind;
        return this;
    }
}

export default ChainClear;