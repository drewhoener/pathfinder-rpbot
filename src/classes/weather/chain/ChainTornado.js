import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import { WeatherType } from '../WeatherBaselines';

class ChainTornado extends ChainBase {
	constructor() {
		super(WeatherType.TORNADO);
	}

	get curImage() {
		return 'tornado';
	}

	mutate(temp, weather_generic, wind) {

		this.weatherUntil = weather_generic.precip_duration;
		this.wind = WeatherFactory.getWindInstanceType(WindType.TORNADO);
		return this;
	}
}

export default ChainTornado;