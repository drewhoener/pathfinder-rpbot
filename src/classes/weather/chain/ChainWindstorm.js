import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import { WeatherType } from '../WeatherBaselines';

class ChainWindstorm extends ChainBase {
	constructor() {
		super(WeatherType.WINDSTORM);
	}

	mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll()) {
		this.weatherUntil = weather_generic.precip_duration;
		this.wind = WeatherFactory.getWindInstanceType(WindType.WINDSTORM);
		return this;
	}
}

export default ChainWindstorm;