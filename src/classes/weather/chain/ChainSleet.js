import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WeatherType } from '../WeatherBaselines';

class ChainSleet extends ChainBase {
	constructor() {
		super(WeatherType.SLEET);
	}

	get curImage() {
		return 'snow';
	}

	mutate(temp, weather_generic, wind = WeatherFactory.getWindInstanceRoll()) {
		if (temp < 32) {
			return WeatherFactory.newChain(WeatherType.SNOW, weather_generic).mutate(temp, weather_generic, wind);
		}
		if (temp > 40) {
			return WeatherFactory.newChain(WeatherType.RAIN, weather_generic).mutate(temp, weather_generic, wind);
		}

		this.weatherUntil = weather_generic.precip_duration;
		this.wind = wind;
		return this;
	}
}

export default ChainSleet;