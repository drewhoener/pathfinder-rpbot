// @ts-nocheck
import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import { doesRollSucceed } from '../../../util/diceutil';
import PrecipThunderstorm from '../precipitation/PrecipThunderstorm';
import PrecipHail from '../precipitation/PrecipHail';
import logger from '../../../util/logging';
import { WeatherType } from '../WeatherBaselines';

class ChainThunderstorm extends ChainBase {
    constructor() {
        super(WeatherType.THUNDERSTORM);
        this.hailDuring = false;
    }

    get curImage() {
        return 'thunderstorm';
    }

    getPrecipObj(temp) {
        let arr = [
            new PrecipThunderstorm(this.wind, temp, this.wind.strength >= WindType.SEVERE),
        ];
        if (this.hailDuring) {
            arr.push(new PrecipHail(this.wind, temp));
        }
        return arr;
    }

    rollNext() {
        if (this.next) {
            return this.next;
        }
        let flag = doesRollSucceed('1d100', 25);
        if (flag) {
            this.next = WeatherFactory.newChain(WeatherType.RAIN, null);
        }
        return null;
    }

    mutate(temp, weather_generic, wind) {
        if (!wind) {
            return this;
        }
        if (wind.strength < WindType.WINDSTORM) {
            wind = WeatherFactory.getThunderstormWind();
        }
        if (wind.strength >= WindType.WINDSTORM) {
            logger.info(`Testing Wind Strength for more`);
            let choice = doesRollSucceed();
            let typeResult = false;
            if (choice) {
                logger.info(`Checking for tornado roll`);
                typeResult = doesRollSucceed('1d100', 10);//tornado
                if (typeResult) {
                    logger.info(`Generating Tornado`);
                    return WeatherFactory.newChain(WeatherType.TORNADO, weather_generic).mutate(temp, weather_generic, WeatherFactory.getWindInstanceType(WindType.TORNADO));
                }
            } else {
                logger.info(`Checking for hurricane roll`);
                typeResult = doesRollSucceed('1d100', 20);//hurricane
                if (typeResult && temp >= 85) {
                    this.next = WeatherFactory.newChain(WeatherType.HURRICANE, Object.assign({}, weather_generic));//TODO set wind to hurricane level
                }
            }
        }

        if (weather_generic.can_hail && doesRollSucceed('1d100', 40)) {
            let noHail = Object.assign({}, weather_generic, { can_hail: false });
            if (doesRollSucceed()) {//before
                let weather = Object.assign({}, weather_generic, { precip_duration: 1 });
                let chain = WeatherFactory.newChain(WeatherType.HAIL, weather);
                chain.next = ChainBase.createNext(noHail, wind);
                return chain.mutate(temp, weather, wind);
            } else {
                this.hailDuring = true;
                let next = Object.assign({}, this);
                next.__proto__ = ChainThunderstorm.prototype;
                next.hailDuring = false;
                this.next = next;
            }
        }

        this.weatherUntil = weather_generic.precip_duration;
        this.wind = wind;
        this.next = this.rollNext();
        return this;
    }
}

export default ChainThunderstorm;