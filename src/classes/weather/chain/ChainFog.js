import ChainBase from './ChainBase';
import { doesRollSucceed } from '../../../util/diceutil';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import moment from 'moment';
import { WeatherType } from '../WeatherBaselines';

class ChainFog extends ChainBase {
	constructor() {
		super(WeatherType.FOG);
	}

	get curImage() {
		return 'fog';
	}

	mutate(temp, weather_generic, wind, dew) {
        wind = WeatherFactory.getWindInstanceType(WindType.LIGHT);
        let time = moment();
        if (time.hour() > 10 && time.hour() < 20) {
            return WeatherFactory.newChain(WeatherType.CLEAR).mutate(temp, weather_generic, undefined, dew);
        }
        if (doesRollSucceed('1d100', 10)) {
            this.next = WeatherFactory.newChain(WeatherType.RAIN, Object.assign({}, weather_generic));//Don't want to think about stuff before we have to
        }

        this.weatherUntil = weather_generic.precip_duration;
        this.wind = wind;
        return this;
    }
}

export default ChainFog;