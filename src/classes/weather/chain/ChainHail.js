import ChainBase from './ChainBase';
import WeatherFactory from '../../factory/WeatherFactory';
import { WindType } from '../wind/AbstractBaseWind';
import { WeatherType } from '../WeatherBaselines';

class ChainHail extends ChainBase {
	constructor() {
		super(WeatherType.HAIL);
	}

	get curImage() {
		return 'thunderstorm';
	}

	mutateInPlace(temp, dew) {

		if (temp >= 32) {
			let weather = Object.assign({}, this.weatherObj);
			return WeatherFactory.newChain(WeatherType.RAIN, weather).mutate(temp, weather, undefined, dew);
		}

		return this;
	}

	mutate(temp, weather_generic, wind) {
		wind = WeatherFactory.getWindInstanceType(WindType.LIGHT);
		if (weather_generic['hail_before']) {
			this.next = WeatherFactory.newChain(WeatherType.THUNDERSTORM, null);//Don't want to think about stuff before we have to
		}

		this._weatherObj = weather_generic;
		this.weatherUntil = weather_generic.precip_duration;
		this.wind = wind;
		return this;
	}
}

export default ChainHail;