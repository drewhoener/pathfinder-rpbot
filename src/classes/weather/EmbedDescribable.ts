import { IEmbedDescribed } from '../types/weather';
import { EmbedFieldData } from 'discord.js';

class EmbedDescribable implements IEmbedDescribed {
    public title: string;
    public description: string[];
    public extraFields: EmbedFieldData[] = [];

    constructor(title: string, description: string[]) {
        this.title = title;
        this.description = description;
    }

    getEmbedDescription(additional?: IEmbedDescribed[]): EmbedFieldData[] {
        const strings: EmbedFieldData[] = [];
        let entry = '';
        let idx = 0;

        const pushField = (index: number, val: string): void => {
            const name = this.title;
            strings.push({
                name: index === 0 ? name : '\u200b',
                value: val.replace(/^\s+|\s+$/g, ''),
                inline: false,
            });
        };

        this.description.forEach((line: string) => {
            if ((entry.length + line.length) >= 1024) {
                pushField(idx++, entry);
                entry = line;
                return;
            }
            entry = `${ entry }\n${ line }`;
        });
        if (entry.length) {
            pushField(idx++, entry);
        }
        return [...strings, ...this.extraFields];
    }

}

export default EmbedDescribable;