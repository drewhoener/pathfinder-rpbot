import moment, {Moment} from 'moment';
import {
    CloudCoverRollTable,
    IClimateBaseline,
    IElevationData,
    IPrecipTypeFrequency,
    IWindTypeProps,
    WindTypeRollTable,
} from '../types/weather';
import {CloudType, PrecipIntensity, Season, WindType} from './WeatherConstants';

export const CloudRollTable: CloudCoverRollTable = {
    '1-50': CloudType.NONE,
    '51-70': CloudType.LIGHT,
    '71-85': CloudType.MEDIUM,
    '86-100': CloudType.OVERCAST,
};

export const ClimateBaseline: IClimateBaseline[] = [
    // Cold
    {
        frequencyModifier: -1,
        intensityModifier: -1,
        varianceTable: {
            '1-20': {
                variation: '-3d10',
                daysToUseVariationRoll: '1d4',
            },
            '21-40': {
                variation: '-2d10',
                daysToUseVariationRoll: '1d6+1',
            },
            '41-60': {
                variation: '-1d10',
                daysToUseVariationRoll: '1d6+2',
            },
            '61-70': {
                variation: '-1d6+1',
                daysToUseVariationRoll: '1d4+2',
            },
            '71-80': {
                variation: '1d5',
                daysToUseVariationRoll: '1d4+2',
            },
            '81-95': {
                variation: '1d10',
                daysToUseVariationRoll: '1d6+1',
            },
            '96-99': {
                variation: '2d10',
                daysToUseVariationRoll: '1d4',
            },
            '100': {
                variation: '3d10',
                daysToUseVariationRoll: '1d2',
            },
        },
    },
    // Temperate
    {
        frequencyModifier: 0,
        intensityModifier: 0,
        varianceTable: {
            '1-10': {
                variation: '-2d10',
                daysToUseVariationRoll: '1d2',
            },
            '11-25': {
                variation: '-1d10',
                daysToUseVariationRoll: '1d2',
            },
            '26-40': {
                variation: '-1d6+1',
                daysToUseVariationRoll: '1d3',
            },
            '41-55': {
                variation: '1d5',
                daysToUseVariationRoll: '1d3',
            },
            '56-85': {
                variation: '1d10',
                daysToUseVariationRoll: '1d4',
            },
            '86-100': {
                variation: '2d10',
                daysToUseVariationRoll: '1d2',
            },
        },
    },
    // Tropical
    {
        frequencyModifier: 1,
        intensityModifier: 1,
        varianceTable: {
            '1-20': {
                variation: '-3d10',
                daysToUseVariationRoll: '1d4',
            },
            '21-40': {
                variation: '-2d10',
                daysToUseVariationRoll: '1d6+1',
            },
            '41-60': {
                variation: '-1d10',
                daysToUseVariationRoll: '1d6+2',
            },
            '61-70': {
                variation: '-1d6+1',
                daysToUseVariationRoll: '1d5+2',
            },
            '71-80': {
                variation: '1d5',
                daysToUseVariationRoll: '1d5+2',
            },
            '81-95': {
                variation: '1d10',
                daysToUseVariationRoll: '1d6+1',
            },
            '96-99': {
                variation: '2d10',
                daysToUseVariationRoll: '1d4',
            },
            '100': {
                variation: '3d10',
                daysToUseVariationRoll: '1d2',
            },
        },
    },
];

export const PrecipFrequencyProps: IPrecipTypeFrequency[] = [
    {
        precipChance: 5,
    },
    {
        precipChance: 15,
    },
    {
        precipChance: 30,
    },
    {
        precipChance: 60,
    },
    {
        precipChance: 95,
    },
];

export const WindRollTable: WindTypeRollTable = {
    '1-50': WindType.LIGHT,
    '51-80': WindType.MODERATE,
    '81-90': WindType.STRONG,
    '91-95': WindType.SEVERE,
    '96-100': WindType.WINDSTORM,
};

export const ThunderstormWindRollTable: WindTypeRollTable = {
    '1-50': WindType.STRONG,
    '51-90': WindType.SEVERE,
    '91-100': WindType.WINDSTORM,
};

export const WindTypeProps: IWindTypeProps[] = [
    {
        mphRoll: '1d10',
    },
    {
        mphRoll: '1d10 + 10',
    },
    {
        mphRoll: '1d10 + 20',
    },
    {
        mphRoll: '1d20 + 30',
    },
    {
        mphRoll: '1d25 + 50',
    },
    {
        mphRoll: '1d100 + 75',
    },
    {
        mphRoll: '1d125 + 175',
    },
];

export const ElevationData: IElevationData = {
    SEA: {
        type: 'SEA',
        baseTemperatureModifier: 10,
        basePrecipIntensity: PrecipIntensity.HEAVY,
        precipFrequencyModifier: 0,
    },
    LOW: {
        type: 'LOWLAND',
        baseTemperatureModifier: 0,
        basePrecipIntensity: PrecipIntensity.MEDIUM,
        precipFrequencyModifier: 0,
    },
    HIGH: {
        type: 'HIGHLAND',
        baseTemperatureModifier: -10,
        basePrecipIntensity: PrecipIntensity.MEDIUM,
        precipFrequencyModifier: -1,
    },
    fromString(str) {
        str = str.toLowerCase().trim();
        switch (str) {
            case 'sea':
            case 'sea level':
            case 'sea-level':
                return ElevationData.SEA;
            case 'low':
            case 'lowland':
                return ElevationData.LOW;
            case 'high':
            case 'highland':
                return ElevationData.HIGH;
            default:
                return ElevationData.LOW;
        }
    },
};

const dateIsBefore = (dates: Moment | Moment[], check: Moment): boolean => {
    if (Array.isArray(dates)) {
        for (const date of dates) {
            if (check.isBefore(date, 'day')) {
                return true;
            }
        }
        return false;
    }
    return check.isBefore(dates, 'day');
};

const isAfterOrSameDate = (dates: Moment | Moment[], checkAgainst: Moment): boolean => {
    if (Array.isArray(dates)) {
        for (const date of dates) {
            if (checkAgainst.isAfter(date, 'day') || checkAgainst.isSame(date, 'day')) {
                return true;
            }
        }
        return false;
    }
    return checkAgainst.isAfter(dates, 'day') || checkAgainst.isSame(dates, 'day');
};

export const getSeasonFromDate = (date = moment()): Season => {
    const thisWinter = date.clone().year(date.year()).month(11).date(19);
    const lastWinter = thisWinter.clone().year(thisWinter.year() - 1);
    const thisSpring = date.clone().month(2).date(19);
    const nextSpring = thisSpring.clone().year(thisSpring.year() + 1);
    const summer = date.clone().month(5).date(21);
    const fall = date.clone().month(8).date(23);
    if ((isAfterOrSameDate(lastWinter, date) && dateIsBefore(thisSpring, date)) || (isAfterOrSameDate(thisWinter, date) && dateIsBefore(nextSpring, date))) {
        return Season.WINTER;
    }
    if (isAfterOrSameDate([thisSpring], date) && dateIsBefore(summer, date)) {
        return Season.SPRING;
    }
    if (isAfterOrSameDate(summer, date) && dateIsBefore(fall, date)) {
        return Season.SUMMER;
    }
    return Season.FALL;
};

