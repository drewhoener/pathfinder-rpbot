import AbstractBasePrecip from './AbstractBasePrecip';
import { PrecipIntensity, WeatherType, WindType } from '../WeatherConstants';
import { doesRollSucceed } from '../../../util/diceutil';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';
import PrecipitationGenerator from '../PrecipitationGenerator';
import PrecipClear from './PrecipClear';

const normal = [
    'These desert storms blow fine grains of sand. The combined effects of dust and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Duststorms make ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Duststorms automatically extinguish candles, torches, and similar unprotected flames. They cause protected flames, such as those of lanterns, to dance wildly and have a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 15 Fort save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
    'Duststorms leave behind a deposit of 1d6 inches of sand.',
];

const severe = [
    'Greater duststorms drive fine grains of sand in windstorm-force winds. The combined effects of dust and wind reduce visibility to zero, making Spot, Search, and Listen checks and all ranged weapon attacks impossible.',
    'Greater duststorms automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
    'In addition, greater duststorms deal 1d3 points of nonlethal damage each round to anyone caught out in the open without shelter and also pose a choking hazard. A character with a scarf or similar protection across her mouth and nose does not begin to choke until after a number of rounds equal to 10 x her Constitution score.',
    'Greater duststorms leave 2d3-1 feet of fine sand in their wake.',
];


class PrecipDuststorm extends AbstractBasePrecip {

    constructor(windType: WindType, temp: number) {
        super(WeatherType.WINDSTORM, 'Duststorm', normal, windType);

        if (doesRollSucceed('1d100', 10)) {
            this.name = `Greater ${ this.name }`;
            this.description = severe;
            this.intensity = PrecipIntensity.TORRENTIAL;
        }
        this.imageName = 'duststorm';
    }


    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        return new PrecipClear(WindType.MODERATE, temperature)
            .apply(PrecipitationGenerator.getClearWeather(temperature))
            .mutate(temperature, dewPoint, expired, generator);
    }
}

export default PrecipDuststorm;