import AbstractBasePrecip from './AbstractBasePrecip';
import { WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, ISpecialWeather, MutatedPrecipResult } from '../../types/weather';
import { getWeatherInstance } from '../../factory/WeatherCache';

class SpecialWeather extends AbstractBasePrecip implements ISpecialWeather {

    public temperatureOffset: string;
    public enabled: boolean;

    constructor(weatherType: WeatherType, name: string, description: string[], windType: WindType) {
        super(weatherType, name, description, windType);
        this.temperatureOffset = '+0';
        this.enabled = true;
        this.extraFields = [];
    }

    addExtra(header: string, text: string, inline: boolean): void {
        this.extraFields.push({
            name: header,
            value: text,
            inline,
        });
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        return getWeatherInstance(WeatherType.CLEAR, undefined, temperature, this.intensity)
            .apply(this)
            .mutate(temperature, dewPoint, expired, generator);
    }
}

export default SpecialWeather;