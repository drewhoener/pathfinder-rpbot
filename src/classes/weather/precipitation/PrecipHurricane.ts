import AbstractBasePrecip from './AbstractBasePrecip';
import { WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';

const description = [
    'The combined effects of torrential precipitation and high wind reduce visibility to zero, making Spot, Search, and Listen checks and all ranged weapon attacks impossible.',
    'All flames are automatically extinguished.',
    'Hurricane-force winds often fell trees. Creatures must succeed on a DC 20 Fort save or be blown away (Medium or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Large; prone), or checked (Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Large or smaller creatures), blown back 1d6 x 10 feet (Huge), or blown back 1d6 x 5 feet (Gargantuan).',
    'In addition, hurricanes are accompanied by floods. Most adventuring activity is impossible under such conditions.',
];

class PrecipHurricane extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.HURRICANE, 'Hurricane', description, windType);
        this.imageName = 'hurricane';
    }


    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        return this;
    }
}

export default PrecipHurricane;