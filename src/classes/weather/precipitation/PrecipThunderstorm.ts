import AbstractBasePrecip from './AbstractBasePrecip';
import {
    clampPrecipIntensity,
    clampWindType,
    getPrecipIntensityName,
    PrecipIntensity,
    WeatherType,
    WindType,
} from '../WeatherConstants';
import {IPrecipitationGenerator, MutatedPrecipResult} from '../../types/weather';
import {EmbedFieldData} from 'discord.js';
import PrecipHail from './PrecipHail';
import logger from '../../../util/logging';
import {doesRollSucceed, rollDiceToInt} from '../../../util/diceutil';
import {getWeatherInstance, getWindInstance} from '../../factory/WeatherCache';

const normal = [
    'The combined effects of precipitation and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Thunderstorms make ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Thunderstorms automatically extinguish candles, torches, and similar unprotected flames. They cause protected flames, such as those of lanterns, to dance wildly and have a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 15 Fort save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
    'In addition, thunderstorms are accompanied by lightning that can pose a hazard to characters without proper shelter (especially those in metal armor). Every 10 minutes during a thunderstorm, a bolt of lightning strikes an unsheltered creature at random (though this can strike wildlife as easily as PCs).',
    'A creature struck by this lightning must succeed a DC 18 Reflex saving throw or take 10d8 points of electricity damage (a successful saving throw halves the damage). Creatures in metal armor take a –4 penalty on the Reflex saving throw.',
];

const severe = [
    'The combined effects of torrential precipitation and high wind reduce visibility to zero, making Spot, Search, and Listen checks and all ranged weapon attacks impossible.',
    'Unprotected flames are automatically extinguished, and protected flames have a 75% chance of being doused.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
    'In addition, thunderstorms are accompanied by lightning that can pose a hazard to characters without proper shelter (especially those in metal armor). Every 10 minutes during a thunderstorm, a bolt of lightning strikes an unsheltered creature at random (though this can strike wildlife as easily as PCs).',
    'A creature struck by this lightning must succeed a DC 18 Reflex saving throw or take 10d8 points of electricity damage (a successful saving throw halves the damage). Creatures in metal armor take a –4 penalty on the Reflex saving throw.',
];


class PrecipThunderstorm extends AbstractBasePrecip {

    private hadHailBefore: boolean;
    private hailDuring?: PrecipHail;
    private inTornado?: boolean;
    private timeStorage?: number;

    constructor(windType: WindType, temp: number, hadHailBefore = false) {
        super(WeatherType.THUNDERSTORM, 'Thunderstorm', normal, AbstractBasePrecip.rollThunderstormWindType());
        this.imageName = 'thunderstorm';
        this.hadHailBefore = hadHailBefore;
    }

    set intensity(intensity: PrecipIntensity) {
        this._intensity = intensity;
        switch (this.intensity) {
            default:
            case PrecipIntensity.HEAVY:
                if (this.windType === WindType.STRONG || this.windType === WindType.SEVERE) {
                    this.description = normal;
                } else if (this.windType >= WindType.WINDSTORM) {
                    this.description = severe;
                }
                break;
            case PrecipIntensity.TORRENTIAL:
                if (this.windType === WindType.STRONG) {
                    this.description = normal;
                } else if (this.windType >= WindType.SEVERE) {
                    this.description = severe;
                }
                break;
        }

        this.name = `${ getPrecipIntensityName(this._intensity) } ${ this._baseName }`;
    }

    getEmbedDescription(): EmbedFieldData[] {
        if (this.hailDuring) {
            return super.getEmbedDescription([this.hailDuring]);
        }
        return super.getEmbedDescription();
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        if (this.windType < WindType.WINDSTORM) {
            this.windType = AbstractBasePrecip.rollThunderstormWindType();
        }
        if (expired) {
            if (this.inTornado) {
                this.imageName = 'thunderstorm';
                this.inTornado = false;
                this.duration = this.timeStorage ?? 0;
                this.windInstance = getWindInstance(clampWindType(this.windType - Math.floor(3 * Math.random())));
                return this;
            }
            // Random Chance and 20% chance with temp greater than 85
            if (this.windType >= WindType.WINDSTORM && doesRollSucceed() && doesRollSucceed('1d100', 20) && temperature >= 85) {
                const newWeather = getWeatherInstance(WeatherType.HURRICANE, WindType.HURRICANE, temperature, clampPrecipIntensity(this.intensity + 1))
                    .apply(this).mutate(temperature, dewPoint, expired, generator);
                if (newWeather) {
                    newWeather.duration = rollDiceToInt('1d3');
                    return newWeather;
                }
            }
            return;
        }
        if (this.windType >= WindType.WINDSTORM) {
            logger.info('Testing Wind Strength for more');
            const choice = doesRollSucceed();
            let typeResult = false;
            if (choice) {
                logger.info('Checking for tornado roll');
                // tornado
                typeResult = doesRollSucceed('1d100', 10);
                if (typeResult && !this.inTornado) {
                    logger.info('Generating Tornado');
                    this.windInstance = getWindInstance(WindType.TORNADO);
                    this.imageName = 'tornado';
                    this.inTornado = true;
                    this.timeStorage = this.remainingHours;
                    this.duration = rollDiceToInt('5d10') / 60.0;
                    return this;
                }
            }
        }

        if (this.settings.canBeHail && doesRollSucceed('1d100', 40)) {
            // before
            if (doesRollSucceed()) {
                const nextWeather: MutatedPrecipResult = new PrecipHail(this.windType, temperature, true)
                    .apply(this)
                    .mutate(temperature, dewPoint, expired, generator);
                if (!nextWeather) {
                    return this;
                }
                nextWeather.duration = 1;
                return nextWeather;
            } else {
                this.hailDuring = new PrecipHail(this.windType, temperature);
            }
        }

        return this;
    }
}

export default PrecipThunderstorm;