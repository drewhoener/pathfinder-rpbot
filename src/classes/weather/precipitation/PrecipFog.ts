import AbstractBasePrecip from './AbstractBasePrecip';
import { PrecipIntensity, WeatherType, WindType } from '../WeatherConstants';
import moment from 'moment';
import { doesRollSucceed, rollDiceToInt } from '../../../util/diceutil';
import PrecipClear from './PrecipClear';
import { getWeatherInstance } from '../../factory/WeatherCache';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';
import PrecipitationGenerator from '../PrecipitationGenerator';

const description = ['Whether in the form of a low-lying cloud or a mist rising from the ground, fog obscures all sight, including darkvision, beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance).'];

class PrecipFog extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.FOG, 'Fog', description, WindType.LIGHT);
        this.imageName = 'fog';
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        this.windType = WindType.LIGHT;
        if (expired) {
            if (doesRollSucceed('1d100', 10)) {
                const windModifier = Math.floor(Math.random() * 2);
                // Don't want to think about stuff before we have to
                const newWeather = getWeatherInstance(WeatherType.RAIN, this.windType + windModifier, temperature, PrecipIntensity.LIGHT);
                newWeather.duration = rollDiceToInt('1d4');
                return newWeather.mutate(temperature, dewPoint, false, generator);
            }
        }
        const time = moment();
        if (time.hour() > 10 && time.hour() < 20) {
            return new PrecipClear(this.windType, temperature)
                .apply(PrecipitationGenerator.getClearWeather(temperature))
                .mutate(temperature, dewPoint, expired, generator);
        }

        const setupData = generator?.getPrecipitationForTemp(temperature);
        if (setupData) {
            return getWeatherInstance(setupData?.weatherType, this.windType, temperature, this.intensity)
                .apply(setupData)
                .mutate(temperature, dewPoint, false, generator);
        }
        this.duration = 1;
        return this;
    }

}

export default PrecipFog;