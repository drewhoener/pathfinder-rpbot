import AbstractBasePrecip from './AbstractBasePrecip';
import { WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';

const description = [
    'Essentially frozen rain, sleet reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Sleet automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 75% chance to extinguish these lights.',
    'A day of sleet leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a sleet-covered square.',
];

class PrecipSleet extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SLEET, 'Sleet', description, windType);
        this.imageName = 'snow';
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        return undefined;
    }
}

export default PrecipSleet;