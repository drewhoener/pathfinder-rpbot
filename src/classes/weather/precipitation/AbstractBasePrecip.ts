import moment, { Moment } from 'moment';
import { getWindInstance } from '../../factory/WeatherCache';
import logger from '../../../util/logging';
import { PrecipIntensity, WeatherType, WindType } from '../WeatherConstants';
import {
    IBasePrecipitation,
    IPrecipitationGenerator,
    IPrecipitationSettings,
    isPrecipSetupData,
    IWindBase,
    MutatedPrecipResult,
    WeatherApplyReference,
} from '../../types/weather';
import { getEntryFromRollTable } from '../../../util/diceutil';
import { ThunderstormWindRollTable, WindRollTable } from '../WeatherBaselines';
import EmbedDescribable from '../EmbedDescribable';

abstract class AbstractBasePrecip extends EmbedDescribable implements IBasePrecipitation {

    public weatherType: WeatherType;
    public _baseName: string;
    public settings: IPrecipitationSettings = {};
    public windInstance: IWindBase;
    public imageName: string;

    constructor(weatherType: WeatherType, name: string, description: string[], windType: WindType) {
        super(name, description);
        logger.info(`\t${ this.constructor.name } instance being created`);
        this.weatherType = weatherType;
        this._name = name;
        this._baseName = name;
        this.description = description;
        this.windInstance = getWindInstance(windType);
        this._intensity = PrecipIntensity.LIGHT;
        this.imageName = 'clear';
        this._duration = 1;
        this._endTime = moment().add(1, 'h');
    }

    public _name: string;

    protected _intensity: PrecipIntensity;

    get intensity(): PrecipIntensity {
        return this._intensity;
    }

    set intensity(value: PrecipIntensity) {
        this._intensity = value;
    }

    get name(): string {
        return this._name;
    }

    set name(str: string) {
        this._name = str;
        this.title = str;
    }

    protected _duration: number;

    get duration(): number {
        return this._duration;
    }

    /**
     * Sets duration in hours
     * */
    set duration(duration: number) {
        logger.info(`Setting duration ${ duration } hours.`);
        this._duration = duration;
        if (this._duration <= 0) {
            // Subtract a bit of time so that it looks like we already ended in case it takes a couple ms to propogate
            this._endTime = moment().subtract(10, 'ms');
        } else {
            this._endTime = moment().add(this._duration, 'hours');
        }
    }

    protected _endTime: Moment;

    get endTime(): Moment {
        if (!this._endTime) {
            logger.error('\t\t\tMOMENT IS NULL FOR END TIME');
            return moment().subtract(10, 'ms');
        }
        return this._endTime.clone();
    }

    set endTime(time: Moment) {
        if (!time.isValid()) {
            return;
        }
        this._endTime = time.clone();
        this._duration = this.remainingHours;
    }

    get windType(): WindType {
        return this.windInstance.strength;
    }

    set windType(type: WindType) {
        if (type < WindType.LIGHT) {
            this.windInstance = getWindInstance(WindType.MODERATE);
            return;
        }
        this.windInstance = getWindInstance(type);
    }

    get remainingHours(): number {
        const now = moment().subtract('10', 'ms');
        const end = this.endTime;
        if (now.isAfter(end)) {
            return -1;
        }
        return end.diff(now, 'hours', true);
    }

    get windSpeed(): number {
        if (!this.windInstance) {
            return 0;
        }
        return this.windInstance.mph;
    }

    static rollThunderstormWindType(): WindType {
        return getEntryFromRollTable(ThunderstormWindRollTable, '1d100') as WindType;
    }

    static rollGenericWindType(roll = '1d100'): WindType {
        return getEntryFromRollTable(WindRollTable, roll) as WindType;
    }

    abstract mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult;

    apply(applyReference: WeatherApplyReference): IBasePrecipitation {
        if (isPrecipSetupData(applyReference)) {
            // weather template fields
            this.duration = applyReference.durationHours;
            this.intensity = applyReference.weatherIntensity;
            // Apply settings data
            this.settings.canBeHail = applyReference.canBeHail ?? false;
            this.settings.canBeSleet = applyReference.canBeSleet ?? false;
            // If we have an alternate name, use it
            if (applyReference.alternateName) {
                this.name = applyReference.alternateName;
            }
        } else {
            // copy apply fields
            this.duration = applyReference.remainingHours;
            this.intensity = applyReference.intensity;
        }
        return this;
    }
}

export default AbstractBasePrecip;
