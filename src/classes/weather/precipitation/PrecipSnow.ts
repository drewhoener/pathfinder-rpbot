import AbstractBasePrecip from './AbstractBasePrecip';
import {
    clampPrecipIntensity,
    getPrecipIntensityName,
    PrecipIntensity,
    WeatherType,
    WindType
} from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';
import { doesRollSucceed, rollDiceToInt } from '../../../util/diceutil';
import { getWeatherInstance, getWindInstance } from '../../factory/WeatherCache';
import { ClientContext } from '../../../handler/ClientContext';

const normal = [
    'Snow reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'A day of snowfall leaves 1d6 inches of snow on the ground. It costs 2 squares of movement to enter a snow-covered square.',
];

const medium = [
    'Medium snow reduces visibility by half, imposing a -4 penalty on Spot and Search checks. All sight, including darkvision, is obscured beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance). It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Medium snow has a 50% chance of extinguishing candles, torches, and similar unprotected flames.',
    'A day of medium snowfall leaves 1d4 feet of snow on the ground. It costs 4 squares of movement to enter a square covered with heavy snow. Heavy snow accompanied by strong or severe winds may result in snowdrifts 1d4 x 5 feet deep, especially in and around objects big enough to deflect the wind -- a cabin or a large tent, for instance.',
];

const heavy = [
    'The combined effects of precipitation and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Snowstorms make ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Snowstorms automatically extinguish candles, torches, and similar unprotected flames. They cause protected flames, such as those of lanterns, to dance wildly and have a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 15 Fort save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
    'In addition, snowstorms leave 1d6 inches of snow on the ground afterward. It costs 2 squares of movement to enter a snow-covered square.',
];


class PrecipSnow extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Snow', normal, windType);
        this.imageName = 'snow';
    }

    set intensity(intensity: PrecipIntensity) {
        this._intensity = intensity;
        switch (this.intensity) {
            case PrecipIntensity.LIGHT:
                this.description = normal;
                break;
            case PrecipIntensity.MEDIUM:
                this.description = medium;
                break;
            case PrecipIntensity.HEAVY:
            case PrecipIntensity.TORRENTIAL:
                this.description = heavy;
                break;
            default:
                break;
        }
        this.name = `${ getPrecipIntensityName(this._intensity) } ${ this._baseName }`;
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {

        ClientContext.logger.log('Mutating type: Snow');

        if (expired) {
            if (temperature > 32 || doesRollSucceed('1d100', 50)) {
                return getWeatherInstance(WeatherType.CLEAR, undefined, temperature, this.intensity)
                    .apply(this)
                    .mutate(temperature, dewPoint, expired, generator);
            }
            this.intensity = clampPrecipIntensity(this.intensity - 1);
            this.windInstance = getWindInstance(this.windType - 2);
            const newData = generator?.getSpecificWeatherType(WeatherType.SNOW, temperature);
            if (!newData) {
                this.duration = rollDiceToInt('1d2');
                return this.mutate(temperature, dewPoint, false, generator);
            }
            if (newData.weatherIntensity >= this.intensity) {
                newData.weatherIntensity = clampPrecipIntensity(this.intensity - 1);
            }
            return this.apply(newData).mutate(temperature, dewPoint, false, generator);
        }

        if (temperature > 40) {
            return getWeatherInstance(WeatherType.RAIN, undefined, temperature, this.intensity)
                .apply(this)
                .mutate(temperature, dewPoint, expired, generator);
        }
        if (this.settings.canBeSleet && temperature > 32) {
            return getWeatherInstance(WeatherType.SLEET, undefined, temperature, this.intensity)
                .apply(this)
                .mutate(temperature, dewPoint, expired, generator);
        }

        if (this.intensity >= PrecipIntensity.HEAVY && this.windType >= WindType.SEVERE) {
            if (doesRollSucceed('1d100', 30)) {
                return getWeatherInstance(WeatherType.BLIZZARD, this.windType, temperature, this.intensity)
                    .apply(this)
                    .mutate(temperature, dewPoint, expired, generator);
            }
        }

        return this;
    }
}

export default PrecipSnow;