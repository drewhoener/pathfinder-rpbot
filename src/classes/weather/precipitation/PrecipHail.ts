import AbstractBasePrecip from './AbstractBasePrecip';
import {WeatherType, WindType} from '../WeatherConstants';
import {IPrecipitationGenerator, IPrecipitationHail, MutatedPrecipResult} from '../../types/weather';
import PrecipThunderstorm from './PrecipThunderstorm';
import PrecipClear from './PrecipClear';
import PrecipRain from './PrecipRain';
import PrecipitationGenerator from '../PrecipitationGenerator';
import {rollDiceToInt} from '../../../util/diceutil';

const description = [
    'The sound of falling hail imposes a -4 penalty on Listen checks. Sometimes hail can become large enough to deal 1 point of lethal damage (per storm) to anything in the open.',
    'A day of hail leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a hail-covered square.',
];

class PrecipHail extends AbstractBasePrecip implements IPrecipitationHail {

    constructor(windType: WindType, temp: number, hasThunderstormNext = false) {
        super(WeatherType.HAIL, 'Hail', description, windType);
        this.imageName = 'rain';
        this._hasThunderstormNext = hasThunderstormNext;
    }

    private _hasThunderstormNext: boolean;

    get hasThunderstormNext(): boolean {
        return this._hasThunderstormNext;
    }

    set hasThunderstormNext(val) {
        if (val && !this.hasThunderstormNext) {
            this.windType = this.windType - 1;
        }
        this._hasThunderstormNext = val;
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        if (expired) {
            if (this.hasThunderstormNext) {
                const next = new PrecipThunderstorm(AbstractBasePrecip.rollThunderstormWindType(), temperature);
                next.duration = rollDiceToInt('1d5');
                return next.mutate(temperature, dewPoint, false, generator);
            }
            return new PrecipClear(AbstractBasePrecip.rollGenericWindType('1d70'), temperature)
                .apply(PrecipitationGenerator.getClearWeather(temperature))
                .mutate(temperature, dewPoint, false, generator);
        }

        if (!this.hasThunderstormNext && temperature >= 32) {
            const next = new PrecipRain(AbstractBasePrecip.rollGenericWindType(), temperature);
            return next.apply(this).mutate(temperature, dewPoint, expired, generator);
        }

        return this;
    }

}

export default PrecipHail;