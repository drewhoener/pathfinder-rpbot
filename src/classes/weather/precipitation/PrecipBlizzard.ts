import AbstractBasePrecip from './AbstractBasePrecip';
import { PrecipIntensity, WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';
import { getWeatherInstance } from '../../factory/WeatherCache';

const description = [
    'The combination of high winds, heavy snow (typically 1d3 feet), and bitter cold make blizzards deadly for all who are unprepared for them.',
    'Blizzards reduce visibility to zero, making Spot, Search, and Listen checks and all ranged weapon attacks impossible. All sight, including darkvision, is obscured beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance).',
    'Blizzards automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
    'In addition, blizzards leave 1d3 feet of heavy snow on the ground afterward. It costs 4 squares of movement to enter a square covered with heavy snow. A blizzard may result in snowdrifts 1d4 x 5 feet deep, especially in and around objects big enough to deflect the wind -- a cabin or a large tent, for instance.',
];

class PrecipBlizzard extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.BLIZZARD, 'Blizzard', description, windType);
        this.imageName = 'blizzard';
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        if (!expired && temperature > 40) {
            return getWeatherInstance(WeatherType.RAIN, this.windType, temperature, this.intensity)
                .apply(this)
                .mutate(temperature, dewPoint, expired, generator);
        }

        if (!expired) {
            return this;
        }

        const newWeather = getWeatherInstance(WeatherType.SNOW, this.windType - 2, temperature, PrecipIntensity.LIGHT);
        newWeather.remainingHours = 2;
        return newWeather.mutate(temperature, dewPoint, false, generator);
    }
}

export default PrecipBlizzard;