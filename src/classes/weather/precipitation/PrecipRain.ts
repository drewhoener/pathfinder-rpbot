import AbstractBasePrecip from './AbstractBasePrecip';
import { getPrecipIntensityName, PrecipIntensity, WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';
import { getWeatherInstance } from '../../factory/WeatherCache';

const lightIntensity = [
    'Rain reduces visibility to three-quarters, imposing a -2 penalty on Spot and Search checks.',
    'Light Rain automatically extinguishes candles, and other tiny unprotected flames (excluding torches).',
];

const mediumIntensity = [
    'Rain reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Rain automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 50% chance to extinguish these lights.',
];

const heavyIntensity = [
    'Heavy Rain reduces visibility to one-quarter of the normal range, imposing a -6 penalty on Spot and Search checks. It also imposes a -6 penalty on Listen checks and ranged weapon attacks.',
    'Heavy Rain automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 50% chance to extinguish these lights.',
];

class PrecipRain extends AbstractBasePrecip {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.RAIN, 'Rain', mediumIntensity, windType);
        this.imageName = 'rain';
    }

    set intensity(intensity: PrecipIntensity) {
        this._intensity = intensity;
        switch (intensity) {
            case PrecipIntensity.LIGHT:
                this.description = lightIntensity;
                break;
            default:
            case PrecipIntensity.MEDIUM:
                this.description = mediumIntensity;
                break;
            case PrecipIntensity.HEAVY:
            case PrecipIntensity.TORRENTIAL:
                this.description = heavyIntensity;
                break;
        }
        this.name = `${ getPrecipIntensityName(this._intensity) } ${ this._baseName }`;
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {

        if (expired) {
            const data = generator.getPrecipitationForTemp(temperature);
            return getWeatherInstance(data.weatherType, undefined, temperature, data.weatherIntensity)
                .apply(data)
                .mutate(temperature, dewPoint, expired, generator);
        }

        if (temperature < 32) {
            console.log(`**Rain thinks temperature should mutate to snow. Temp: ${ temperature }`);
            return getWeatherInstance(WeatherType.SNOW, undefined, temperature, this.intensity)
                .apply(this)
                .mutate(temperature, dewPoint, expired, generator);
        }
        if (this.settings.canBeSleet && temperature < 40) {
            return getWeatherInstance(WeatherType.SLEET, undefined, temperature, this.intensity)
                .apply(this)
                .mutate(temperature, dewPoint, expired, generator);
        }

        return this;
    }
}

export default PrecipRain;