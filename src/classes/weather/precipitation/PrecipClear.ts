import AbstractBasePrecip from './AbstractBasePrecip';
import { getWeatherInstance } from '../../factory/WeatherCache';
import { WeatherType, WindType } from '../WeatherConstants';
import { IPrecipitationGenerator, MutatedPrecipResult } from '../../types/weather';

const description = [
    'Beautifully clear weather with no discernable effects',
];

class PrecipClear extends AbstractBasePrecip {
    constructor(windType: WindType, temperature: number) {
        super(WeatherType.CLEAR, 'Clear Weather', description, windType);
    }

    mutate(temperature: number, dewPoint: number, expired: boolean, generator: IPrecipitationGenerator): MutatedPrecipResult {
        if (!expired && this.windType >= WindType.WINDSTORM) {
            const newChain = getWeatherInstance(WeatherType.WINDSTORM, this.windType, temperature, this.intensity);
            return newChain.apply(this).mutate(temperature, dewPoint, expired, generator);
        }

        return this;
    }
}

export default PrecipClear;
