// virginia
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 18.39775103705989,
        stddev: 17.890413764782004,
        averageDiff: 8.207656244643285,
        averageStddev: 7.128427014791299,
    },
    {
        month: 'February',
        monthId: 1,
        average: 20.548683315746278,
        stddev: 17.23710314713206,
        averageDiff: 9.310629616602181,
        averageStddev: 8.361200937990278,
    },
    {
        month: 'March',
        monthId: 2,
        average: 26.91403977415252,
        stddev: 15.740750062787646,
        averageDiff: 11.57583625617758,
        averageStddev: 10.736505493377463,
    },
    {
        month: 'April',
        monthId: 3,
        average: 35.30563567575667,
        stddev: 14.275606684599683,
        averageDiff: 14.287421336529817,
        averageStddev: 12.675971233274506,
    },
    {
        month: 'May',
        monthId: 4,
        average: 45.80712035650656,
        stddev: 13.445128054713997,
        averageDiff: 13.807582428811985,
        averageStddev: 13.223633333777508,
    },
    {
        month: 'June',
        monthId: 5,
        average: 54.528309747728436,
        stddev: 12.772780525089932,
        averageDiff: 13.417509678446205,
        averageStddev: 14.290205315718541,
    },
    {
        month: 'July',
        monthId: 6,
        average: 59.4145794617184,
        stddev: 10.951981957511915,
        averageDiff: 13.82552076110171,
        averageStddev: 13.697238885353332,
    },
    {
        month: 'August',
        monthId: 7,
        average: 58.47134828365199,
        stddev: 11.343384434971595,
        averageDiff: 13.439656914063217,
        averageStddev: 13.198204771638444,
    },
    {
        month: 'September',
        monthId: 8,
        average: 52.173591048961576,
        stddev: 13.11186121684516,
        averageDiff: 12.18947556626234,
        averageStddev: 12.366093401841482,
    },
    {
        month: 'October',
        monthId: 9,
        average: 41.575519513431324,
        stddev: 14.083392539317904,
        averageDiff: 10.654849395409457,
        averageStddev: 10.318235526272112,
    },
    {
        month: 'November',
        monthId: 10,
        average: 30.851226453574608,
        stddev: 15.387461217861826,
        averageDiff: 9.428572752892316,
        averageStddev: 8.46126991628969,
    },
    {
        month: 'December',
        monthId: 11,
        average: 22.168213199964875,
        stddev: 17.38764220326666,
        averageDiff: 7.619715895072699,
        averageStddev: 6.790855009603002,
    },
];

class DewTemperateHigh extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewTemperateHigh;