import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

// florida
const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 38.99273984898886,
        stddev: 15.508806342567,
        averageDiff: 9.057025186123871,
        averageStddev: 9.162579899542001,
    },
    {
        month: 'February',
        monthId: 1,
        average: 42.11285029033073,
        stddev: 14.487569651833594,
        averageDiff: 8.890543914275295,
        averageStddev: 9.499906474148794,
    },
    {
        month: 'March',
        monthId: 2,
        average: 47.20571002450197,
        stddev: 12.650219233442922,
        averageDiff: 10.188212421433898,
        averageStddev: 10.272466463435276,
    },
    {
        month: 'April',
        monthId: 3,
        average: 54.2548079542359,
        stddev: 10.415982269363136,
        averageDiff: 10.244156905475347,
        averageStddev: 9.928785681910142,
    },
    {
        month: 'May',
        monthId: 4,
        average: 62.160001028119055,
        stddev: 7.491514062843705,
        averageDiff: 8.807176271012183,
        averageStddev: 8.640556431865214,
    },
    {
        month: 'June',
        monthId: 5,
        average: 69.23816961227566,
        stddev: 4.453857844391036,
        averageDiff: 7.8781476914645845,
        averageStddev: 7.559105591744428,
    },
    {
        month: 'July',
        monthId: 6,
        average: 71.17862525567875,
        stddev: 3.8496876998564993,
        averageDiff: 8.054661427494887,
        averageStddev: 7.861381792003328,
    },
    {
        month: 'August',
        monthId: 7,
        average: 71.29053894105579,
        stddev: 4.353318850516075,
        averageDiff: 7.722796496488598,
        averageStddev: 8.038201408080294,
    },
    {
        month: 'September',
        monthId: 8,
        average: 67.05475579751871,
        stddev: 7.505257280524898,
        averageDiff: 7.21899701617547,
        averageStddev: 8.206746917535456,
    },
    {
        month: 'October',
        monthId: 9,
        average: 58.24837441006817,
        stddev: 11.20182413278158,
        averageDiff: 7.530912427897221,
        averageStddev: 8.573187422800112,
    },
    {
        month: 'November',
        monthId: 10,
        average: 49.30675343612454,
        stddev: 13.040920126134703,
        averageDiff: 7.52464018578025,
        averageStddev: 8.18104119203884,
    },
    {
        month: 'December',
        monthId: 11,
        average: 43.532187094640996,
        stddev: 14.953397707188028,
        averageDiff: 7.122567845969937,
        averageStddev: 7.922152111332433,
    },
];

class DewTropicalSea extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewTropicalSea;