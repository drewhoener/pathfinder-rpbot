// boston & milton
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 26.600529610014444,
        stddev: 18.932372860697118,
        averageDiff: 9.142501203659124,
        averageStddev: 8.003613116629102,
    },
    {
        month: 'February',
        monthId: 1,
        average: 28.693275164092285,
        stddev: 18.928636225973264,
        averageDiff: 9.736862791337499,
        averageStddev: 8.662458570684851,
    },
    {
        month: 'March',
        monthId: 2,
        average: 34.178942462191365,
        stddev: 17.560768241912744,
        averageDiff: 10.75900599786579,
        averageStddev: 9.65143404243615,
    },
    {
        month: 'April',
        monthId: 3,
        average: 42.74503206547175,
        stddev: 15.09711767381588,
        averageDiff: 11.290132436090415,
        averageStddev: 10.508191002742187,
    },
    {
        month: 'May',
        monthId: 4,
        average: 52.71126843797277,
        stddev: 12.027492291773031,
        averageDiff: 9.408342000756429,
        averageStddev: 9.39333884104757,
    },
    {
        month: 'June',
        monthId: 5,
        average: 61.44169845538115,
        stddev: 9.519100912801735,
        averageDiff: 8.285798261522107,
        averageStddev: 8.032504026117719,
    },
    {
        month: 'July',
        monthId: 6,
        average: 65.6115596015115,
        stddev: 7.358426926590547,
        averageDiff: 8.417738136133876,
        averageStddev: 7.887257536977934,
    },
    {
        month: 'August',
        monthId: 7,
        average: 65.63252856774638,
        stddev: 7.789588434567086,
        averageDiff: 8.052545014912916,
        averageStddev: 7.831393473331213,
    },
    {
        month: 'September',
        monthId: 8,
        average: 60.52060036408218,
        stddev: 10.251821483674721,
        averageDiff: 7.39606939852138,
        averageStddev: 7.686579055202814,
    },
    {
        month: 'October',
        monthId: 9,
        average: 50.12862813649441,
        stddev: 13.297829134387335,
        averageDiff: 7.8331330347062895,
        averageStddev: 7.906504666234226,
    },
    {
        month: 'November',
        monthId: 10,
        average: 40.24490129716536,
        stddev: 15.305954433402135,
        averageDiff: 8.30015978661567,
        averageStddev: 7.818312226048145,
    },
    {
        month: 'December',
        monthId: 11,
        average: 32.51529556446368,
        stddev: 17.3698872926349,
        averageDiff: 7.826398003949532,
        averageStddev: 7.299292833391995,
    },
];

class DewTemperateSea extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewTemperateSea;