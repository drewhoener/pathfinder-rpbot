import DewColdSea from './DewColdSea';
import DewColdLow from './DewColdLow';
import DewColdHigh from './DewColdHigh';
import DewTemperateSea from './DewTemperateSea';
import DewTemperateLow from './DewTemperateLow';
import DewTemperateHigh from './DewTemperateHigh';
import DewTropicalSea from './DewTropicalSea';
import DewTropicalLow from './DewTropicalLow';
import DewTropicalHigh from './DewTropicalHigh';
import DewPointBase from './DewPointBase';
import { ClimateType } from '../WeatherConstants';
import { IElevationType } from '../../types/weather';
import { ElevationData } from '../WeatherBaselines';

const humidityNameRollTable = {
    '0-15': 'Very Dry',
    '16-30': 'Dry',
    '31-50': 'Normal',
    '51-60': 'Slightly Humid',
    '61-70': 'Moderately Humid',
    '71-90': 'Very Humid',
    '91-100': 'Extremely Humid',
};

interface IDewPointInstance {
    climate: ClimateType;
    elevationType: IElevationType;
    instance: DewPointBase;
}

const DewPointInstances = [
    {
        climate: ClimateType.COLD,
        elevationType: ElevationData.SEA.type,
        instance: new DewColdSea(),
    },
    {
        climate: ClimateType.COLD,
        elevationType: ElevationData.LOW.type,
        instance: new DewColdLow(),
    },
    {
        climate: ClimateType.COLD,
        elevationType: ElevationData.HIGH.type,
        instance: new DewColdHigh(),
    },
    {
        climate: ClimateType.TEMPERATE,
        elevationType: ElevationData.SEA.type,
        instance: new DewTemperateSea(),
    },
    {
        climate: ClimateType.TEMPERATE,
        elevationType: ElevationData.LOW.type,
        instance: new DewTemperateLow(),
    },
    {
        climate: ClimateType.TEMPERATE,
        elevationType: ElevationData.HIGH.type,
        instance: new DewTemperateHigh(),
    },
    {
        climate: ClimateType.TROPICAL,
        elevationType: ElevationData.SEA.type,
        instance: new DewTropicalSea(),
    },
    {
        climate: ClimateType.TROPICAL,
        elevationType: ElevationData.LOW.type,
        instance: new DewTropicalLow(),
    },
    {
        climate: ClimateType.TROPICAL,
        elevationType: ElevationData.HIGH.type,
        instance: new DewTropicalHigh(),
    },
];

export function findDewTable(climate = ClimateType.TEMPERATE, elevation = ElevationData.SEA): DewPointBase {
    const result = DewPointInstances.find(table => table.climate === climate && table.elevationType === elevation.type);
    if (!result) {
        return DewPointInstances[3].instance;
    }
    return result.instance;
}

export { humidityNameRollTable };