//maine
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 28.455738983491486,
        stddev: 19.11032581234928,
        averageDiff: 9.039213137484293,
        averageStddev: 8.185823473687174,
    },
    {
        month: 'February',
        monthId: 1,
        average: 30.82370555208983,
        stddev: 19.04506146764335,
        averageDiff: 9.424033063006862,
        averageStddev: 8.726970375309701,
    },
    {
        month: 'March',
        monthId: 2,
        average: 36.361384835813915,
        stddev: 17.39488584087809,
        averageDiff: 10.397643205941694,
        averageStddev: 9.564207343440382,
    },
    {
        month: 'April',
        monthId: 3,
        average: 44.71924209254926,
        stddev: 14.867918025951269,
        averageDiff: 10.667547369215672,
        averageStddev: 10.039352923729725,
    },
    {
        month: 'May',
        monthId: 4,
        average: 54.26081611891372,
        stddev: 11.73009075758022,
        averageDiff: 8.902718512448073,
        averageStddev: 8.895590729053861,
    },
    {
        month: 'June',
        monthId: 5,
        average: 62.580715317811446,
        stddev: 9.354494516913045,
        averageDiff: 7.985684706022553,
        averageStddev: 7.868590890503432,
    },
    {
        month: 'July',
        monthId: 6,
        average: 66.40480618565759,
        stddev: 7.2283727854432565,
        averageDiff: 8.109347313544305,
        averageStddev: 7.791333988254402,
    },
    {
        month: 'August',
        monthId: 7,
        average: 66.45725852418046,
        stddev: 7.64176125628453,
        averageDiff: 7.807316253306636,
        averageStddev: 7.783255699385644,
    },
    {
        month: 'September',
        monthId: 8,
        average: 61.438463682643075,
        stddev: 10.271038006549214,
        averageDiff: 7.22622243739272,
        averageStddev: 7.754248102078462,
    },
    {
        month: 'October',
        monthId: 9,
        average: 51.46028101887505,
        stddev: 13.28554253325238,
        averageDiff: 7.608070211127417,
        averageStddev: 7.956080563905864,
    },
    {
        month: 'November',
        monthId: 10,
        average: 41.68424857667861,
        stddev: 15.30253226944151,
        averageDiff: 7.989727987468747,
        averageStddev: 7.804991936237473,
    },
    {
        month: 'December',
        monthId: 11,
        average: 34.24661292603592,
        stddev: 17.511022677161193,
        averageDiff: 7.581643331919106,
        averageStddev: 7.3851975135611365,
    },
];

class DewColdSea extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewColdSea;