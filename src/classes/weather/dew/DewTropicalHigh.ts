// utah
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 17.634040992427934,
        stddev: 18.140435066755654,
        averageDiff: 7.627660960672558,
        averageStddev: 6.629124996402163,
    },
    {
        month: 'February',
        monthId: 1,
        average: 19.944325707802317,
        stddev: 17.603812567880748,
        averageDiff: 8.683479775191957,
        averageStddev: 7.828620406343327,
    },
    {
        month: 'March',
        monthId: 2,
        average: 26.301112559583636,
        stddev: 15.947504315878492,
        averageDiff: 11.04378091134353,
        averageStddev: 10.500946109681028,
    },
    {
        month: 'April',
        monthId: 3,
        average: 34.47876468371295,
        stddev: 14.354680122495807,
        averageDiff: 14.071246198026424,
        averageStddev: 12.743711274847518,
    },
    {
        month: 'May',
        monthId: 4,
        average: 44.79686312409083,
        stddev: 13.561553162150895,
        averageDiff: 14.047033004645918,
        averageStddev: 13.593714129621231,
    },
    {
        month: 'June',
        monthId: 5,
        average: 53.63395764176857,
        stddev: 13.131634994973194,
        averageDiff: 13.692033173897826,
        averageStddev: 14.867237824558151,
    },
    {
        month: 'July',
        monthId: 6,
        average: 58.67446448999297,
        stddev: 11.303001961312432,
        averageDiff: 14.184791620750817,
        averageStddev: 14.227309159095379,
    },
    {
        month: 'August',
        monthId: 7,
        average: 57.69938346819966,
        stddev: 11.70503314536889,
        averageDiff: 13.8363014340613,
        averageStddev: 13.672203359695878,
    },
    {
        month: 'September',
        monthId: 8,
        average: 51.28896027537843,
        stddev: 13.439828304520963,
        averageDiff: 12.53030124861028,
        averageStddev: 12.76609241092139,
    },
    {
        month: 'October',
        monthId: 9,
        average: 40.87174261471578,
        stddev: 14.277551328127705,
        averageDiff: 10.58079134811841,
        averageStddev: 10.428526610904592,
    },
    {
        month: 'November',
        monthId: 10,
        average: 30.302732394660644,
        stddev: 15.615627937629712,
        averageDiff: 8.955496670743308,
        averageStddev: 8.175297040015892,
    },
    {
        month: 'December',
        monthId: 11,
        average: 21.36618699969483,
        stddev: 17.683658464882402,
        averageDiff: 7.110049531679147,
        averageStddev: 6.301941354994648,
    },
];

class DewTropicalHigh extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewTropicalHigh;