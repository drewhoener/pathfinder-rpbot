import { getEntryFromRollTable, randomFloat } from '../../../util/diceutil';
import logger from '../../../util/logging';
import { HumidityRollTable, IDewPointStats, IHumidityRoll } from '../../types/weather';
import { Times } from '../WeatherConstants';

const humidityRollTableDay: HumidityRollTable = {
    '1-33': {
        humidity: (table): number => table.average + (randomFloat() * 1.5 * table.stddev),
    },
    '34-67': {
        humidity: (table): number => table.average + (randomFloat() * 1.25 * table.stddev),
    },
    '68-100': {
        humidity: (table): number => table.average - (randomFloat() * 0.75 * table.stddev),
    },
};

const humidityRollTableNight: HumidityRollTable = {
    '1-33': {
        humidity: (table): number => table.average + (randomFloat() * 1.05 * table.stddev),
    },
    '34-67': {
        humidity: (table): number => table.average - (randomFloat() * 1.15 * table.stddev),
    },
    '68-100': {
        humidity: (table): number => table.average - (randomFloat() * 1.25 * table.stddev),
    },
};

export interface IDewPointBase {
    rollHumidityForMonth(month: number, timeOfDay: Times): number;
}

class DewPointBase implements IDewPointBase {

    protected dewTable: IDewPointStats[];

    constructor(data: IDewPointStats[] = []) {
        if (data.length < 12) {
            throw new Error('Must be a full year of data');
        }
        this.dewTable = data;
    }

    rollHumidityForMonth(month: number, timeOfDay: Times): number {
        logger.info(`Rolled dew table is ${ this.constructor.name }`);
        const dewTable = this.dewTable[month];
        const humidityRollTable = (timeOfDay === Times.DAWN ? humidityRollTableDay : humidityRollTableNight);
        logger.info(`Selected table ${ timeOfDay }, rolling...`);
        const rollResult = getEntryFromRollTable(humidityRollTable, '1d100') as IHumidityRoll;
        const result = rollResult.humidity(dewTable);
        logger.info(`Dew table roll for ${ month } returned result: ${ result }`);
        return result;
    }
}

export default DewPointBase;