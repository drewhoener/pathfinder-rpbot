//montana
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 23.457444176692107,
        stddev: 18.509621315172232,
        averageDiff: 8.966510596578676,
        averageStddev: 7.545602325968044,
    },
    {
        month: 'February',
        monthId: 1,
        average: 25.613776911933662,
        stddev: 18.29338089640607,
        averageDiff: 9.825785503771417,
        averageStddev: 8.318256233053107,
    },
    {
        month: 'March',
        monthId: 2,
        average: 31.073949422708363,
        stddev: 16.987180656100062,
        averageDiff: 11.448305520497359,
        averageStddev: 9.766455001588188,
    },
    {
        month: 'April',
        monthId: 3,
        average: 39.06437186610652,
        stddev: 15.222510837131164,
        averageDiff: 12.408572590169012,
        averageStddev: 10.839683634403869,
    },
    {
        month: 'May',
        monthId: 4,
        average: 48.81299020958469,
        stddev: 13.432163926277703,
        averageDiff: 11.357129736779266,
        averageStddev: 10.7544160295754,
    },
    {
        month: 'June',
        monthId: 5,
        average: 57.139418877739175,
        stddev: 12.090751213452197,
        averageDiff: 10.742629219153354,
        averageStddev: 10.263680009400979,
    },
    {
        month: 'July',
        monthId: 6,
        average: 60.90023117632449,
        stddev: 11.491429041962888,
        averageDiff: 12.42263235322214,
        averageStddev: 12.401504303409851,
    },
    {
        month: 'August',
        monthId: 7,
        average: 60.593994952777884,
        stddev: 12.204341059295126,
        averageDiff: 11.947453447023285,
        averageStddev: 12.255571083958099,
    },
    {
        month: 'September',
        monthId: 8,
        average: 55.53411130188478,
        stddev: 13.537923285820174,
        averageDiff: 10.293852884400858,
        averageStddev: 10.710831585155875,
    },
    {
        month: 'October',
        monthId: 9,
        average: 45.58204706656019,
        stddev: 14.8650025222125,
        averageDiff: 9.368152567053407,
        averageStddev: 8.982480867585688,
    },
    {
        month: 'November',
        monthId: 10,
        average: 35.72952736581751,
        stddev: 16.38470008293151,
        averageDiff: 8.951851174608063,
        averageStddev: 7.932216246532644,
    },
    {
        month: 'December',
        monthId: 11,
        average: 27.881949000961736,
        stddev: 18.226096165394,
        averageDiff: 8.003802007963543,
        averageStddev: 7.015919509271869,
    },
];

class DewColdHigh extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewColdHigh;