//north dakota
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 17.550736950980756,
        stddev: 18.681142467144905,
        averageDiff: 7.635624838387943,
        averageStddev: 6.57654277839199,
    },
    {
        month: 'February',
        monthId: 1,
        average: 19.811330318827224,
        stddev: 18.13690155527577,
        averageDiff: 8.297814003052908,
        averageStddev: 7.348899722019638,
    },
    {
        month: 'March',
        monthId: 2,
        average: 26.770559094891457,
        stddev: 16.28776665057989,
        averageDiff: 9.820573696905454,
        averageStddev: 9.051448584487884,
    },
    {
        month: 'April',
        monthId: 3,
        average: 35.589148902903005,
        stddev: 14.12399790842551,
        averageDiff: 12.440147553609794,
        averageStddev: 10.983776814451778,
    },
    {
        month: 'May',
        monthId: 4,
        average: 46.312483902610104,
        stddev: 12.688252163288203,
        averageDiff: 12.043012225796376,
        averageStddev: 11.11992645438222,
    },
    {
        month: 'June',
        monthId: 5,
        average: 55.786463155836465,
        stddev: 10.691466132618068,
        averageDiff: 10.726065527573455,
        averageStddev: 9.857473142865892,
    },
    {
        month: 'July',
        monthId: 6,
        average: 60.150036916842666,
        stddev: 9.954371692946532,
        averageDiff: 11.820297967570413,
        averageStddev: 11.126588636781124,
    },
    {
        month: 'August',
        monthId: 7,
        average: 59.038873499013086,
        stddev: 10.777804526263951,
        averageDiff: 11.86596208212695,
        averageStddev: 11.49503364956757,
    },
    {
        month: 'September',
        monthId: 8,
        average: 52.6373862715727,
        stddev: 12.709431713658452,
        averageDiff: 10.69107250059742,
        averageStddev: 10.495602222567152,
    },
    {
        month: 'October',
        monthId: 9,
        average: 41.81389681937815,
        stddev: 14.075969289605318,
        averageDiff: 9.355310701699603,
        averageStddev: 8.87938362954229,
    },
    {
        month: 'November',
        monthId: 10,
        average: 31.003330908098768,
        stddev: 15.852037566000963,
        averageDiff: 8.196767995998362,
        averageStddev: 7.39864113537432,
    },
    {
        month: 'December',
        monthId: 11,
        average: 21.67828386687538,
        stddev: 18.20200578206717,
        averageDiff: 6.941213181506604,
        averageStddev: 6.112292533756922,
    },
];

class DewColdLow extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewColdLow;