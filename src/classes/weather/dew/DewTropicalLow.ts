// arkansas
import DewPointBase from './DewPointBase';
import { IDewPointStats } from '../../types/weather';

const data: IDewPointStats[] = [
    {
        month: 'January',
        monthId: 0,
        average: 31.841136933242197,
        stddev: 13.988537952888402,
        averageDiff: 10.14542036910458,
        averageStddev: 9.653833399481707,
    },
    {
        month: 'February',
        monthId: 1,
        average: 35.05877817319099,
        stddev: 13.366901880598572,
        averageDiff: 9.463582443653618,
        averageStddev: 9.799389429561147,
    },
    {
        month: 'March',
        monthId: 2,
        average: 42.34545247321632,
        stddev: 12.346882735491766,
        averageDiff: 10.448484157738774,
        averageStddev: 10.562357385110897,
    },
    {
        month: 'April',
        monthId: 3,
        average: 51.00267045454545,
        stddev: 10.82214395056979,
        averageDiff: 10.238465909090909,
        averageStddev: 10.089796146747897,
    },
    {
        month: 'May',
        monthId: 4,
        average: 60.28211773417253,
        stddev: 7.881793115644773,
        averageDiff: 7.667847887025969,
        averageStddev: 7.995648856159887,
    },
    {
        month: 'June',
        monthId: 5,
        average: 67.26897307832867,
        stddev: 4.737423900876686,
        averageDiff: 8.192106224875136,
        averageStddev: 8.04939713948171,
    },
    {
        month: 'July',
        monthId: 6,
        average: 69.37841579390876,
        stddev: 4.310171591052702,
        averageDiff: 8.840195016251354,
        averageStddev: 8.985933651961648,
    },
    {
        month: 'August',
        monthId: 7,
        average: 68.84982342384068,
        stddev: 4.830553449187018,
        averageDiff: 8.92352226017484,
        averageStddev: 9.65548494701707,
    },
    {
        month: 'September',
        monthId: 8,
        average: 62.70402665926132,
        stddev: 7.974799791665333,
        averageDiff: 8.37067481255207,
        averageStddev: 9.90636001223896,
    },
    {
        month: 'October',
        monthId: 9,
        average: 53.14444750620005,
        stddev: 10.826425330077091,
        averageDiff: 7.910884541195922,
        averageStddev: 9.66693876931264,
    },
    {
        month: 'November',
        monthId: 10,
        average: 43.65573583831483,
        stddev: 12.70095565979327,
        averageDiff: 8.133959578707657,
        averageStddev: 8.894062259364874,
    },
    {
        month: 'December',
        monthId: 11,
        average: 35.8170420624152,
        stddev: 13.26355377799781,
        averageDiff: 7.905128900949796,
        averageStddev: 8.515029940546592,
    },
];

class DewTropicalLow extends DewPointBase {
    constructor() {
        super(data);
    }
}

export default DewTropicalLow;