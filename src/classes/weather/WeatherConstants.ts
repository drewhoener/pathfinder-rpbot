import { clamp } from 'lodash';

export enum WeatherType {
    CLEAR,
    FOG,
    RAIN,
    SLEET,
    SNOW,
    HAIL,
    THUNDERSTORM,
    WINDSTORM,
    TORNADO,
    HURRICANE,
    BLIZZARD,
}

export enum ClimateType {
    COLD,
    TEMPERATE,
    TROPICAL,
}

export enum Season {
    WINTER,
    SPRING,
    SUMMER,
    FALL,
}

export enum PrecipFrequency {
    DROUGHT,
    RARE,
    INTERMITTENT,
    COMMON,
    CONSTANT,
}

export enum PrecipIntensity {
    LIGHT,
    MEDIUM,
    HEAVY,
    TORRENTIAL,
}

export enum CloudType {
    NONE,
    LIGHT,
    MEDIUM,
    OVERCAST,
}

export enum WindType {
    LIGHT = 0,
    MODERATE = 1,
    STRONG = 2,
    SEVERE = 3,
    WINDSTORM = 4,
    HURRICANE = 5,
    TORNADO = 6,
}

export enum Times {
    DAWN = 'dawn',
    DUSK = 'dusk',
}

export const getWeatherName = (type: WeatherType) => {
    if (type === WeatherType.CLEAR) {
        return 'Clear Weather';
    }
    const name = WeatherType[type];
    return `${ name.charAt(0) }${ name.slice(1).toLowerCase() }`;
};

export const getPrecipIntensityName = (intensity: PrecipIntensity) => {
    const name = PrecipIntensity[intensity];
    return `${ name.charAt(0) }${ name.slice(1).toLowerCase() }`;
};

export const getWindTypeName = (windType: WindType) => {
    const name = WindType[windType];
    const formattedName = `${ name.charAt(0) }${ name.slice(1).toLowerCase() }`;
    switch (windType) {
        default:
        case WindType.LIGHT:
        case WindType.MODERATE:
            return `${ formattedName } Wind`;
        case WindType.STRONG:
        case WindType.SEVERE:
        case WindType.HURRICANE:
        case WindType.TORNADO:
            return `${ formattedName } Winds`;
        case WindType.WINDSTORM:
            return formattedName;

    }
};

export const clampClimateType = (val: ClimateType): ClimateType => {
    return clamp(val, ClimateType.COLD, ClimateType.TROPICAL);
};

export const clampWindType = (val: WindType): WindType => {
    return clamp(val, WindType.LIGHT, WindType.TORNADO);
};

export const clampWeatherType = (val: WeatherType): WeatherType => {
    return clamp(val, WeatherType.CLEAR, WeatherType.BLIZZARD);
};

export const clampPrecipIntensity = (val: PrecipIntensity): PrecipIntensity => {
    return clamp(val, PrecipIntensity.LIGHT, PrecipIntensity.TORRENTIAL);
};

export const clampPrecipFrequency = (val: PrecipFrequency): PrecipFrequency => {
    return clamp(val, PrecipFrequency.DROUGHT, PrecipFrequency.CONSTANT);
};