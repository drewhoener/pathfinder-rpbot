import { WindType } from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = ['A gentle breeze, having little or no game effect.'];

class WindLight extends AbstractBaseWind {
    constructor() {
        super(WindType.LIGHT, 'Light Wind', description);
    }
}

export default WindLight;