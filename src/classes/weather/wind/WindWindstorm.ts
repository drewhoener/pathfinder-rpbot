import {WindType} from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = [
    'Powerful enough to bring down branches if not whole trees, windstorms impose a -8 penalty on Listen checks due to the howling of the wind. Ranged weapon attacks are impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Windstorms automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Creatures must succeed on a DC 15 Strength save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
];

class WindWindstorm extends AbstractBaseWind {
    constructor() {
        super(WindType.WINDSTORM, 'Windstorm', description);
    }
}

export default WindWindstorm;