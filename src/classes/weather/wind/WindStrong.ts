import {WindType} from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = [
    'Strong gusts of wind that impose a -2 penalty on Listen checks and ranged weapon attacks. Such gusts automatically extinguish candles, torches, and similar unprotected flames.',
    'Tiny or smaller land-bound creatures must succeed on a DC 10 Strength save every round or be knocked prone. Small or smaller airborne creatures are instead blown back 1d6 x 10 feet.',
];

class WindStrong extends AbstractBaseWind {
    constructor() {
        super(WindType.STRONG, 'Strong Wind', description);
    }
}

export default WindStrong;