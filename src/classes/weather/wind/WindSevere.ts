import {WindType} from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = [
    'Winds of this magnitude impose a -4 penalty on Listen checks and ranged weapon attacks. Severe wind automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 10 Strength save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
];

class WindSevere extends AbstractBaseWind {
    constructor() {
        super(WindType.SEVERE, 'Severe Wind', description);
    }
}

export default WindSevere;