import {WindType} from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = [
    'While a tornado\'s rotational speed can be as great as 300 mph, the funnel itself moves forward at an average of 30 mph (roughly 250 feet per round).',
    'Listen checks are impossible, as all that characters can hear is the roaring of the wind. Ranged weapon attacks are impossible, even those using siege weapons.',
    'All flames are automatically extinguished.',
    'A tornado uproots trees, destroys buildings, and causes other similar forms of major destruction. Creatures must succeed on a DC 30 Fort save or be sucked toward the tornado (Large or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Huge; prone), or checked (Gargantuan or Colossal; unable to move forward against the force of the wind).',
    'Airborne creatures are instead sucked 2d6 x 10 feet toward the tornado and dealt 2d6 points of nonlethal damage due to battering and buffeting (Huge or smaller creatures), sucked 1d6 x 10 feet (Gargantuan), or sucked 1d6 x 5 feet (Colossal).',
    'Those who come in contact with the actual funnel cloud are picked up and whirled around for 1d10 rounds, taking 6d6 points of damage per round, before being violently expelled (falling damage may apply).',
];

class WindTornado extends AbstractBaseWind {
    constructor() {
        super(WindType.TORNADO, 'Tornado', description);
    }
}

export default WindTornado;