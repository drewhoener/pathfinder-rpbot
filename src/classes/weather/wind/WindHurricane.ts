import {WindType} from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = [
    'Listen checks are impossible, as all that characters can hear is the roaring of the wind. Ranged weapon attacks are impossible, except for those using siege weapons, which suffer a -8 penalty on attack rolls.',
    'All flames are automatically extinguished.',
    'Hurricane-force winds often fell trees. Creatures must succeed on a DC 15 Strength save or be blown away (Medium or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Large; prone), or checked (Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Large or smaller creatures), blown back 1d6 x 10 feet (Huge), or blown back 1d6 x 5 feet (Gargantuan).',
];

class WindHurricane extends AbstractBaseWind {
    constructor() {
        super(WindType.HURRICANE, 'Hurricane', description);
    }
}

export default WindHurricane;