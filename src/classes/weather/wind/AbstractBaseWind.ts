import { rollDiceToInt } from '../../../util/diceutil';
import { IWindBase } from '../../types/weather';
import { getWindTypeName, WindType } from '../WeatherConstants';
import { WindTypeProps } from '../WeatherBaselines';
import EmbedDescribable from '../EmbedDescribable';

class AbstractBaseWind extends EmbedDescribable implements IWindBase {

    public name: string;
    public mphRoll: string;
    private readonly windStrength: WindType;
    private curMph: number;

    constructor(windType: WindType, name: string, description: string[]) {
        super(getWindTypeName(windType), description);
        this.windStrength = windType;
        this.name = name;
        this.mphRoll = WindTypeProps[this.windStrength].mphRoll;
        this.curMph = rollDiceToInt(this.mphRoll);
    }

    get mph(): number {
        return this.curMph;
    }

    get strength(): WindType {
        return this.windStrength;
    }

    rollSpeed(): number {
        this.curMph = rollDiceToInt(this.mphRoll);
        return this.mph;
    }
}

export default AbstractBaseWind;