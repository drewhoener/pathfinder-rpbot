import { WindType } from '../WeatherConstants';
import AbstractBaseWind from './AbstractBaseWind';

const description = ['A steady wind with a 50% chance of extinguishing candles, torches, and similar unprotected flames.'];

class WindModerate extends AbstractBaseWind {
    constructor() {
        super(WindType.MODERATE, 'Moderate Wind', description);
    }
}

export default WindModerate;