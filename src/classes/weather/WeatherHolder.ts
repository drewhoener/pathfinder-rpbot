import Forecast from './climate/Forecast';
import { getTimes, GetTimesResult } from 'suncalc';
import moment, { Moment } from 'moment';
import { Job, scheduleJob } from 'node-schedule';
import { blankEmbed } from '../../util/embeds';
import { getChannel } from '../../util/utils';
import logger from '../../util/logging';
import { getEntryFromRollTable } from '../../util/diceutil';
import { humidityNameRollTable } from './dew/DewTables';
import { IClientContext, Reacts } from '../types/context';
import { EmbedFieldData, Snowflake } from 'discord.js';
import { ClimateType, Times, WeatherType, WindType } from './WeatherConstants';
import { IElevationType, IForecast, IPrintable, MutatedPrecipResult } from '../types/weather';
import { ElevationData } from './WeatherBaselines';
import { imagePath } from './PrecipitationGenerator';

export interface IUpdateTimes {
    cur: Times;
    next: Times;
    dawn: Moment;
    dusk?: Moment;
}

class WeatherHolder implements IPrintable<WeatherHolder> {

    private ctx: IClientContext;
    private readonly guild: Snowflake;
    private readonly channel: Snowflake;
    private forecast: IForecast;
    public weatherJob: Job | undefined;
    private chainExpireTime: Moment | undefined;
    private timeCheckJob: Job;

    constructor(ctx: IClientContext, guild: Snowflake, channel: Snowflake, climateType: ClimateType = ClimateType.TEMPERATE, elevation: IElevationType = ElevationData.LOW, json = null) {
        this.ctx = ctx;
        if (guild == null || channel == null) {
            throw new Error('Client, Guild or Channel may not be null!');
        }
        this.guild = guild;
        this.channel = channel;
        if (json) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            this.forecast = Forecast.fromJSON(json);
        } else {
            this.forecast = new Forecast(ctx, climateType, elevation);
        }

        this.sendEmbedCallback = this.sendEmbedCallback.bind(this);

        this.timeCheckJob = scheduleJob('*/30 * * * * *', () => {
            // logger.info('Checking to see if chain time is the same as stored time');
            const nextTime = this.getNextUpdateTime(WeatherHolder.getTimes());
            if (nextTime.valueOf() !== this.chainExpireTime?.valueOf()) {
                logger.info(`*** Next Time is ${ nextTime.valueOf() }, which is different from ${ this.chainExpireTime }`);
                logger.info(`*** Scheduling a job for the next embed to be sent at ${ nextTime.toLocaleString() }`);
                this.scheduleEmbedJob(nextTime);
            }
        });
    }

    static testNextTime(): { date: Date; temp_mod: number } {
        const date = new Date();
        date.setMinutes(date.getMinutes() + 2);
        return {
            date: date,
            // eslint-disable-next-line @typescript-eslint/camelcase
            temp_mod: 0,
        };
    }

    get weather(): IForecast {
        return this.forecast;
    }

    static getTimes(): IUpdateTimes {
        const dawnTimes: Moment[] = this.dawnTime().map(val => moment(val));
        const twilightTime = moment(this.twilightTime());
        const now = moment().add(10, 'seconds');
        if (now.isAfter(dawnTimes[0]) && now.isBefore(twilightTime)) {
            return {
                cur: Times.DAWN,
                next: Times.DUSK,
                dawn: dawnTimes[0],
                dusk: twilightTime,
            };
        }
        if (now.isAfter(twilightTime) && now.isBefore(dawnTimes[1])) {
            return {
                cur: Times.DUSK,
                next: Times.DAWN,
                dawn: dawnTimes[1],
                dusk: twilightTime,
            };
        }
        return {
            cur: Times.DUSK,
            next: Times.DAWN,
            dawn: dawnTimes[0],
        };
    }

    static getSunTimes(date = new Date()): GetTimesResult {
        return getTimes(date, 40.71, -74);
    }

    // save time to compare

    printable(): WeatherHolder {
        return Object.assign({}, this, { ctx: null, forecast: this.forecast.printable() });
    }

    static dawnTime(): [Date, Date] {
        const date = new Date();
        const todayTimes = this.getSunTimes();
        date.setDate(date.getDate() + 1);
        const tomorrowTimes = this.getSunTimes(date);

        return [todayTimes.goldenHourEnd, tomorrowTimes.goldenHourEnd];
    }

    static twilightTime() {
        const sunTimes = WeatherHolder.getSunTimes();
        return sunTimes.dusk;
    }

    scheduleEmbedJob(date: Moment): void {
        if (this.weatherJob) {
            this.weatherJob.cancel();
        }
        logger.info('Scheduling new embed job');
        this.chainExpireTime = date.clone();
        this.weatherJob = scheduleJob(date.toDate(), () => {
            logger.info(`Reached date & time for new embed to be sent: ${ date.toLocaleString() }`);
            this.sendEmbedCallback(true, false);
        });
    }

    // have second task checking every minute, if it's not the same, update the task
    getNextUpdateTime(times: IUpdateTimes): Moment {
        const chain = this.weather.currentPrecipitation;
        const now = moment();
        const nextSunTime: Times = times.next;
        let nextSunMoment: Moment = times[nextSunTime] as Moment;
        const endTime = chain?.endTime ?? moment().subtract(10, 'ms');
        if (!chain || !endTime) {
            return nextSunMoment;
        }
        if (nextSunMoment.isAfter(endTime) && now.isBefore(endTime)) {
            nextSunMoment = endTime;
        }
        return nextSunMoment;
    }

    getChain = (times: IUpdateTimes, withMutate = true, forceExpire = false): MutatedPrecipResult => {
        if (!this.forecast.currentPrecipitation) {
            return this.weather.getOrGeneratePrecip(times.cur, forceExpire);
        }
        return withMutate ?
            this.weather.getOrGeneratePrecip(times.cur, forceExpire) :
            this.weather.currentPrecipitation;
    };

    sendEmbedCallback(withMutate = true, forceExpire = false) {
        const { user } = this.ctx.client;
        if (!user) {
            return;
        }
        const times = WeatherHolder.getTimes();
        const temp = this.weather.getTemperature(times.cur);
        const celsius = Math.round(((temp - 32) * (5 / 9)));
        const chain = this.getChain(times, withMutate, forceExpire);
        if (!chain) {
            return;
        }
        logger.info(JSON.stringify(chain, null, 4));
        const image = imagePath.replace('{name}', chain.imageName);
        const mph = chain.windSpeed;
        const kmh = Math.round((mph * 1.609));

        const nextMoment = times[times.next] as Moment;
        const weatherUntil = chain.endTime;
        const date = this.getNextUpdateTime(times);
        this.scheduleEmbedJob(date);

        const channel = getChannel(this.ctx.client, this.guild, this.channel);
        if (!channel) {
            return;
        }

        let weatherName = WeatherType[chain.weatherType];
        weatherName = `${ weatherName.charAt(0) }${ weatherName.slice(1).toLowerCase() }`;
        let windName = WindType[chain.windInstance.strength];
        windName = `${ windName.charAt(0) }${ windName.slice(1).toLowerCase() }`;
        const humidityRoll = this.weather.humidity(times.cur).toString();
        const humidityStr = getEntryFromRollTable(humidityNameRollTable, humidityRoll.toString());
        logger.info(`Humidity Roll ${ humidityRoll }`);
        logger.info(`Roll From table: ${ JSON.stringify(getEntryFromRollTable(humidityNameRollTable, humidityRoll), null, 4) }`);

        const embed = blankEmbed();
        embed.attachFiles(
            [
                { attachment: image, name: 'footer.jpg' },
            ],
        );
        embed.setThumbnail('attachment://footer.jpg')
            .setFooter(`Debug:\nNext weather update at: ${ date.toLocaleString() }.\nNext update due to time based temp is ${ nextMoment.toLocaleString() }\nCurWeather lasts until ${ weatherUntil.toLocaleString() }`);

        console.log(`Getting description for weather ${ chain.constructor.name }`);
        chain.getEmbedDescription().forEach((data: EmbedFieldData) => {
            embed.addField(data.name, data.value, data.inline);
        });

        const windDataString = `${ mph } mph -- ${ kmh } kmh`;
        let addedWindData = false;
        chain.windInstance.getEmbedDescription().forEach((line: EmbedFieldData, index: number) => {
            let val = line.value;
            if (index === 0 && (val.length + windDataString.length + 1) <= 1024) {
                val = `${ windDataString }\n${ val }`;
                addedWindData = true;
            }
            embed.addField(line.name, val, line.inline);
        });
        if (!addedWindData) {
            embed.addField('Wind Speed', windDataString, true);
        }

        embed
            .addField('Temperature', `${ temp }°F\n${ celsius }°C`, true);
        // .addField('Humidity', `${ humidityStr }\n(Dew Point: ${ this.weather.dewPoint().toFixed(2) })`, true);

        channel.send(embed)
            .then(message => {
                return message.react(Reacts.WEATHER);
            });
    }

    startWeather() {
        if (this.weatherJob != null) {
            return;
        }
        this.sendEmbedCallback(true, false);
    }
}

export default WeatherHolder;