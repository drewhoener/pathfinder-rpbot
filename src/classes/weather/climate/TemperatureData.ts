import { clamp, find, isEqual } from 'lodash';
import moment, { Moment } from 'moment';
import { getEntryFromRollTable, rollDiceToInt } from '../../../util/diceutil';
import { findDewTable } from '../dew/DewTables';
import { clampClimateType, ClimateType, Season, Times } from '../WeatherConstants';
import JSON from 'circular-json';
import { IClientContext } from '../../types/context';
import {
    IClimateBaseline,
    IElevationType,
    ITemperatureData,
    ITemperatureVariation,
    TemperatureVarRollTable,
} from '../../types/weather';
import { ClimateBaseline, ElevationData, getSeasonFromDate } from '../WeatherBaselines';

class TemperatureData implements ITemperatureData {

    // The roll modifier to the total temperature
    private _climateType: ClimateType;
    public lastRolledTemperature = 55;
    // Default dew point
    public dewPoint: number;
    private readonly ctx: IClientContext;
    private _climateBaseline: IClimateBaseline;
    // Table with variations to temperature based on climate type. Contains variation and duration of variation
    private climateVariationTable: TemperatureVarRollTable;
    // Roll for nighttime
    private readonly nightVariationRoll: string;
    // Current numerical mod to the temperature based on variance roll
    private curClimateTempMod: number;
    // Current nighttime temp modifier from variance roll
    private curNightTempMod: number | null;

    constructor(ctx: IClientContext, climate: ClimateType, elevation: IElevationType) {
        this.ctx = ctx;
        this._climateType = climate;
        this._climateBaseline = ClimateBaseline[this._climateType];
        this._elevation = elevation;

        this._baseClimateTempMod = 0;
        this.climateVariationTable = this._climateBaseline.varianceTable;
        this.nightVariationRoll = '-2d6-5';
        this._curClimateVarianceRoll = '0';
        this._useClimateVarRollUntil = moment().subtract(10, 'ms');
        this.curClimateTempMod = 0;
        this._useClimateTempModUntil = moment().subtract(1, 'second');
        this.curNightTempMod = null;
        this.dewPoint = 55;

        this.resetMods();
    }

    get useClimateVarRollUntil(): Moment {
        if (!this._useClimateVarRollUntil) {
            this._useClimateVarRollUntil = moment().subtract(10, 'ms');
        }
        return this._useClimateVarRollUntil.clone();
    }

    get climateType(): ClimateType {
        return this._climateType;
    }

    set climateType(type: ClimateType) {
        if (isNaN(type)) {
            return;
        }
        this._climateType = clampClimateType(type);
        this.resetMods();
    }

    private _elevation: IElevationType;

    get elevation(): IElevationType {
        return this._elevation;
    }

    set elevation(level: IElevationType) {
        if (!find(ElevationData, (elem: IElevationType) => isEqual(level, elem))) {
            console.log('Couldn\'t find elevation');
            return;
        }
        this._elevation = level;
        this.resetMods();
    }

    // Modifier to temperature based on climate type. Assigned from `climate`
    private _baseClimateTempMod: number;

    get baseClimateTempMod(): number {
        return this._baseClimateTempMod;
    }

    // The roll modifier to the total temperature
    private _curClimateVarianceRoll: string;

    get curClimateVarianceRoll(): string {
        return this._curClimateVarianceRoll;
    }

    // How many days to use the above variance roll
    private _useClimateVarRollUntil: Moment;

    get useClimateTempModUntil(): Moment {
        if (!this._useClimateTempModUntil) {
            this._useClimateTempModUntil = moment().subtract(10, 'ms');
        }
        return this._useClimateTempModUntil.clone();
    }

    // 1 day until modified otherwise
    private _useClimateTempModUntil: Moment;

    printable(): ITemperatureData {
        return Object.assign({}, this, { ctx: null });
    }

    resetMods(): void {
        this._baseClimateTempMod = this._elevation.baseTemperatureModifier;
        this._climateBaseline = ClimateBaseline[this.climateType];
        this.climateVariationTable = this._climateBaseline.varianceTable;
    }

    logger(message: string | null): void {
        this.ctx.logger.log(message);
    }

    humidity(cur = Times.DAWN): number {
        return clamp(Math.trunc(Math.round(100 - ((25.0 / 9.0) * (this.getTemp(cur) - this.dewPoint)))), 0, 100);
    }

    rollFinalTempModifiers(curTime: Times): void {
        this.logger('Starting rolling modifiers...');
        this.logger(`Rolling ${ this._curClimateVarianceRoll } into a modifier`);
        this.curClimateTempMod = rollDiceToInt(this._curClimateVarianceRoll);
        this.logger(`Roll spit out ${ this.curClimateTempMod }`);
        this._useClimateTempModUntil = moment().add(1, 'hours');
        const dewTable = findDewTable(this._climateType, this._elevation);
        this.dewPoint = dewTable.rollHumidityForMonth(moment().month(), curTime);
        this.logger('Finished rolling modifiers');
    }

    /**
     * @return boolean If the variance roll needs to be chosen again
     * */
    shouldChooseNewVarianceTable(): boolean {
        const end = this.useClimateVarRollUntil;
        this.logger(`Is temperature variance roll expired: ${ moment().isAfter(end) }`);
        return moment().isAfter(end);
    }

    rollClimateVarTable(): void {
        if (!this.climateVariationTable) {
            return;
        }
        if (!this.shouldChooseNewVarianceTable()) {
            this.logger(`No need for new variance table roll, time not up. Diff is ${ this.useClimateVarRollUntil.diff(moment(), 'minutes') } minutes`);
            return;
        }
        const choiceFromVarianceTable: ITemperatureVariation = getEntryFromRollTable(this.climateVariationTable, '1d100') as ITemperatureVariation;
        if (!choiceFromVarianceTable) {
            return;
        }
        this.logger(`Rolled Variance Table Data:\n ${ JSON.stringify(choiceFromVarianceTable, null, 4) }`);
        const varRollDuration = rollDiceToInt(choiceFromVarianceTable.daysToUseVariationRoll);
        this.logger(`The new roll will be used for ${ varRollDuration } days`);
        this._curClimateVarianceRoll = choiceFromVarianceTable.variation;
        const endDate = moment().add(varRollDuration, 'days');
        this.logger(`The new roll expires at ${ endDate.toString() }`);
        this._useClimateVarRollUntil = endDate;
    }

    rollAllModifiers(curTime: Times): void {
        const date = moment().add(5, 'seconds');
        const end = this.useClimateTempModUntil;

        if (this.shouldChooseNewVarianceTable()) {
            this.rollClimateVarTable();
        }

        if (date.isAfter(end)) {
            this.logger('Rerolling day\'s temp variance');
            this.rollFinalTempModifiers(curTime);
        }
    }

    rollNightTempModifier(curTime = Times.DAWN): number {
        this.logger(`Rolling night mod/Time: ${ curTime }`);
        this.logger(`${ this.curNightTempMod }`);
        if (curTime === Times.DAWN) {
            this.curNightTempMod = null;
            return 0;
        }
        if (this.curNightTempMod === null) {
            this.logger(`Rolling new night variance, roll is ${ this.nightVariationRoll }`);
            this.curNightTempMod = rollDiceToInt(this.nightVariationRoll);
            this.logger(`Night Temp Mod is ${ this.curNightTempMod }`);
            this.logger(null);
        }
        return this.curNightTempMod;
    }

    getTemp(curTime = Times.DAWN): number {
        this.logger('Starting temperature calculation');
        const season = getSeasonFromDate();
        const seasonBaseTemp = this.getBaseTemp(season);
        this.logger(`Season Base Temp: ${ seasonBaseTemp }`);
        this.rollAllModifiers(curTime);
        this.logger(`Base climate temp mod: ${ this._baseClimateTempMod }`);
        this.logger(`Additional climate temp mod: ${ this.curClimateTempMod }`);
        const temp = seasonBaseTemp +
            this._baseClimateTempMod +
            this.curClimateTempMod +
            this.rollNightTempModifier(curTime);
        this.logger(`Temp: ${ temp }`);
        if (temp !== this.lastRolledTemperature) {
            this.lastRolledTemperature = temp;
            this.logger(`Final Object from temp calculation:\n${ JSON.stringify(this.printable(), null, 4) }`);
        }
        this.logger(null);
        this.lastRolledTemperature = temp;
        return temp;
    }

    getBaseTemp(season: Season): number {
        switch (this.climateType) {
            case ClimateType.COLD:
                switch (season) {
                    case Season.WINTER:
                        return 20;
                    case Season.SPRING:
                        return 30;
                    case Season.SUMMER:
                        return 40;
                    case Season.FALL:
                        return 30;
                }
                break;
            case ClimateType.TEMPERATE:
                switch (season) {
                    case Season.WINTER:
                        return 30;
                    case Season.SPRING:
                        return 60;
                    case Season.SUMMER:
                        return 72;
                    case Season.FALL:
                        return 45;
                }
                break;
            case ClimateType.TROPICAL:
                switch (season) {
                    case Season.WINTER:
                        return 50;
                    case Season.SPRING:
                        return 75;
                    case Season.SUMMER:
                        return 95;
                    case Season.FALL:
                        return 75;
                }
                break;
        }
        // Code should never get to here but just in case
        switch (season) {
            case Season.WINTER:
                return 30;
            case Season.SPRING:
                return 60;
            case Season.SUMMER:
                return 80;
            case Season.FALL:
                return 60;
            default:
                return 60;
        }
    }

}

export default TemperatureData;