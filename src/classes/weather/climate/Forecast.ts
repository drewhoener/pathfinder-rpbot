import { rollDiceToInt } from '../../../util/diceutil';
import moment from 'moment';
import WeatherHolder from '../WeatherHolder';
import { find, isEqual } from 'lodash';
import { getWeatherInstance } from '../../factory/WeatherCache';
import PrecipitationGenerator, {
    HeavyFrozenPrecipTable,
    HeavyUnfrozenPrecipTable,
    LightFrozenPrecipTable,
    LightUnfrozenPrecipTable,
    MediumFrozenPrecipTable,
    MediumUnfrozenPrecipTable,
    TorrentialFrozenPrecipTable,
    TorrentialUnfrozenPrecipTable,
} from '../PrecipitationGenerator';
import {
    IBasePrecipitation,
    IClimateBaseline,
    IElevationType,
    IForecast,
    IPrecipitationGenerator,
    IPrecipSetupData,
    IPrecipTypeFrequency,
    ITemperatureData,
    MutatedPrecipitation,
    MutatedPrecipResult,
} from '../../types/weather';
import {
    clampClimateType,
    clampPrecipFrequency,
    clampPrecipIntensity,
    ClimateType,
    PrecipFrequency,
    PrecipIntensity,
    Season,
    Times,
    WeatherType,
} from '../WeatherConstants';
import { ClimateBaseline, ElevationData, getSeasonFromDate, PrecipFrequencyProps } from '../WeatherBaselines';
import { IClientContext } from '../../types/context';
import PrecipClear from '../precipitation/PrecipClear';
import AbstractBasePrecip from '../precipitation/AbstractBasePrecip';
import TemperatureData from './TemperatureData';

export const PrecipitationGenerators: IPrecipitationGenerator[] = [
    new PrecipitationGenerator(LightFrozenPrecipTable, LightUnfrozenPrecipTable),
    new PrecipitationGenerator(MediumFrozenPrecipTable, MediumUnfrozenPrecipTable),
    new PrecipitationGenerator(HeavyFrozenPrecipTable, HeavyUnfrozenPrecipTable),
    new PrecipitationGenerator(TorrentialFrozenPrecipTable, TorrentialUnfrozenPrecipTable),
];

const getPrecipitationGenerator = (intensity: PrecipIntensity): IPrecipitationGenerator => {
    return PrecipitationGenerators[clampPrecipIntensity(intensity)];
};

export interface IForecastTestData {
    temp: number;
    variance: string;
    mod: number;
    night: number;
}

class Forecast implements IForecast {

    public climateTypeData: IClimateBaseline;
    public baseIntensity: PrecipIntensity;
    public precipChance: IPrecipTypeFrequency;
    protected temperatureData: ITemperatureData;
    protected _climateType: ClimateType;
    protected _elevation: IElevationType;
    public currentPrecipitation: MutatedPrecipResult;
    private ctx: IClientContext;
    private loggerMessage: string;

    constructor(ctx: IClientContext, climateType: ClimateType = ClimateType.TEMPERATE, elevation: IElevationType = ElevationData.LOW) {
        this.ctx = ctx;
        this._climateType = climateType;
        this.climateTypeData = ClimateBaseline[this.climateType];
        this._elevation = elevation;
        this.temperatureData = new TemperatureData(ctx, climateType, elevation);
        this.baseIntensity = this.elevation.basePrecipIntensity;
        this.precipChance = PrecipFrequencyProps[this.precipFrequency];

        const wind = AbstractBasePrecip.rollGenericWindType('1d70');
        const time = WeatherHolder.getTimes().cur;
        this.currentPrecipitation = new PrecipClear(wind, this.temperatureData.getTemp(time));

        this.loggerMessage = '';

        this.resetMods();
    }

    printable(): IForecast {
        return Object.assign({}, this, { ctx: null, temperatureData: this.temperatureData.printable() });
    }

    get precipFrequency(): PrecipFrequency {
        const season = getSeasonFromDate();
        const frequency: PrecipFrequency = this.getBaseFrequency(season) + this.frequencyModifier;
        return clampPrecipFrequency(frequency);
    }

    get frequencyModifier(): number {
        return this.climateTypeData.frequencyModifier + this.elevation.precipFrequencyModifier;
    }

    get intensityModifier(): number {
        return this.climateTypeData.intensityModifier;
    }

    get intensity(): PrecipIntensity {
        return clampPrecipIntensity(this.baseIntensity + this.intensityModifier);
    }

    set intensity(intensity: PrecipIntensity) {
        if (!intensity) {
            return;
        }
        this.baseIntensity = intensity;
    }

    get climateType(): ClimateType {
        return this._climateType;
    }

    set climateType(type: ClimateType) {
        if (isNaN(type)) {
            return;
        }
        this._climateType = clampClimateType(type);
        this.temperatureData.climateType = this._climateType;
        this.resetMods();
    }

    resetMods(): void {
        this.baseIntensity = this.elevation.basePrecipIntensity;
        this.precipChance = PrecipFrequencyProps[clampPrecipFrequency(this.precipFrequency)];
    }

    logger(message: string | null): void {
        this.ctx.logger.log(message);
    }

    get elevation() {
        return this._elevation;
    }

    set elevation(level: IElevationType) {
        if (!find(ElevationData, (elem) => isEqual(level, elem))) {
            console.log('Couldn\'t find elevation');
            return;
        }
        this._elevation = level;
        this.temperatureData.elevation = level;
        this.resetMods();
    }

    varianceObject(): IForecastTestData {
        const season = getSeasonFromDate();
        const temp = this.temperatureData.getBaseTemp(season);
        return {
            temp: temp,
            variance: this.temperatureData.curClimateVarianceRoll,
            mod: this.temperatureData.baseClimateTempMod,
            night: this.temperatureData.rollNightTempModifier(WeatherHolder.getTimes().cur),
        };
    }

    dewPoint(): number {
        return this.temperatureData.dewPoint;
    }

    getCachedTemp(): number {
        return this.temperatureData.lastRolledTemperature;
    }

    getTemperature(curTime: Times): number {
        return this.temperatureData.getTemp(curTime);
    }

    humidity(curTime: Times): number {
        return this.temperatureData.humidity(curTime);
    }

    // If weather not over mutateInPlace (temp)
    // If weather over mutateToNext(temp, intensity)

    // If expired and weather type is clear, roll for new

    // Otherwise, mutate and save
    getOrGeneratePrecip(curTime: Times, forceExpire = false): IBasePrecipitation {
        if (forceExpire) {
            this.logger('Force Expire Option has been forcibly set, chain will mutate out of place');
        }
        if (!this.currentPrecipitation) {
            this.logger('Creating New Chain');
            this.logger(null);
            this.currentPrecipitation = this.generatePrecipChain(curTime);
            return this.currentPrecipitation;
        }
        this.logger('Asking for time to check if chain is completed');
        const chainEndTime = this.currentPrecipitation.endTime;
        const now = moment().add(10, 'seconds');
        // Assume this generation is due to a temperature change
        if (!forceExpire && chainEndTime.isAfter(now)) {
            this.logger('Chain duration not complete, mutating in place with expired = false');
            this.logger(null);
            this.currentPrecipitation = this.currentPrecipitation.mutate(this.getTemperature(curTime), this.dewPoint(), false, getPrecipitationGenerator(this.intensity));
            if (this.currentPrecipitation == null) {
                this.currentPrecipitation = this.generatePrecipChain(curTime);
            }
            this.currentPrecipitation.windInstance.rollSpeed();
            return this.currentPrecipitation;
        }

        this.logger('Mutating to next, chain is expired');
        if (this.currentPrecipitation.weatherType === WeatherType.CLEAR) {
            this.logger('Clear weather over, checking to see if it should change');
            this.currentPrecipitation = this.generatePrecipChain(curTime);
            this.currentPrecipitation.windInstance.rollSpeed();
            this.logger(null);
            return this.currentPrecipitation;
        }

        // Chain is not clear, mutate from its own state machine
        this.currentPrecipitation = this.currentPrecipitation.mutate(this.getTemperature(curTime), this.dewPoint(), true, getPrecipitationGenerator(this.intensity));
        if (this.currentPrecipitation == null) {
            this.currentPrecipitation = this.generatePrecipChain(curTime);
        }
        this.currentPrecipitation.windInstance.rollSpeed();
        this.logger(null);
        return this.currentPrecipitation;
    }

    willHavePrecipitation(): boolean {
        if (!this.precipChance) {
            return false;
        }
        const roll = rollDiceToInt('1d100');
        return roll <= this.precipChance.precipChance;
    }

    generateClearChain(temperature: number): MutatedPrecipitation {
        const chain: IBasePrecipitation = getWeatherInstance(WeatherType.CLEAR, undefined, temperature, this.intensity);
        return chain
            .apply(PrecipitationGenerator.getClearWeather(temperature))
            .mutate(temperature, this.dewPoint(), false, getPrecipitationGenerator(this.intensity)) as MutatedPrecipitation;
    }

    generatePrecipChain(curTime: Times): MutatedPrecipitation {
        const temperature = this.getTemperature(curTime);
        if (!this.willHavePrecipitation()) {
            this.logger('Precip roll failed, weather will be clear');
            this.logger(null);
            return this.generateClearChain(temperature);
        }
        this.logger(`Getting generator for precipitation. Intensity is ${ PrecipIntensity[this.intensity] }`);
        const precipGenerator = getPrecipitationGenerator(this.intensity);
        if (!precipGenerator) {
            this.logger('Chosen generator for intensity is null. Cannot continue...');
            this.logger('Using Clear weather');
            this.logger(null);
            return this.generateClearChain(temperature);
        }

        let message = `Found Precipitation Generator ${ precipGenerator.constructor.name }\nGetting temperature:`;
        message += `Temp Variance Object:\n${ JSON.stringify(this.varianceObject(), null, 4) }\n`;
        message += `Temp with variance applied is ${ temperature }\nGenerating precipitation...`;
        const setupData: IPrecipSetupData = precipGenerator.getPrecipitationForTemp(temperature);
        message += `Generated Result:\n${ JSON.stringify(setupData, null, 4) }`;
        this.logger(message);
        this.logger(null);
        const chain = getWeatherInstance(setupData.weatherType, undefined, temperature, this.intensity);
        console.log(`Chain type: ${ chain.constructor.name }`);
        return chain.apply(setupData).mutate(temperature, this.dewPoint(), false, precipGenerator) as MutatedPrecipitation;
    }

    getPrecipSetupData(curTime: Times): IPrecipSetupData {
        return getPrecipitationGenerator(this.intensity).getPrecipitationForTemp(this.getTemperature(curTime));
    }

    getBaseFrequency(season: Season): PrecipFrequency {
        switch (this._climateType) {
            case ClimateType.COLD:
            case ClimateType.TEMPERATE:
                switch (season) {
                    case Season.WINTER:
                        return PrecipFrequency.RARE;
                    case Season.SPRING:
                        return PrecipFrequency.INTERMITTENT;
                    case Season.SUMMER:
                        return PrecipFrequency.COMMON;
                    case Season.FALL:
                        return PrecipFrequency.INTERMITTENT;
                }
                break;
            case ClimateType.TROPICAL:
                switch (season) {
                    case Season.WINTER:
                        return PrecipFrequency.RARE;
                    case Season.SPRING:
                        return PrecipFrequency.COMMON;
                    case Season.SUMMER:
                        return PrecipFrequency.INTERMITTENT;
                    case Season.FALL:
                        return PrecipFrequency.COMMON;
                }
                break;
        }
        return PrecipFrequency.INTERMITTENT;
    }
}

export default Forecast;
