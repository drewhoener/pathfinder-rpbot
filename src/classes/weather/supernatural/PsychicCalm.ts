import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['A supernatural calm settles over the land, stopping raging creatures from fighting and joyous ones from reveling. Creatures cannot take violent actions (although they can defend themselves) or do anything destructive. Psychic calm negates any morale bonuses granted by spells such as bless, good hope, and rage, as well as a bard\'s ability to inspire courage or a barbarian\'s rage ability. It also negates any fear or confusion effects.'];

class PsychicCalm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Psychic Calm', description, windType);

        this.enabled = false;
    }
}

export default PsychicCalm;