import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Leaden snow appears to be normal snow, but is far heavier. Objects beneath leaden snow take 1d6 points of damage per inch of snowfall. Creatures beneath leaden snow take 1d6 points of nonlethal damage per minute. If a creature falls unconscious beneath leaden show, it takes 1d6 points of lethal damage (DC 15 Constitution check negates) each minute thereafter until freed or dead.',
    'The combined effects of precipitation and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Leaden snowstorms make ranged weapon attacks impossible, even those using siege weapons.',
    'Snowstorms automatically extinguish candles, torches, and similar unprotected flames. They cause protected flames, such as those of lanterns, to dance wildly and have a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
    'In addition, snowstorms leave 1d6 inches of snow on the ground afterward. It costs 2 squares of movement to enter a snow-covered square.',
];

class LeadenSnowstorm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Leaden Snowstorm', description, Math.max(WindType.SEVERE, windType));

        this.enabled = false;
    }
}

export default LeadenSnowstorm;