import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Clouds shaped like dragons and winged wyrms twist through the sky, sometimes colliding and tearing through each other.'];

class DraconicClouds extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Draconic Clouds', description, windType);
        this.imageName = 'draconic_clouds';

        this.enabled = false;
    }
}

export default DraconicClouds;