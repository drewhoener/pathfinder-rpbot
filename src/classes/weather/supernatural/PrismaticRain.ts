import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Prismatic rain falls in brilliant, rainbow-hued drops. Prismatic rain reduces visibility by three quarters, imposing a -8 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Rain automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 50% chance to extinguish these lights.',
];

class PrismaticRain extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.RAIN, 'Prismatic Rain', description, windType);
        this.imageName = 'prismatic_rain';

        this.enabled = false;
    }
}

export default PrismaticRain;