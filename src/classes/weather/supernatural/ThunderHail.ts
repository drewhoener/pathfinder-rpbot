import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Thunder hail strikes the ground with deafening blasts of noise and ice shrapnel. It deals 1d6 points of damage (DC 15 Fort save negates) each minute to anyone caught out in the open without shelter. The sound of thunder hail makes Listen checks impossible.',
    'A day of hail leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a hail-covered square.',
];

class ThunderHail extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.HAIL, 'Thunder Hail', description, windType);

        this.enabled = false;
    }
}

export default ThunderHail;