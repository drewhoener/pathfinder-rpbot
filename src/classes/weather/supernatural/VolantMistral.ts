import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'While a volant mistral blows, an individual of at least fifth level (5 HD) can fly at a speed of 40 feet (or 30 feet if it wears medium or heavy armor, or if it carries a medium or heavy load). It can ascend at half speed and descend at double speed, and its maneuverability is good. Flying requires only as much concentration as walking, so the individual can attack or cast spells normally.',
    'A volant mistral has a 50% chance of extinguishing candles, torches, and similar unprotected flames.'
];

class VolantMistral extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Volant Mistral', description, Math.max(WindType.STRONG, windType));

        this.enabled = false;
    }
}

export default VolantMistral;