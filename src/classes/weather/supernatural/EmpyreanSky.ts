import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Golden or rose colored light fills the sky, and heroes and heroic deeds are granted divine favor. Good characters beneath an empyrean sky gain a +2 morale bonus to attack rolls and a +4 morale bonus to skill checks, and neutral characters gain a +1 morale bonus to attack rolls and a +2 morale bonus to skill checks.'];

class EmpyreanSky extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Empyrean Sky', description, windType);
        this.imageName = 'empyrean_sky';

        this.enabled = false;
    }
}

export default EmpyreanSky;