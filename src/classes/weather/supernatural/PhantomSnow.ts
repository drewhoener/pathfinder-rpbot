import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Phantom snow was named by rogues and thieves, who take particular advantage of it. Anyone moving upon it does so silently (+4 bonus to Move Silently checks), and leaves neither track nor trace of their passage.',
    'Snow reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'A day of snowfall leaves 1d6 inches of snow on the ground. It costs 2 squares of movement to enter a snow-covered square.',
];

class PhantomSnow extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Phantom Snow', description, windType);

        this.enabled = false;
    }
}

export default PhantomSnow;