import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Dire hail is at least an inch in size, and covered with sharp spikes of ice. It deals 1d6 points of damage (DC 15 Reflex save negates) each minute to anyone caught out in the open without shelter. The sound of falling hail imposes a -4 penalty on Listen checks.',
    'A day of hail leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a hail-covered square.',
];

class DireHail extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.HAIL, 'Dire Hail', description, windType);

        this.enabled = false;
    }
}

export default DireHail;