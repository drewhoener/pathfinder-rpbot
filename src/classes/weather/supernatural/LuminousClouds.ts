import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Luminous clouds cast light as bright as full daylight, even at night. Creatures which suffer penalties or are damaged or destroyed by daylight are similarly affected by luminous cloudlight. Magical darkness of any level counters luminous cloudlight.'];

class LuminousClouds extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Luminous Clouds', description, windType);
        this.imageName = 'luminous_clouds';

        this.enabled = false;
    }
}

export default LuminousClouds;