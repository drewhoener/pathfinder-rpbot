import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Fog obscures all sight, including darkvision, beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance). In addition, solid fog is so thick that any creature attempting to move through it progresses at a speed of 5 feet, regardless of its normal speed. A creature can\'t take a 5-foot step while in solid fog.',
    'Solid fog imposes a -2 penalty on melee attacks and damage rolls and makes ranged weapon attacks impossible, except for magic rays and the like. A creature or object that falls into solid fog is slowed, so that each 10 feet of vapor that it passes through reduces falling damage by 1d6.',
];

class SolidFog extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.FOG, 'Solid Fog', description, windType);

        this.enabled = false;
    }
}

export default SolidFog;