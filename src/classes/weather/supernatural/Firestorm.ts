import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'A firestorm manifests as a swirling cloud of smoke which suddenly explodes with flashes of fire and sheets of falling flame. Firestorms deal 1d6 points of fire damage (DC 15 Reflex save negates) each round to anyone caught out in the open without shelter, but rarely last more than a few minutes.',
    'The combined effects of smoke and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Firestorms make ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Creatures must succeed on a DC 15 Fort save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
    'Firestorms leave behind a deposit of 1d6 inches of fine ash.',
];

class Firestorm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.WINDSTORM, 'Firestorm', description, WindType.STRONG);
        this.imageName = 'firestorm';
        this.temperatureOffset = '+20';

        this.enabled = false;
    }
}

export default Firestorm;