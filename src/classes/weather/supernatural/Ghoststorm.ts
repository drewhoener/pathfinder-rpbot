import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'A chill windstorm brings a cacophany of noises, including the lost voices of the dead. An individual of at least third level (3 HD) who spends ten minutes within the storm shouting the name of a deceased creature might attract that spirit\'s attention. They may then convey one message or ask one question before the voice is carried away. However, answers are usually brief, cryptic, or fragmentary.',
    'Ghoststorms impose a -8 penalty on Listen checks due to the howling of the wind. Ranged weapon attacks are impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Ghoststorms automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
];

class Ghoststorm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.THUNDERSTORM, 'Ghoststorm', description, Math.max(windType, WindType.WINDSTORM));
        this.temperatureOffset = '-20';

        this.enabled = false;
    }
}

export default Ghoststorm;