import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Blacksleet is frozen rain imbued with negative energy. Creatures caught out in the open without shelter must succeed on a DC 15 Fort save once every hour or gain 1 negative level.',
    'Each negative level gives a creature a -1 penalty on attack rolls, saving throws, skill checks, ability checks, and effective level (for determining the power, duration, DC, and other details of spells or special abilities). Additionally, a spellcaster loses one spell or spell slot from his or her highest available level. Negative levels stack. If the subject has at least as many negative levels as HD, it dies.',
    'Twenty-four hours after gaining a negative level, the subject must make a Fort saving throw against the same DC for each negative level. If the save succeeds, that negative level is removed. If it fails, the negative level also goes away, but one of the subject\'s character levels is permanently drained.',
    'Sleet reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Sleet automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 75% chance to extinguish these lights.',
    'A day of sleet leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a sleet-covered square.',
];

class Blacksleet extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SLEET, 'Blacksleet', description, windType);

        this.enabled = false;
    }
}

export default Blacksleet;