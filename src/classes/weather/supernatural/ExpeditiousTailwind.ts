import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'An expeditious tailwind is a fey zephyr which follows creatures, pushing them along more swiftly. Creatures gain an enhancement bonus of 30 feet to their walking or flying speed.',
    'Expeditious wind has a 50% chance of extinguishing candles, torches, and similar unprotected flames.',
];

class ExpeditiousTailwind extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Expeditious Tailwind', description, WindType.MODERATE);

        this.enabled = false;
    }
}

export default ExpeditiousTailwind;