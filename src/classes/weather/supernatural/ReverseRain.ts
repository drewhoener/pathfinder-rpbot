import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Reverse rain rises in sheets from pools, lakes, streams, rivers, and other open water into the sky.',
    'Rain reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Rain automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 50% chance to extinguish these lights.',
];

class ReverseRain extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.RAIN, 'Reverse Rain', description, windType);

        this.enabled = false;
    }
}

export default ReverseRain;