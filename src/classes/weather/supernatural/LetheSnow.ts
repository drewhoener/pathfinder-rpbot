import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Lethe snow is imbued with enchantment magic, and is often accompanied by a faint tinkling sound as it falls. Creatures caught out in the open without shelter must succeed on a DC 15 Fort save once every hour or fall asleep for 1 hour.',
    'Sleeping creatures are helpless. Slapping or wounding awakens an affected creature, but normal noise or cold damage does not. Awakening a creature is a standard action (an application of the aid another action).',
    'Snow reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'A day of snowfall leaves 1d6 inches of snow on the ground. It costs 2 squares of movement to enter a snow-covered square.',
];

class LetheSnow extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Lethe Snow', description, windType);

        this.enabled = false;
    }
}

export default LetheSnow;