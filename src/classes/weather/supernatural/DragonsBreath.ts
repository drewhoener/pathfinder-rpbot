import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Gusts of hot air blow out of the east, accompanied by wisps of flame. Dragon\'s breath has a 50% chance of igniting candles, torches, tinder, loose paper, and similar flammable objects.'];

class DragonsBreath extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Dragon\'s Breath', description, Math.max(windType, WindType.MODERATE));
        this.temperatureOffset = '+20';

        this.enabled = false;
    }
}

export default DragonsBreath;