import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'An arcane tempest appears to be a normal thunderstorm, but possesses a powerful magical aura. All spells and spell-like abilities used within the tempest are empowered as if affected by the Empower Spell feat, although a spell so affected doesn\'t require a higher-level spell slot or additional casting time. Spells and spell-like abilities that are already empowered are unaffected.',
    'The combined effects of precipitation and wind reduce visibility by three quarters, imposing a -8 penalty on Spot, Search, and Listen checks. Arcane tempests make ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Arcane tempests automatically extinguish candles, torches, and similar unprotected flames. They cause protected flames, such as those of lanterns, to dance wildly and have a 50% chance to extinguish these lights.',
    'Creatures must succeed on a DC 15 Fort save or be blown away (Tiny or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Small; prone), or checked (Medium; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Small or smaller creatures), blown back 1d6 x 10 feet (Medium), or blown back 1d6 x 5 feet (Large).',
    'In addition, thunderstorms are accompanied by lightning that can pose a hazard to characters without proper shelter (especially those in metal armor). As a rule of thumb, assume one bolt per minute for a 1 hour period at the center of the storm. Each bolt causes electricity damage equal to 1d10 eight-sided dice.',
];

class ArcaneTempest extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.THUNDERSTORM, 'Arcane Tempest', description, Math.max(windType, WindType.STRONG));

        this.enabled = true;
    }
}

export default ArcaneTempest;