import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Incorporeal wind passes through physical objects and barriers, such as clothing, armor, walls, and solid earth and stone. It negates any benefit of winter clothing against cold and exposure effects, but also negates any penalty for heavy clothing or armor against heat. Incorporeal wind is blocked only by mage armor, wall of force, and similar force effects.'];

class IncorporealWind extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Incorporeal Wind', description, windType);

        this.enabled = false;
    }
}

export default IncorporealWind;