import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['These dread fogs drift from distant and bloody battlefields, bringing with them terrible visions of death and madness. Fog obscures all sight, including darkvision, beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance). In addition, creatures within the fog must succeed on a DC 15 Will save once every minute or act randomly.'];

class CrimsonFog extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.FOG, 'Crimson Fog', description, windType);
        this.imageName = 'crimson_fog';
        this.addExtra('d%',
            `01-50
			51-70
			71-100`,
            true,
        );
        this.addExtra('Behavior',
            `Act normally
			Flee in a random direction
			Attack the nearest creature`,
            true,
        );

        this.enabled = false;
    }
}

export default CrimsonFog;