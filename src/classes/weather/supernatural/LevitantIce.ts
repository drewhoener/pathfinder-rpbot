import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Levitant ice occurs when strange alchemical essences become mixed into cold clouds. Unsecured creatures and objects lose weight as ice accumulates upon them, and after one minute begin floating upwards at the rate of twenty feet per round. The effect ends gradually, as the levitant ice melts.',
    'Sleet reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Sleet automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 75% chance to extinguish these lights.',
];

class LevitantIce extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SLEET, 'Levitant Ice', description, windType);
        this.imageName = 'snow';

        this.enabled = false;
    }
}

export default LevitantIce;