import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Gatestorms can manifest suddenly and out of thin air. They appear at first as clouds of twinkling stars, but within seconds transform into maelstroms of powerful winds and dimensional gates which flash open randomly. Creatures of Medium size or smaller and airborne creatures of Gargantuan size or smaller must succeed on a DC 15 Fort or Will save (player\'s choice) every minute or be blown through a gate to a random plane of existence or alternate dimension. A gatestorm typically lasts for only 1d6 minutes before fading as suddenly as it appeared.',
    'The wind of a gatestorm is powerful enough to bring down branches if not whole trees, and impose a -8 penalty on Listen checks. Ranged weapon attacks are impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'Gatestorms automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Large or Huge creatures must succeed on a DC 18 Fort save or be checked (unable to move forward against the force of the wind).',
];

class Gatestorm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.WINDSTORM, 'Gatestorm', description, Math.max(windType, WindType.WINDSTORM));
        this.imageName = 'gatestorm';

        this.enabled = false;
    }
}

export default Gatestorm;