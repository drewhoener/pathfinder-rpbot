import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Ethereal fog is a shimmering mist which manifests suddenly and out of thin air. It obscures all sight, including darkvision, beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance). In addition, creatures taking a movement action within the fog must succeed on a DC 15 Will save or be teleported to a different random location within the fog.'];

class EtherealFog extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.FOG, 'Ethereal Fog', description, windType);

        this.enabled = false;
    }
}

export default EtherealFog;