import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'While a whispering wind blows, an individual of at least third level (3 HD) can send a message or sound on the wind to a designated location. The location must be familiar and within one mile. The message is delivered regardless of whether anyone is present to hear it.',
    'Whispering wind has a 50% chance of extinguishing candles, torches, and similar unprotected flames.',
];

class WhisperingWind extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Whispering Wind', description, WindType.MODERATE);

        this.enabled = false;
    }
}

export default WhisperingWind;