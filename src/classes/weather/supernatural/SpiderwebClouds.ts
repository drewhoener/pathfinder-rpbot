import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Tangled strands of cloud fill the sky like spiderwebs. Airborne creatures which fly into a spiderweb cloud become entangled (DC 15 Reflex save negates). An entangled creature moves at half speed, cannot run or charge, and takes a -2 penalty on all attack rolls and a -4 penalty to Dexterity.'];

class SpiderwebClouds extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Spiderweb Clouds', description, windType);
        this.imageName = 'spiderweb_clouds';

        this.enabled = false;
    }
}

export default SpiderwebClouds;