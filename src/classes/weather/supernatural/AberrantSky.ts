import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'An aberrant sky appears twisted and wrong, filled with an unearthly shade of light and clouds of impossible and monstrous shapes. Creatures which gaze upon an aberrant sky must succeed on a DC 18 Will save or be driven insane, suffering the effects of confusion for 1d6 hours. Roll on the following table at the beginning of each affected creature\'s turn each round to see what the creature does in that round.',
];

class AberrantSky extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Aberrant Sky', description, windType);
        this.imageName = 'aberrant_sky.jpg';
        this.addExtra('d%',
            `01-10
			11-20
			21-50
			51-70
			71-100`,
            true,
        );
        this.addExtra('Behavior',
            `Do nothing but babble incoherently
			Act normally
			Do nothing but babble incoherently
			Flee in a random direction
			Attack the nearest creature`,
            true,
        );

        this.enabled = false;
    }
}

export default AberrantSky;