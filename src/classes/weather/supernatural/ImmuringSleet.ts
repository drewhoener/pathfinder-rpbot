import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Immuring sleet falls as rain, but freezes into masses of ice the instant it touches an object or creature. Creatures caught out in the open without shelter must attempt a DC 15 Fort save once every hour.',
    'A creature which has failed one saving throw is covered with masses of ice, and is slowed. A slowed creature can take only a single move action or standard action each turn, but not both (nor may it take full-round actions). It moves at half its normal speed (round down to the next 5-foot increment). Additionally, it takes a -1 penalty on attack rolls, AC, and Reflex saves.',
    'A creature which fails a second saving throw becomes frozen to the ground, and is immobilized. It cannot move, and can take only a single standard action each turn. Additionally, it takes a -2 penalty on attack rolls, AC, and Reflex saves.',
    'A creature which fails a third saving throw becomes trapped within solid ice, and is paralyzed. A paralyzed creature is unable to move or act. It has effective Dexterity and Strength scores of 0 and is helpless, but can take purely mental actions.',
    'A creature which fails a fourth saving throw becomes completely entombed within a large mass of solid ice, and dies.',
    'Sleet reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks and ranged weapon attacks.',
    'Sleet automatically extinguishes candles, torches, and similar unprotected flames. It causes protected flames, such as those of lanterns, to dance wildly and has a 75% chance to extinguish these lights.',
    'A day of immuring sleet leaves 1d6 inches of solid ice on the ground. Creatures walking on ice must spend 2 squares of movement to enter a square covered by ice, and the DC for Balance and Tumble checks increases by +5.',
];

class ImmuringSleet extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SLEET, 'Immuring Sleet', description, windType);

        this.enabled = false;
    }
}

export default ImmuringSleet;