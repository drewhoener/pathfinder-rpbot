import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Rogue zephyrs are gentle but magical breezes which undo simple (those of DC 20 or less) knots, locks, bolts, manacles, shackles, and similar impediments.',
    'A rogue zephyr has a 50% chance of extinguishing candles, torches, and similar unprotected flames.'
];

class RogueZephyr extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Rogue Zephyr', description, Math.max(WindType.MODERATE, windType));

        this.enabled = false;
    }
}

export default RogueZephyr;