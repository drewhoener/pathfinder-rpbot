import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Stars fall from the sky, plummeting to the ground as frozen pellets of soft, fading light. After a starfall hail, the night sky remains totally dark until the next evening.',
    'The sound of falling hail imposes a -4 penalty on Listen checks. Sometimes hail can become large enough to deal 1 point of lethal damage (per storm) to anything in the open.',
    'A day of hail leaves 1d6 inches of loose ice on the ground. It costs 2 squares of movement to enter a hail-covered square. An area covered by starfall hail provides shadowy illumination.',
];

class StarfallHail extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.HAIL, 'Starfall Hail', description, windType);
        this.imageName = 'starfall';

        this.enabled = false;
    }
}

export default StarfallHail;