import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['The sky is a perfect azure blue, colors seem more vibrant, and details appear sharper. Celestial clarity grants a +4 bonus to Spot, Search, Listen, and Sense Motive checks. It also negates any fear or confusion effects.'];

class CelestialClarity extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Celestial Clarity', description, windType);

        this.enabled = false;
    }
}

export default CelestialClarity;