import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Violent shockwaves suddenly tear through the sky, causing it to crack and clouds to be torn apart and scattered. Creatures must make a DC 15 Fort save or be deafened for 1 hour from the thunderous noise. Airborne creatures must make an additional DC 15 Fort save or take 2d6 points of nonlethal damage due to battering and buffeting.'];

class Skyquake extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Skyquake', description, windType);
        this.imageName = 'skyquake';

        this.enabled = false;
    }
}

export default Skyquake;