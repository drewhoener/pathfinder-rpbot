import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Dark clouds shot through with lightning rage overhead, and rumbling thunder and howling winds fill the air. The noise imposes a -4 penalty on Listen checks, but the storm has no other effect.'];

class HallucinatoryStorm extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Hallucinatory Storm', description, windType);

        this.enabled = false;
    }
}

export default HallucinatoryStorm;