import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Leaden snow appears to be normal snow, but is far heavier. Objects beneath leaden snow take 1d6 points of damage per inch of snowfall. Creatures beneath leaden snow take 1d6 points of nonlethal damage per minute. If a creature falls unconscious beneath leaden show, it takes 1d6 points of lethal damage (DC 15 Constitution check negates) each minute thereafter until freed or dead.',
    'Snow reduces visibility by half, imposing a -4 penalty on Spot and Search checks. It also imposes a -4 penalty on Listen checks. Leaden snow makes ranged weapon attacks impossible, except for those using siege weapons, which suffer a -4 penalty on attack rolls.',
    'A day of snowfall leaves 1d6 inches of snow on the ground. It costs 2 squares of movement to enter a snow-covered square.',
];

class LeadenSnow extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Leaden Snow', description, windType);

        this.enabled = false;
    }
}

export default LeadenSnow;