import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Clouds of roiling smoke shot through with hot embers drift overhead, casting a fiery glow. Airborne creatures flying into an incendiary cloud cause it to ignite, dealing 4d6 points of fire damage (DC 18 Reflex save negates).'];

class IncendiaryClouds extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Incendiary Clouds', description, windType);
        this.imageName = 'incendiary_clouds';

        this.enabled = false;
    }
}

export default IncendiaryClouds;