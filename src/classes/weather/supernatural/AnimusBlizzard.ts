import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'The combination of high winds, heavy snow (typically 1d3 feet), and bitter cold make blizzards deadly for all who are unprepared for them. Further, animus blizzards are infused with negative energy, and any creature which perishes in one becomes a wight 1d4 rounds later.',
    'Blizzards reduce visibility to zero, making Spot, Search, and Listen checks and all ranged weapon attacks impossible. All sight, including darkvision, is obscured beyond 5 feet. Creatures 5 feet away have concealment (attacks by or against them have a 20% miss chance).',
    'Blizzards automatically extinguish candles, torches, and similar unprotected flames. They have a 75% chance to extinguish protected flames, such as those of lanterns.',
    'Creatures must succeed on a DC 18 Fort save or be blown away (Small or smaller creatures; knocked prone and rolled 1d4 x 10 feet, taking 1d4 points of nonlethal damage per 10 feet), knocked down (Medium; prone), or checked (Large or Huge; unable to move forward against the force of the wind).',
    'Airborne creatures are instead blown back 2d6 x 10 feet and dealt 2d6 points of nonlethal damage due to battering and buffeting (Medium or smaller creatures), blown back 1d6 x 10 feet (Large), or blown back 1d6 x 5 feet (Huge or Gargantuan).',
    'In addition, blizzards leave 1d3 feet of heavy snow on the ground afterward. It costs 4 squares of movement to enter a square covered with heavy snow. A blizzard may result in snowdrifts 1d4 x 5 feet deep, especially in and around objects big enough to deflect the wind -- a cabin or a large tent, for instance.',
];

class AnimusBlizzard extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.SNOW, 'Animus Blizzard', description, Math.max(WindType.WINDSTORM, windType));

        this.enabled = false;
    }
}

export default AnimusBlizzard;