import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = [
    'Temporal winds are rarely severe in any physical dimension, but can easily blow creatures through the dimension of time. Creatures must succeed on a DC 15 Will save or be blown 1d6 hours forward. The creature seems to disappear in a shimmer of silver light, then reappears later in exactly the same space (or closest unoccupied space), orientation, and condition as before. From the affected creature\'s point of view, no time has passed at all.',
    'Temporal wind has a 50% chance of extinguishing candles, torches, and similar unprotected flames.',
];

class TemporalWind extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Temporal Wind', description, Math.max(windType, WindType.MODERATE));

        this.enabled = false;
    }
}

export default TemporalWind;