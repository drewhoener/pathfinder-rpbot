import SpecialWeather from '../precipitation/SpecialWeather';
import { ISpecialWeather } from '../../types/weather';
import { WeatherType, WindType } from '../WeatherConstants';

const description = ['Clouds shaped like gears spin slowly overhead, sometimes interlocking with other clouds to form complex clockworks.'];

class ClockworkClouds extends SpecialWeather implements ISpecialWeather {
    constructor(windType: WindType, temp: number) {
        super(WeatherType.CLEAR, 'Clockwork Clouds', description, windType);
        this.imageName = 'clockwork_clouds';

        this.enabled = true;
    }
}

export default ClockworkClouds;