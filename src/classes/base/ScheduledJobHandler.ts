import { IClientContext } from '../types/context';
import { Job, RecurrenceRule, RecurrenceSpecDateRange, RecurrenceSpecObjLit, scheduleJob } from 'node-schedule';
import logger from '../../util/logging';

export interface IScheduledJobHandler {
    start(arg0: IClientContext): void;

    cancel(): void;

    executeJob(arg0: IClientContext): Promise<void>;
}

abstract class ScheduledJobHandler implements IScheduledJobHandler {

    protected name: string;
    protected scheduledJob: Job | undefined;
    protected scheduleParameter: RecurrenceRule | RecurrenceSpecDateRange | RecurrenceSpecObjLit | Date | string | number;

    protected constructor(parameter: RecurrenceRule | RecurrenceSpecDateRange | RecurrenceSpecObjLit | Date | string | number, name = 'Undefined Task') {
        this.scheduleParameter = parameter;
        this.name = name;
    }

    start(context: IClientContext): void {
        if (!this.scheduledJob) {
            logger.info(`Starting Scheduled Task [${ this.name }]...`);
            this.scheduledJob = scheduleJob(this.scheduleParameter, this.executeJob.bind(this, context));
        } else {
            logger.info(`Scheduled task [${ this.name }] is already set, cannot start`);
        }
    }

    cancel(): void {
        this.scheduledJob?.cancel(false);
        this.scheduledJob = undefined;
    }

    abstract executeJob(context: IClientContext): Promise<void>;
}

export default ScheduledJobHandler;