import { Message } from 'discord.js';
import { CommandNames, HelpCategory, IClientContext } from '../types/context';

export interface ICommandOptions {
    readonly name: string;
    readonly commandName: CommandNames;
    readonly description: string;
    readonly usage: string;
    readonly aliases?: string[];
    readonly preventDM: boolean;
    /**  @default HelpCategory.GENERAL */
    readonly helpCategory?: HelpCategory;
}

export interface ICommand extends ICommandOptions {
    aliases: string[];

    execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void>;
}

abstract class Command implements ICommand {
    readonly name: string;
    readonly commandName: CommandNames;
    readonly description: string;
    readonly usage: string;
    readonly aliases: string[];
    readonly preventDM: boolean;
    readonly helpCategory?: HelpCategory;

    protected constructor(options: ICommandOptions) {
        this.name = options.name;
        this.commandName = options.commandName;
        this.description = options.description;
        this.usage = options.usage;
        this.preventDM = options.preventDM;
        this.helpCategory = options.helpCategory ?? HelpCategory.GENERAL;

        if (options.aliases && options.aliases.length) {
            this.aliases = [...options.aliases];
        } else {
            this.aliases = [];
        }
    }

    abstract execute(ctx: IClientContext, message: Message, args: string[], command: string): Promise<Message | void>;
}


export default Command;