import { PendingSheetType } from '../types/context';
import moment, { Moment } from 'moment';

export interface ICharacterSheetTransaction {
    transactionType: PendingSheetType;
    cost: number;
    date: Date;

    toMoment(): Moment;
}

class CharacterSheetTransaction implements ICharacterSheetTransaction {

    public transactionType: PendingSheetType;
    public cost: number;
    public date: Date;

    constructor(transactionType: PendingSheetType, cost: number, date: Date = new Date()) {
        this.transactionType = transactionType;
        this.cost = cost;
        this.date = date;
    }

    toMoment(): Moment {
        return moment(this.date);
    }
}

export default CharacterSheetTransaction;