import Command from './Command';
import WeatherHolder from '../weather/WeatherHolder';
import { ClientContext } from '../../handler/ClientContext';
import { ClimateType } from '../weather/WeatherConstants';
import { ElevationData } from '../weather/WeatherBaselines';

abstract class WeatherCommand extends Command {
    protected static clientWeatherHolder: WeatherHolder | undefined;

    static getWeatherHolder(): WeatherHolder {
        if (!WeatherCommand.clientWeatherHolder) {
            WeatherCommand.clientWeatherHolder = new WeatherHolder(ClientContext, '446704418600124427', '704475700819656755', ClimateType.TEMPERATE, ElevationData.SEA);
        }
        return WeatherCommand.clientWeatherHolder;
    }
}


export default WeatherCommand;