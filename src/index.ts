import * as mongo from './db/database';
import logger from './util/logging.js';
import { FILE_REGEX, getChannel } from './util/utils';
import { promisify } from 'util';
import fs from 'fs';
// const heapdump = require('heapdump');
// noinspection JSUnusedLocalSymbols
import Command from './classes/base/Command';
import path from 'path';
import { IExitOptions } from './classes/types/context';
import { loadPolls, loadWatchedWords } from './handler/clienthandler';
import { ClientContext } from './handler/ClientContext';
import { TextChannel } from 'discord.js';
import { populateWeatherCache } from './classes/factory/WeatherCache';
import { addReactions, parseCommands, parseWatchedWords } from './handler/messagewatcher';
import InactiveEncounterHandler from './handler/InactiveEncounterHandler';
import LevelUpHandler from './handler/LevelUpHandler';

require('@babel/polyfill');
const readdir = promisify(fs.readdir);

const { client } = ClientContext;
const inactiveHandler = new InactiveEncounterHandler();
const levelUpHandler = new LevelUpHandler();

const init = async (): Promise<void> => {
    await populateWeatherCache();
    const commandFiles = fs.readdirSync(path.resolve(__dirname, './commands')).filter(file => FILE_REGEX.test(file));
    for (const file of commandFiles) {
        try {
            const { default: DynamicCommand } = await import(`./commands/${ file }`);
            const commandInstance: Command = new DynamicCommand();

            ClientContext.commands.set(commandInstance.commandName, commandInstance);
            logger.info(`Registered command: ${ commandInstance.name }`);
            /*
            if (commandInstance.hasOwnProperty('events')) {
                for (const eventName in commandInstance.events) {
                    if (!commandInstance.events.hasOwnProperty(eventName)) continue;
                    client.on(eventName, commandInstance.events[eventName].bind(null, client));
                }
            }
             */
        } catch (err) {
            logger.error(err);
            logger.error(`Failed to register command ${ file }`);
        }
    }

    const evtFiles = await readdir('./events/');
    logger.info(`Loading a total of ${ evtFiles.length } events.`);
    evtFiles.forEach(file => {
        const eventName = file.split('.')[0];
        logger.info(`Loading Event: ${ eventName }`);
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const event = require(`./events/${ file }`);
        client.on(eventName, event.bind(null, ClientContext));
    });
};

client.once('ready', async () => {
    ClientContext.logger.channel = getChannel(client, '446704418600124427', '629134211651076117') as TextChannel;
    logger.info('Winston is starting up');

    await init()
        .then(() => {
            logger.info('Loaded commands');
        })
        .catch(err => {
            logger.error('Error Loading Commands');
            logger.error(err);
        });

    await mongo.connect();
    await loadPolls(ClientContext);
    loadWatchedWords(ClientContext)
        .then((result) => {
            logger.info(`Loaded ${ result.wordsLoaded } word(s) from ${ result.guildsLoaded } different guilds.`);
        })
        .catch(err => logger.error(err));

    // scheduleChannelUpdates(client);

    logger.info('Ready');
});

client.on('message', parseWatchedWords.bind(null, ClientContext));

client.on('message', addReactions.bind(null, ClientContext));

client.on('message', parseCommands.bind(null, ClientContext));

// https://stackoverflow.com/questions/14031763/doing-a-cleanup-action-just-before-node-js-exits
function exitHandler(options: IExitOptions, exitCode: number | string): void {
    if (options.cleanup) {
        logger.info('Cleaning Up...');
    }
    if (exitCode || exitCode === 0) {
        logger.info(`Exit Code: ${ exitCode }`);
    }
    if (options.exit) {
        mongo.close()
            .catch(err => {
                logger.error('Failed to cleanly disconnect database');
                logger.error(err);
                process.exit();
            })
            .then(() => {
                process.exit();
            });
    }
}

// do something when app is closing
process.on('exit', exitHandler.bind(null, { cleanup: true }));

// catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, { exit: true }));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, { exit: true }));
// eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
process.on('SIGUSR2', () => {

});

// catches uncaught exceptions
process.on('uncaughtException', error => {
    logger.error(error);
    exitHandler.bind(null, { exit: true });
});

ClientContext.config.loadConfig()
    .then((partials) => {
        logger.info('Loaded the following partial config data:');
        logger.info(JSON.stringify(partials, null, 4));
    })
    .then(() => ClientContext.config.loadObjects())
    .then(() => client.login(ClientContext.config.auth.clientAuth.token))
    .then(() => {
        inactiveHandler.start(ClientContext);
        levelUpHandler.start(ClientContext);
    })
    .catch(err => {
        logger.error(err);
        process.exit(-1);
    });
