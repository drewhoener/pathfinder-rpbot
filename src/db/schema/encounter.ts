/* eslint-disable @typescript-eslint/camelcase */
import { Document, Model, model, Schema } from 'mongoose';
import { Snowflake } from 'discord.js';
import logger from '../../util/logging';
import { duration, now } from 'moment';

export interface ITimeResult {
    userId: Snowflake;
    ignoreInResults: boolean;
    join: number;
    end: number;
    playTime: number;
    inactive: number;
}

export interface IEncounterMember {
    userId: string;
    joinedAt: number;
    inactiveHours: number;
    ignoreInResults?: boolean;
}

export interface IEncounter extends Document {
    type: number;
    guild: string;
    channel: string;
    gm: Snowflake;
    members: IEncounterMember[];
    startTime: number;
    lastMessage: number;
    warned: boolean;

    getTimes(endTime: number): ITimeResult[];

    getGM(): Snowflake;
}

const getInactiveMillis = (endTime: number, lastMessage: number, curModHours: number): number => {
    logger.error('Calculating total mod hours');
    logger.info(`User currently has ${ curModHours } hours downtime`);
    const inactiveHours = duration(curModHours, 'h');
    const offset = duration(endTime - lastMessage);
    let offsetHours = offset.asHours();
    if (offsetHours < 1 || isNaN(offsetHours)) {
        offsetHours = 0;
    }
    logger.info(`Adding ${ offsetHours } to total`);
    inactiveHours.add(offsetHours, 'h');
    logger.info(`Result of inactiveHours: ${ inactiveHours.asHours() } / ${ inactiveHours.asMilliseconds() }`);
    return inactiveHours.asMilliseconds();
};

const encounterSchema: Schema<IEncounter> = new Schema<IEncounter>({
    type: { type: Schema.Types.Number, required: true },
    guild: { type: String, required: true },
    channel: { type: String, required: true },
    gm: { type: String },
    members: {
        type: [
            {
                userId: String,
                joinedAt: Schema.Types.Number,
                inactiveHours: Schema.Types.Number,
                ignoreInResults: { type: Boolean, required: false, default: false },
            },
        ],
        default: [],
    },
    startTime: { type: Schema.Types.Number, required: true },
    lastMessage: { type: Schema.Types.Number, required: true },
    warned: { type: Boolean, default: false },
});

encounterSchema.methods.getGM = function(): Snowflake {
    if (this.gm) {
        return this.gm;
    }
    return this.members[0].userId;
};

encounterSchema.methods.getTimes = function(endTime: number): ITimeResult[] {
    logger.info(`Starting collection of times for encounter in channel ${ this.channel }`);
    logger.info(`Collecting data for ${ this.members.length } users`);
    const result: ITimeResult[] = [];

    const useQuestTime: boolean = this.type === 1;
    const lastMessage = useQuestTime ? now() : this.lastMessage;

    for (const memberObj of this.members) {
        const member: IEncounterMember = memberObj;
        const inactiveMillis = useQuestTime ? 0 : getInactiveMillis(endTime, lastMessage, member.inactiveHours);
        const playTime = duration(endTime - member.joinedAt);
        const playDuration = playTime.clone();

        let playTimeMillis = 0;
        if (inactiveMillis <= playDuration.asMilliseconds()) {
            playDuration.subtract(inactiveMillis, 'ms');
            playTimeMillis = playDuration.asMilliseconds();
            logger.info(`User ${ member.userId } was in encounter for ${ playDuration.asHours().toFixed(2) } hours`);
            logger.info(`Total Time: ${ playTime.asHours().toFixed(2) } hours`);
            logger.info(`Removed Time: ${ duration(inactiveMillis).asHours().toFixed(2) } hours (${ inactiveMillis } ms)`);
            if (useQuestTime) {
                logger.warn('This encounter is using quest time, the subtracted time should be 0. Please verify.');
            }
        } else {
            logger.error(`User ${ member.userId } was inactive more than active, which shouldn't happen in theory`);
            logger.error(member);
            logger.error(`Play: ${ playTime.asMilliseconds() } / ${ playTime.asHours().toFixed(2) }`);
        }

        result.push({
            userId: member.userId,
            ignoreInResults: member.ignoreInResults ?? false,
            join: member.joinedAt,
            end: endTime,
            playTime: playTimeMillis,
            inactive: inactiveMillis,
        });
    }
    return result;
};


const Encounter: Model<IEncounter> = model<IEncounter>('encounters', encounterSchema);
export default Encounter;