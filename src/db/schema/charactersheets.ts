import { Document, model, Model, Schema } from 'mongoose';
import { Snowflake } from 'discord.js';
import { PendingSheetType } from '../../classes/types/context';
import CharacterSheetTransaction, { ICharacterSheetTransaction } from '../../classes/base/CharacterSheetTransaction';

export interface ICharacterNote {
    note: string;
    user?: Snowflake;
    name?: string;
}

export interface IMythweaversSheetData {
    guild: string;
    userId: Snowflake;
    playerName: string;
    characterName: string;
    race: string;
    class: string;
    mythweavers: string;
    notes: ICharacterNote[];
}

export interface ICharacterSheet extends IMythweaversSheetData {
    transactions: ICharacterSheetTransaction[];
}

export interface ICharacterSheetDoc extends ICharacterSheet, Document {
}

export interface IPendingSheet extends IMythweaversSheetData, Document {
    messageId: string;
    approveMessageId: string;
    requestType: PendingSheetType;
    cost: number;

    hasCost(): boolean;

    getApproved(): ICharacterSheet;
}

const pendingSheetSchema: Schema<IPendingSheet> = new Schema<IPendingSheet>({
    guild: { type: String, required: true },
    userId: { type: String, required: true, index: true },
    playerName: { type: String, required: true },
    characterName: { type: String, required: true },
    race: { type: String, required: true },
    class: { type: String, required: true },
    mythweavers: { type: String, required: true },
    notes: {
        type: [
            {
                note: { type: String, required: true },
                user: { type: String },
                name: { type: String },
            },
        ],
        default: [],
    },
    messageId: { type: String, unique: true, required: true },
    approveMessageId: { type: String, unique: true, required: true, index: true },
    requestType: { type: Number, default: PendingSheetType.REQUEST },
    cost: { type: Number, default: 0 },
});

pendingSheetSchema.methods.hasCost = function(): boolean {
    return this.cost > 0;
};

pendingSheetSchema.methods.getApproved = function(): ICharacterSheet {

    const { guild, userId, playerName, characterName, race, class: characterClass, mythweavers } = this;

    return {
        guild,
        userId,
        playerName,
        characterName,
        race,
        class: characterClass,
        mythweavers,
        notes: [...this.notes],
        transactions: [
            new CharacterSheetTransaction(this.requestType, this.cost),
        ],
    };
};

const approvedSheetSchema: Schema<ICharacterSheetDoc> = new Schema<ICharacterSheetDoc>({
    guild: { type: String, required: true },
    userId: { type: String, required: true, index: true },
    playerName: { type: String, required: true },
    characterName: { type: String, required: true },
    race: { type: String, required: true },
    class: { type: String, required: true },
    mythweavers: { type: String, required: true },
    notes: {
        type: [
            {
                note: { type: String, required: true },
                user: { type: String },
                name: { type: String },
            },
        ],
        default: [],
    },
    transactions: {
        type: [
            {
                transactionType: { type: Number, default: PendingSheetType.REQUEST },
                cost: { type: Number, default: 0 },
                date: { type: Date, default: new Date() },
            },
        ],
        default: [],
    },
});

export const CharacterSheet: Model<ICharacterSheetDoc> = model<ICharacterSheetDoc>('CharacterSheet', approvedSheetSchema);
export const PendingSheet: Model<IPendingSheet> = model<IPendingSheet>('PendingSheet', pendingSheetSchema);