import { Document, model, Model, Schema } from 'mongoose';
import { Collection, Snowflake } from 'discord.js';
import logger from '../../util/logging';
import { IClientContext } from '../../classes/types/context';
import { getWatchedChannels } from './settings';
import * as util from 'util';

export enum WatchSettingType {
    COUNT = 0b1,
    ANNUOUNCE = 0b10,
}

export interface IWordPartial {
    word: string;
    regex: RegExp;
    aliasOf: string;
    setting: number;
    tracked: boolean;
    users: IWatchedWordUser[];
}

export interface IWatchedWordUser {
    userId: string;
    username: string;
    lastTime: number;
    count: number;
}

export interface IWatchedWord extends Document {
    guild: string;
    word: string;
    aliases: string[];
    setting: number;
    tracked: boolean;
    users: IWatchedWordUser[];
    count: number;
    lastTime: number;
    // milliseconds
    announceInterval: number;

    toPartials(): IWordPartial[];
}

export type PartialWordCache = {
    words: Collection<string, IWordPartial>;
    watchedChannels: string[];
};

const watchedWordSchema: Schema<IWatchedWord> = new Schema<IWatchedWord>({
    guild: { type: String, required: true },
    word: { type: String, required: true },
    aliases: { type: [String], default: [] },
    users: {
        type: [{
            userId: String,
            username: String,
            lastTime: Number,
            count: Number,
        }],
        default: [],
    },
    count: { type: Number, default: 0 },
    lastTime: { type: Number, default: new Date().valueOf() },
    announceInterval: { type: Number, default: 600000 },
    setting: { type: Number, default: 1 },
    tracked: { type: Boolean, default: true },
});

watchedWordSchema.methods.toPartials = function(): IWordPartial[] {
    const partials: IWordPartial[] = [];
    const users = this.users.map((user: IWatchedWordUser) => ({
        userId: user.userId,
        username: user.username,
        count: user.count,
        lastTime: user.lastTime,
    }));
    const partialFormat = '(?:\\s|^)%s(?:\\s|$)';
    partials.push({
        word: this.word,
        regex: new RegExp(util.format(partialFormat, this.word)),
        aliasOf: this.word,
        setting: this.setting,
        tracked: this.tracked,
        users,
    });
    this.aliases.forEach((word: string) => {
        partials.push({
            word,
            regex: new RegExp(word),
            aliasOf: this.word,
            setting: this.setting,
            tracked: this.tracked,
            users,
        });
    });
    return partials;
};

const WatchedWord: Model<IWatchedWord> = model<IWatchedWord>('WatchedWord', watchedWordSchema);

export async function getPartialCacheFor(guild: string): Promise<PartialWordCache> {
    logger.info(`Constructing additional partial cache for guild ${ guild }`);
    const words: IWatchedWord[] = await WatchedWord.find({ guild });
    // await channel.send(`\`\`\`${JSON.stringify(words, null, 4)}\`\`\``);
    const cache: PartialWordCache = {
        words: new Collection<string, IWordPartial>(),
        watchedChannels: await getWatchedChannels(guild),
    };
    words.forEach((word: IWatchedWord) => {
        word.toPartials().forEach((partial: IWordPartial) => {
            cache.words.set(partial.word, partial);
        });
    });
    return cache;
}

export async function loadPartialCaches(ctx: IClientContext): Promise<number> {
    const words: IWatchedWord[] = await WatchedWord.find({});
    let num = 0;
    words.forEach((word: IWatchedWord) => {
        let holder: PartialWordCache | undefined = ctx.words.get(word.guild);
        if (!holder) {
            holder = {
                words: new Collection<string, IWordPartial>(),
                watchedChannels: [],
            };
            ctx.words.set(word.guild, holder);
        }
        word.toPartials().forEach((partial: IWordPartial) => {
            if (!holder) {
                return;
            }
            holder.words.set(partial.word, partial);
            num++;
        });
    });
    for (const guild of Array.from(ctx.words.keys())) {
        logger.info(`Getting watched channels for guild: ${ guild }`);
        const cache: PartialWordCache | undefined = ctx.words.get(guild);
        if (!cache) {
            continue;
        }
        cache.watchedChannels = await getWatchedChannels(guild);
        logger.info(`Got channels: ${ cache.watchedChannels }`);
    }
    return num;
}

export async function getWatchedWords(guild: Snowflake): Promise<IWatchedWord[]> {
    try {
        return WatchedWord.find({ guild });
    } catch (err) {
        logger.error('Failed to obtain search results:');
        logger.error(err);
        return [];
    }
}

export default WatchedWord;