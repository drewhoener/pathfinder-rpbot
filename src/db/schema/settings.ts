import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../util/logging';
import { Snowflake } from 'discord.js';

export interface IAutoRoleManager {
    lastChecked: number;
}

export interface ISettings extends Document {
    guild: string;
    info?: string | undefined;
    quest_roles: string[];
    weather?: string | undefined;
    sheets?: string | undefined;
    maxCharacterSheets: number;
    watchedChannels: string[];
    censusData?: string | undefined;
    auto_role: IAutoRoleManager | undefined;

    pushChannel(channelId: Snowflake): void;

    removeChannel(channelId: Snowflake | Snowflake[]): void;
}

const SettingsSchema: Schema<ISettings> = new Schema<ISettings>({
    guild: { type: String, required: true, unique: true, index: true },
    info: { type: String, default: null },
    // eslint-disable-next-line @typescript-eslint/camelcase
    quest_roles: { type: [String], default: [] },
    weather: { type: String, default: null },
    sheets: { type: String, default: null },
    maxCharacterSheets: { type: Number, default: 1 },
    watchedChannels: { type: [String], default: [] },
    censusData: { type: String, default: undefined },
});

SettingsSchema.methods.pushChannel = function(channelId: Snowflake): void {
    if (this.watchedChannels.some((channel: string) => channel === channelId)) {
        return;
    }
    this.watchedChannels.push(channelId);
};

SettingsSchema.methods.removeChannel = function(channelId: Snowflake | Snowflake[]): void {
    this.watchedChannels = this.watchedChannels.filter((channel: string) => {
        if (Array.isArray(channelId)) {
            for (const channelString of channelId) {
                if (channel === channelString) {
                    return false;
                }
            }
            return true;
        }
        return channel !== channelId;
    });
};

const Settings: Model<ISettings> = model<ISettings>('settings', SettingsSchema);

export async function getSettings(guild: Snowflake): Promise<ISettings | null> {
    try {
        const result: ISettings | null = await Settings.findOne({ guild });
        if (!result) {
            return new Settings({ guild });
        }
        return result;
    } catch (err) {
        logger.error('Failed to obtain search results:');
        logger.error(err);
        return null;
    }
}

export async function getWatchedChannels(guild: Snowflake): Promise<string[]> {
    const settings: ISettings | null = await getSettings(guild);
    if (!settings) {
        return [];
    }
    return settings.watchedChannels;
}

export default Settings;