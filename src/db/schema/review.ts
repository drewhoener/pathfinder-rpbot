import { Snowflake } from 'discord.js';
import { Document, model, Model, Schema } from 'mongoose';

export interface ISurveyResults {
    anonymous: boolean;
    gmId: Snowflake;
    userId: Snowflake;
    username: string;
    description: string;
    sessionRunningEnjoyment: Enjoyment;
    contentEnjoyment: Enjoyment;
    wantFollowUp: YesNoEmoji | string;
    followUpComments: string;
    likedDisliked: string;
    comments: string;
}

export interface ISurveyResultsDocument extends ISurveyResults, Document {
}

export enum YesNoEmoji {
    YES = '\u{1F44D}',
    NO = '\u{1F44E}',
    WRITE = '\u{1F4DD}',
}

export enum Enjoyment {
    LOVE = '\u{02764}\u{0FE0F}',
    LIKE = '\u{1F44D}',
    NEUTRAL = '\u{1F937}\u{0200D}\u{02642}\u{0FE0F}',
    DISLIKE = '\u{1F44E}',
    HATE = '\u{02620}\u{0FE0F}',
    UNANSWERED = 'Not Answered',
}

const surveyResultSchema = new Schema({
    anonymous: { type: Boolean, default: true },
    gmId: { type: String, required: true },
    userId: { type: String, required: true },
    username: { type: String, required: true },
    description: { type: String },
    sessionRunningEnjoyment: { type: String, required: true },
    contentEnjoyment: { type: String, required: true },
    wantFollowUp: { type: String, required: true },
    followUpComments: { type: String },
    likedDisliked: { type: String },
    comments: { type: String },
});

const Review: Model<ISurveyResultsDocument> = model('review', surveyResultSchema);
export default Review;