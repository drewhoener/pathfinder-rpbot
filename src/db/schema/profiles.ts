import { Document, model, Model, Schema } from 'mongoose';
import { Snowflake } from 'discord.js';
import axios from 'axios';
import logger from '../../util/logging';
import { ClientContext } from '../../handler/ClientContext';

const { webkey, website } = ClientContext.config.auth.webAuth;

const constructApiPost = (method: string, params: object): IPostRequest => {
    return {
        'jsonrpc': '2.0',
        'id': Math.round(Math.random() * (999999 - 100000) + 100000),
        'method': method,
        'params': params,
    };
};

export interface IWebsiteProfile {
    user_id: string;
    username: string;
    avatar: string;
    is_online: boolean;
    is_nsfw: boolean;
    cover: string;
    cover_timestamp: string;
    cover_ext: string;
    cover_premade: string;
    cover_image: string;
    quote: string;
    location: string;
    number_views: string;
    joined: string;
    last_login: string;
    last_activity: string;
    friend_type: boolean;
    favorite: boolean;
}

export interface IPostRequest {
    jsonrpc: string;
    id: number;
    method: string;
    params: object;
}

export interface IEnjinLink {
    guild: string;
    enjin: string;
}

export interface IPendingProfile {
    guild: string;
    name: string;
}

export interface IProfile extends Document {
    discord: string;
    username: string;
    guilds: IEnjinLink[];
    pending?: IPendingProfile;

    hasEnjin(guild: Snowflake): boolean;

    getEnjin(guild: Snowflake): IEnjinLink | undefined;

    isPending(): boolean;

    getPoints(guild: Snowflake): Promise<number>;

    addPoints(guild: Snowflake, points: number): Promise<void>;

    removePoints(guild: Snowflake, points: number): Promise<void>;

    getProfileData(enjinProfile: string): Promise<IWebsiteProfile>;
}

const enjinLinkSchema: Schema = new Schema({
    guild: { type: String, required: true, index: true },
    enjin: { type: String, required: true },
});

const profileSchema: Schema<IProfile> = new Schema<IProfile>({
    discord: { type: String, required: true, unique: true, index: true },
    username: { type: String, required: true },
    guilds: { type: [enjinLinkSchema], default: [] },
    pending: {
        type: {
            guild: { type: String, required: true },
            name: { type: String, required: true },
        },
        default: undefined,
    },
});

profileSchema.methods.hasEnjin = function(guild: Snowflake): boolean {
    return this.guilds.some((elem: IEnjinLink) => elem.guild === guild);
};

profileSchema.methods.getEnjin = function(guild: Snowflake): IEnjinLink | undefined {
    return this.guilds.find((elem: IEnjinLink) => elem.guild === guild);
};

profileSchema.methods.isPending = function(): boolean {
    return this.pending != null;
};

profileSchema.methods.getPoints = async function(guild: Snowflake): Promise<number> {
    const guildProfile: IEnjinLink | undefined = this.getEnjin(guild);
    if (!guildProfile) {
        logger.error(`Couldn't add points to user ${ this.discord }/${ this.username }`);
        return -1;
    }
    const data = constructApiPost('Points.get', { 'api_key': webkey, 'user_id': guildProfile.enjin });

    return axios.post(website + '/api/v1/api.php', data)
        .then(response => {
            logger.silly(`Got points for user id ${ this.discord }/${ this.username }. Point total is ${ response.data.result }`);
            return response.data.result;
        })
        .catch(error => {
            logger.error(error);
            return -1;
        });
};

profileSchema.methods.addPoints = async function(guild: Snowflake, points: number): Promise<void> {
    const guildProfile: IEnjinLink | undefined = this.getEnjin(guild);
    if (!guildProfile) {
        logger.error(`Couldn't add points to user ${ this.discord }/${ this.username }`);
        return;
    }
    const data = constructApiPost('Points.add', { 'api_key': webkey, 'user_id': guildProfile.enjin, 'points': points });

    return axios.post(website + '/api/v1/api.php', data)
        .then(response => {
            logger.silly(`Added ${ points } point(s) to user id ${ this.discord }/${ this.username }. Point total is ${ response.data.result }`);
        })
        .catch(error => {
            logger.error(error);
        });
};

profileSchema.methods.removePoints = async function(guild: Snowflake, points: number): Promise<void> {
    const guildProfile: IEnjinLink | undefined = this.getEnjin(guild);
    if (!guildProfile) {
        logger.error(`Couldn't remove points from user ${ this.discord }/${ this.username }`);
        return;
    }
    const data = constructApiPost('Points.remove', {
        'api_key': webkey,
        'user_id': guildProfile.enjin,
        'points': points,
    });

    return axios.post(website + '/api/v1/api.php', data)
        .then(response => {
            logger.silly(`Removed ${ points } point(s) from user id ${ this.discord }/${ this.username }. Point total is ${ response.data.result }`);
        })
        .catch(error => {
            logger.error(error);
        });
};

profileSchema.methods.getProfileData = async function(enjinId: string): Promise<IWebsiteProfile> {
    const data = constructApiPost('Profile.getProfile', { 'user_id': enjinId });

    return axios.post(website + '/api/v1/api.php', data)
        .then(response => {
            return response.data.result;
        })
        .catch(error => {
            logger.error(error);
        });
};


const Profile: Model<IProfile> = model('profiles', profileSchema);
export default Profile;