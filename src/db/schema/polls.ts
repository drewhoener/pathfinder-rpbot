import { Document, model, Model, Schema } from 'mongoose';
import { Snowflake } from 'discord.js';

export interface IPoll extends Document {
    guild: Snowflake;
    channel: Snowflake;
    messageId: Snowflake;
    question: string;
    options: string[];
    pollId: string;
    end: number;
}

const pollSchema: Schema = new Schema({
    guild: { type: String, required: true },
    channel: { type: String, required: true },
    messageId: { type: String, required: true },
    question: { type: String, required: true },
    options: { type: [String], required: true },
    pollId: { type: String, unique: true, required: true, index: true },
    end: { type: Schema.Types.Number, required: true },
});

const Poll: Model<IPoll> = model<IPoll>('polls', pollSchema);

export default Poll;