import { Document, model, Model, Schema, Types } from 'mongoose';
import { Snowflake } from 'discord.js';

export interface IUserPlay {
    userId: Snowflake;
    time: number;
}

export interface IHistoryEntry extends Types.Subdocument {
    members: IUserPlay[];
    stamp: number;
    date: string;
    time?: number;
}

export interface IHistory extends Document {
    guild: Snowflake;
    channel: Snowflake;
    channelName: string;
    dates: IHistoryEntry[];
}

const historySchema: Schema = new Schema({
    guild: { type: String, required: true, index: true },
    channel: { type: String, required: true, index: true },
    channelName: { type: String, required: true },
    dates: {
        type: [{
            members: [
                {
                    userId: String,
                    time: Schema.Types.Number,
                }],
            stamp: Schema.Types.Number,
            date: String,
            time: { type: Schema.Types.Number, required: false },
        }],
        default: [],
    },
});

export type IHistoryModel = IHistory & { dates: Types.Array<IHistoryEntry> };

const History: Model<IHistoryModel> = model<IHistoryModel>('history', historySchema);

export default History;