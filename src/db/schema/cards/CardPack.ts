import Card, { CardRarity, ICard } from './Card';
import { ObjectId } from 'mongodb';
import { Document, Schema, SchemaTypes } from 'mongoose';

export type Boons = {
    [K in CardRarity]: string;
};

export type Cards = {
    [K in CardRarity]: ObjectId[];
};

export interface ICardPack {
    name: string;
    boons: Boons;
    cards: Cards;

    getCards(arg0: CardRarity): Promise<ICard[]>;

    getBoon(arg0: CardRarity): string;
}

export interface ICardPackDocument extends ICardPack, Document {

}

const cardPackSchema: Schema<ICardPackDocument> = new Schema<ICardPackDocument>({
    name: { type: String, required: true },
    boons: {
        [CardRarity.COMMON]: String,
        [CardRarity.UNCOMMON]: String,
        [CardRarity.RARE]: String,
        [CardRarity.EPIC]: String,
        [CardRarity.DEIFIC]: String,
    },
    cards: {
        [CardRarity.COMMON]: [SchemaTypes.ObjectId],
        [CardRarity.UNCOMMON]: [SchemaTypes.ObjectId],
        [CardRarity.RARE]: [SchemaTypes.ObjectId],
        [CardRarity.EPIC]: [SchemaTypes.ObjectId],
        [CardRarity.DEIFIC]: [SchemaTypes.ObjectId],
    },
});

cardPackSchema.methods.getCards = async function(rarity: CardRarity): Promise<ICard[]> {
    const rarityIds = this.cards[rarity];
    let cards: ICard[];
    try {
        cards = await Card.find({ '$in': rarityIds });
    } catch (e) {
        cards = [];
    }

    return cards;
};

cardPackSchema.methods.getBoon = function(rarity: CardRarity): string {
    return this.boons[rarity];
};