import { Document, model, Model, Schema } from 'mongoose';

export enum CardRarity {
    COMMON = 'COMMON',
    UNCOMMON = 'UNCOMMON',
    RARE = 'RARE',
    EPIC = 'EPIC',
    DEIFIC = 'DEIFIC',
}

export interface ICardDesign {
    name: string;
    // cardDesign: Buffer;
    image: Buffer;
    rarity: CardRarity;
}

export interface ICard extends ICardDesign, Document {

}

const cardSchema: Schema<ICard> = new Schema<ICard>({
    name: { type: String, required: true },
    // cardDesign: { type: Buffer, required: true },
    image: { type: Buffer, required: true },
    rarity: {
        type: String,
        enum: CardRarity,
        required: true,
    },
});

const Card: Model<ICard> = model('card', cardSchema);

export default Card;