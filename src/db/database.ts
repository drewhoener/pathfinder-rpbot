import mongoose from 'mongoose';
import util from 'util';
import { ClientContext } from '../handler/ClientContext';

const DBUrl = 'mongodb://%sdrewhoener.com/%s?authSource=%s';

let database = null;

export async function connect() {
    const { authSource, database: db, pass, user } = ClientContext.config.auth.dbAuth;
    console.log('Connecting to MongoDB...');
    let auth = '';
    if (user && pass) {
        auth = `${ user }:${ pass }@`;
    }
    const connectionURL = util.format(DBUrl, auth, db, authSource);
    // console.log(connectionURL);

    mongoose.set('useCreateIndex', true);
    await mongoose.connect(connectionURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    database = mongoose.mongo;
    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
}

export function close(): Promise<void> {
    return mongoose.disconnect();
}

export { database };