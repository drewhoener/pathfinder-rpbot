import { promisify } from 'util';
import logger from '../util/logging.js';
import { google, sheets_v4 as sheetsApi } from 'googleapis';
import { GaxiosResponse } from 'gaxios';
import { JWT } from 'google-auth-library';
import fs from 'fs';
import JSON from 'circular-json';
import { ClientContext } from '../handler/ClientContext';
import os from 'os';
import { IClientContext } from '../classes/types/context';

const readFileAsync = promisify(fs.readFile);
// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.

const getTokenPath = (): string => {
    if (os.platform() === 'win32') {
        return `${ process.env.APPDATA }/discord-bot/google-token.json`;
    }
    return '/opt/discord-bot/google-token.json';
};

class SheetAPI {
    private readonly _sheetId: string;
    private readonly tokenPath: string;
    private readonly context: IClientContext;

    constructor(sheetId: string, context: IClientContext = ClientContext) {
        this._sheetId = sheetId;
        this.tokenPath = getTokenPath();
        this.context = context;
    }

    async getSheetsObj(): Promise<sheetsApi.Sheets | undefined> {
        const authToken = await this.getClient();
        if (!authToken) {
            return;
        }
        return google.sheets({ version: 'v4', auth: authToken });
    }

    async getItems(): Promise<any[][] | undefined> {
        console.log('Starting');
        try {
            const sheetNames = await this.getSheetNames();
            if (!sheetNames) {
                logger.error('Failed to get names of sheets in getItems spreadsheet');
                return;
            }
            logger.info(sheetNames);
            const response = await this.getRange(`${ sheetNames[0] }`);
            if (response) {
                const rows = response.data.values;
                if (rows != null && rows.length) {
                    return rows;
                }

                logger.info('No data found.');
            }
        } catch (e) {
            console.log('Error');
            console.error(e);
            logger.error(e);
        }
    }

    async createSheet(name: string): Promise<number | undefined> {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }
        const requestBody: sheetsApi.Schema$BatchUpdateSpreadsheetRequest = {
            requests: [
                {
                    addSheet: {
                        properties: {
                            title: name,
                        },
                    },
                },
            ],
        };
        try {
            const response = await sheets.spreadsheets.batchUpdate({
                spreadsheetId: this._sheetId,
                requestBody,
            });
            if (!response || !response.data || !response.data.replies) {
                return;
            }

            const reply = response.data.replies[0];
            const addSheetReply = reply.addSheet;
            if (!addSheetReply) {
                return;
            }

            const properties = addSheetReply.properties;
            if (!properties) {
                return;
            }
            const id = properties.sheetId;

            return (id === null || id === undefined) ? undefined : id;
        } catch (e) {
            logger.error(e);
            return;
        }
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async setData(range: string, values: any[][], majorDimension = 'ROWS'): Promise<unknown> {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }
        return sheets.spreadsheets.values.update({
            spreadsheetId: this._sheetId,
            range,
            valueInputOption: 'RAW',
            requestBody: {
                range,
                majorDimension,
                values,
            },
        });
    }

    async getSheetNames(): Promise<string[] | undefined> {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }
        const result = await sheets.spreadsheets.get({ spreadsheetId: `${ this._sheetId }` });
        if (!result) {
            return;
        }
        const names: string[] = [];
        if (!result.data.sheets) return;
        for (const sheetData of result.data.sheets) {
            if (!sheetData.properties) continue;
            if (sheetData.properties.title) {
                names.push(sheetData.properties.title);
            }
        }
        return names;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async insertLineAtRange(sheetId: number, startRow: number, numRows: number, data: any[]) {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }

        const request = {
            spreadsheetId: this._sheetId,
            requestBody: {
                requests: [
                    {
                        insertRange: {
                            range: {
                                sheetId: sheetId,
                                startRowIndex: startRow,
                                endRowIndex: startRow + numRows,
                            },
                            shiftDimension: 'ROWS',
                        },
                    },
                    {
                        pasteData: {
                            data: data.join('__'),
                            type: 'PASTE_NORMAL',
                            delimiter: '__',
                            coordinate: {
                                sheetId: sheetId,
                                rowIndex: startRow,
                                columnIndex: 0,
                            },
                        },
                    },
                ],
            },
        };

        return sheets.spreadsheets.batchUpdate(request);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async appendLine(range: string, data: any[]) {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }

        return sheets.spreadsheets.values.append({
            spreadsheetId: `${ this._sheetId }`,
            range: `${ range }`,
            insertDataOption: 'INSERT_ROWS',
            valueInputOption: 'USER_ENTERED',
            requestBody: { values: [data] },
        });
    }

    async getRangeAsRows(range: string, direction = 'ROWS'): Promise<any[] | undefined> {
        const response = await this.getRange(`${ range }`, direction);
        if (response) {
            const rows = response.data.values;
            if (rows != null && rows.length) {
                return rows;
            }

            logger.info('No data found.');
        }
    }

    async getRange(range: string, direction = 'ROWS'): Promise<GaxiosResponse<sheetsApi.Schema$ValueRange> | undefined> {
        const sheets = await this.getSheetsObj();
        if (!sheets) {
            return;
        }
        return sheets.spreadsheets.values.get({
            spreadsheetId: `${ this._sheetId }`,
            range: `${ range }`,
            majorDimension: direction,
        });
    }

    async getClient(): Promise<JWT> {
        const authToken: JWT = new google.auth.JWT(undefined, undefined, undefined, SCOPES);
        authToken.fromJSON(this.context.config.auth.charSheetAuth);

        // Check if we have previously stored a token.
        return readFileAsync(this.tokenPath)
            .then((token: Buffer) => {
                authToken.setCredentials(JSON.parse(token.toString()));
                return authToken;
            }).catch(() => {
                logger.warn('Failed to find auth file, creating a new one');
                return this.getNewToken(authToken);
            });
    }

    /**
     * Get and store new token after prompting for user authorization, and then
     * execute the given callback with the authorized OAuth2 client.
     */
    async getNewToken(authToken: JWT): Promise<JWT> {
        return authToken.authorize().then(credential => {
            authToken.setCredentials(credential);
            fs.writeFile(this.tokenPath, JSON.stringify(credential), (err: any) => {
                if (err) return logger.error(err);
                logger.info(`Token stored to ${ this.tokenPath }`);
            });
            return authToken;
        });
    }
}

export default SheetAPI;