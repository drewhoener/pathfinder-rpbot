// Heavily based on code from drow <drow@bin.sh>
// Thank you very much :)
import {Random} from 'random-js';
import {RollTable, RollTableResult} from '../classes/types/weather';

export const isNumber = (val: string): boolean => /^\d+$/.test(val);
const random = new Random();
const dice = /(\d*)d(\d+)/i;
// Any pos/neg decimal or int +/- any pos/neg decimal or int
const add = /([+-]?\d+\.\d+|[+-]?\d+)\s*(\+|-)\s*([+-]?\d+\.\d+|[+-]?\d+)/;

export function randomFloat(): number {
    return random.realZeroToOneInclusive();
}

function randomWithModifier(max: number, mod: number): number {
    return random.integer(0, max) + mod;
}

export function rollDice(numDice: string | number, sides: string | number): number {
    numDice = parseInt(`${ numDice }`);
    numDice = isNaN(numDice) || numDice < 1 ? 1 : numDice;
    sides = parseInt(`${ sides }`);
    sides = isNaN(sides) || sides < 0 ? 0 : sides;
    let total = 0;
    for (let i = 0; i < numDice; i++) {
        total += random.integer(1, sides);
    }
    return total;
}

function applyOperandMath(a: number | string, op: string, b: number | string): number {
    a = parseFloat(`${ a }`);
    if (isNaN(a)) a = 0.0;
    b = parseFloat(`${ b }`);
    if (isNaN(b)) b = 0.0;

    if (op === '++') return ++a;
    if (op === '--') return --a;
    if (op === '*') return a * b;
    if (op === '/') return a / b;
    if (op === '%') return a % b;
    if (op === '+') return a + b;
    if (op === '-') return a - b;

    return 0;
}

export function rollDiceToString(input: string): string {
    if (!input || !input.length) {
        return '0';
    }
    let match;
    let result: number;
    while ((match = dice.exec(input))) {
        result = rollDice(match[1], match[2]);
        input = input.replace(dice, `${ result }`);
    }
    while ((match = add.exec(input))) {
        result = applyOperandMath(match[1], match[2], match[3]);
        input = input.replace(add, `${ result }`);
    }
    return input;
}

export function rollDiceToInt(input: string): number {
    const result: string = rollDiceToString(input);
    if (!result || isNaN(parseInt(result))) {
        return 0;
    }
    return parseInt(result);
}

export function doesRollSucceed(roll = '1d100', percent = 50): boolean {
    const result = rollDiceToInt(roll);
    return result < percent;
}

export function getEntryFromRollTable(table: RollTable, str: string): RollTableResult {
    const roll = rollDiceToInt(str);
    const regex = /^(\d{1,4})(-(\d{1,4}))?$/;
    for (const key in table) {
        if (!table.hasOwnProperty(key)) {
            continue;
        }
        if (!regex.test(key)) {
            continue;
        }
        const result = regex.exec(key);
        if (!result || isNaN(parseInt(result[1]))) {
            continue;
        }
        const min = parseInt(result[1]);
        const max = parseInt(result[3]);
        let found = false;
        if (!max && roll === min) {
            found = true;
        }
        if (roll >= min && (max && roll <= max)) {
            found = true;
        }
        if (found) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            if (isNaN(table[key])) {
                if ((typeof table[key]) === 'string') {
                    return table[key];
                }
                return Object.assign({}, table[key]);
            }
            return table[key];
        }
    }
}
