import logger from './logging';
import { isEmpty } from 'lodash';
import { Client, GuildChannel, Message, Snowflake, TextChannel } from 'discord.js';
import JSON from 'circular-json';
import Poll from '../db/schema/polls';
import { AsyncForEachType } from '../classes/types/context';

export const FILE_REGEX = /\.[jt]s$/;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function asyncForEach(array: Array<AsyncForEachType>, callback: Function): Promise<void> {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

export function transpose<T>(arr: T[][]): T[][] {
    return arr[0].map((_, c) => arr.map(r => r[c]));
}

export function sleep(ms: number): Promise<unknown> {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

export const codeBlock = (content: string): string => {
    return `\`\`\`${ content }\`\`\``;
};

export const getChannel = (client: Client, guildID: Snowflake, channelID: Snowflake): TextChannel | null => {
    const guildObject = client.guilds.cache.get(guildID);
    if (!guildObject) {
        return null;
    }
    const channel = guildObject.channels.cache.get(channelID);
    if (!channel || !(channel instanceof TextChannel)) {
        return null;
    }
    return channel;
};

export function capitalize(stringArr: (string[] | string)): string {
    if (!Array.isArray(stringArr)) {
        stringArr = [stringArr];
    }
    let str = '';
    for (const elem of stringArr) {
        str += ` ${ elem.charAt(0).toUpperCase() }${ elem.slice(1).toLowerCase() }`;
    }
    return str.trim();
}

export function toClazz(obj: object, proto: object): object {
    return Object.setPrototypeOf(obj, proto);
}

export async function fetchPollMessage(client: Client, guildID: Snowflake, channelID: Snowflake, messageID: Snowflake, pollId: string): Promise<Message | undefined> {
    const guild = client.guilds.cache.get(guildID);
    if (!guild || isEmpty(guild)) {
        logger.info('Couldn\'t find the server this poll is supposed to be in. This is a bug');
        return undefined;
    }
    const guildChannel: GuildChannel | undefined = guild.channels.cache.get(channelID);
    if (!guildChannel || isEmpty(guildChannel) || !(guildChannel instanceof TextChannel)) {
        logger.error('Couldn\'t find the channel to complete the poll!');
        return undefined;
    }
    const pollMessage: Message = await guildChannel.messages.fetch(messageID);
    if (!pollMessage || isEmpty(pollMessage)) {
        logger.info(`Couldn't find the original poll message! This is a bug\nChannel name: ${ guildChannel.name }`);
        logger.info(`Message Id: ${ messageID }`);
        logger.info(`Message: ${ JSON.stringify(pollMessage, null, 4) }`);
        logger.info(`Raw Message: ${ pollMessage }`);
        Poll.deleteOne({ pollId });
        return undefined;
    }
    if (pollMessage.partial) {
        return await pollMessage.fetch();
    }
    return pollMessage;
}

export function average(data: number[]): number {
    const sum = data.reduce((curSum, value) => curSum + value, 0);
    return sum / data.length;
}

export function standardDeviation(values: number[]): number {
    const avg = average(values);

    const squareDiffs = values.map(value => {
        const diff = value - avg;
        return diff * diff;
    });

    const avgSquareDiff = average(squareDiffs);
    return Math.sqrt(avgSquareDiff);
}

export function modField(obj: any, field: any, val: any) {
    if (!field || isNaN(val)) {
        return;
    }
    obj[`${ field }`] += parseInt(val);
}

export function assignField(obj: any, field: any, val: any, checkNull = true, valRange = [
    Number.MIN_SAFE_INTEGER,
    Number.MAX_SAFE_INTEGER,
]) {
    let isNull;
    if (isNaN(val)) {
        isNull = !val;
    } else if (valRange.length >= 2) {
        isNull = val < valRange[0] || val > valRange[1];
    } else {
        isNull = false;
    }
    if (!field || (checkNull && isNull)) {
        return;
    }
    obj[`${ field }`] = val;
}

export function clone(obj: any) {
    if (obj == null || typeof obj != 'object') return obj;
    const copy = obj.constructor();
    for (const attr in obj) {
        if (obj.hasOwnProperty(attr)) {
            copy[attr] = obj[attr];
        }
    }
    return copy;
}