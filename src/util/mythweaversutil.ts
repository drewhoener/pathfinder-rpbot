import { IRawCharacterSheet } from '../classes/types/context';
import axios from 'axios';
import util from 'util';
import { keys } from 'ts-transformer-keys';
import { sleep } from './utils';
import logger from './logging';

export const SNOWFLAKE_REGEX = /^\d+$/i;
export const ONLY_DIGITS = /^[+-]?\d+$/;
export const TokenRegex = /^(\d+):(\d+):((?:\S|\s)+)$/i;
export const MW_SHEET_REGEX = /(?:https?:\/\/)?www\.myth-weavers\.com\/sheet\.html#id=(\d+)/i;
export const MW_API_LINK = 'https://www.myth-weavers.com/api/v1/sheets/sheets/%s';
export const MW_API_TIME = 1000;

const validStatus = [
    403,
    404,
    '404',
    '403',
];

export const getMythWeaversRaw = async (sheetLink: string, sheetId: string): Promise<IRawCharacterSheet | undefined> => {
    return axios.get(util.format(MW_API_LINK, sheetId))
        .then(({ data }) => {
            const sheetData = Object.assign({}, data.sheetdata.data) as IRawCharacterSheet;
            const objKeys = keys<IRawCharacterSheet>();
            const ret = {} as IRawCharacterSheet;
            for (const key of objKeys) {
                if (!key) {
                    continue;
                }
                ret[key] = sheetData[key] ?? 'Not Specified';
            }
            ret.URL = sheetLink;
            return ret;
        })
        .catch(err => {
            if (err.response && err.response.hasOwnProperty('status') && validStatus.some(status => status === err.response.status)) {
                logger.info(`Sheet ${ sheetId } returned ${ err.response.status }`);
            } else {
                console.error(err);
            }
            return undefined;
        });
};

export const requestMythSheetsBulk = async (sheets: string[], sleepTime = MW_API_TIME, printInterval = 1): Promise<IRawCharacterSheet[]> => {
    let num = 0;
    const links: { id: string; link: string }[] = sheets
        .filter(str => MW_SHEET_REGEX.test(str))
        .map(sheet => ({ arr: MW_SHEET_REGEX.exec(sheet), link: sheet }))
        .filter(o => !(!o || !o.arr || !o.arr[1]))
        .map(o => {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            const arr = o.arr!;
            return {
                id: arr[1],
                link: o.link,
            };
        });

    const results: IRawCharacterSheet[] = [];
    const time = new Date().valueOf();
    for (let i = 0; i < links.length; i++) {
        num++;
        const linkObj = links[i];
        const rawSheet: IRawCharacterSheet | undefined = await getMythWeaversRaw(linkObj.link, linkObj.id);
        if (rawSheet) {
            if (printInterval === 1 || num % printInterval === 0) {
                logger.info(`Got Sheet ${ i + 1 }/${ links.length } at ${ new Date().valueOf() - time }ms`);
            }
            if (!rawSheet.DiscordName) {
                rawSheet.DiscordName = 'undefined';
            }
            results.push(rawSheet);
        } else {
            logger.info(`Discarded Sheet ${ i + 1 }/${ links.length } [ ${ linkObj.link } ] because of undefined at ${ new Date().valueOf() - time }ms`);
        }
        await sleep(sleepTime);
    }

    return results;
};