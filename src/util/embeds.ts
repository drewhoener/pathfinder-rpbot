import { MessageEmbed, User } from 'discord.js';

const exampleEmbed = new MessageEmbed()
    .setColor('#0099ff')
    .setTitle('Some title')
    .setURL('https://discord.js.org/')
    .setAuthor('Some name', 'https://i.imgur.com/PGci2eX.png', 'https://discord.js.org')
    .setDescription('Some description here')
    .setThumbnail('https://i.imgur.com/wSTFkRM.png')
    .addField('Regular field title', 'Some value here')
    .addField('\u200b', '\u200b')
    .addField('Inline field title', 'Some value here', true)
    .addField('Inline field title', 'Some value here', true)
    .addField('Inline field title', 'Some value here', true)
    .setImage('https://i.imgur.com/wSTFkRM.png')
    .setTimestamp()
    .setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png',
    );

function getRandomArbitrary(min: number, max: number): number {
    return Math.random() * (max - min) + min;
}

function randColor(): number {
    const red = Math.floor(getRandomArbitrary(0, 256)) << 16;
    const green = Math.floor(getRandomArbitrary(0, 256)) << 8;
    const blue = Math.floor(getRandomArbitrary(0, 256));

    return red | green | blue;
}

export const generateEmbed = (botUser: User, title: string | null, url: string | null, description: string | null, username?: string): MessageEmbed => {
    const embed = new MessageEmbed().setColor(randColor());

    const userStr = username ? username : botUser.username;
    const avatar = botUser.avatarURL();
    if (url) {
        embed.setURL(url);
    }

    if (avatar) {
        embed.setAuthor(userStr, avatar);
    } else {
        embed.setAuthor(userStr);
    }

    if (title) {
        embed.setTitle(title);
    }
    if (description) {
        embed.setDescription(description);
    }

    return embed;
};

export const blankEmbed = (): MessageEmbed => {
    return new MessageEmbed()
        .setColor(randColor());
};