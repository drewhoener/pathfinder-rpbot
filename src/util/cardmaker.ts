import sharp from 'sharp';

export const CARD_LENGTH = 1050;
export const CARD_WIDTH = 750;

const getRoundedCorners = (width: number, height: number, rx = 50, ry = 50): Buffer => {
    return Buffer.from(
        `<svg><rect x="0" y="0" width="${ width }" height="${ height }" rx="${ rx }" ry="${ ry }"/></svg>`
    );
};

export const resizeImage = async (image: Buffer, width = 500, height = 500, roundEdges = true): Promise<Buffer> => {
    let resized = sharp(image)
        .resize(width, height);

    if (roundEdges) {
        resized = resized.composite([
            {
                input: getRoundedCorners(width, height),
                blend: 'dest-in',
            },
        ]);
    }

    return resized.png()
        .toBuffer();
};