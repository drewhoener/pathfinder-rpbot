import { IClientAuth, IDatabaseAuth, IServiceAccountAuth, IWebsiteAuth } from '../classes/types/context';

const isObject = (x: unknown): x is Record<string, unknown> => {
    return typeof x === 'object' && x != null;
};

const validateAllKeys = <T>(schema: Record<keyof T, string>, obj: unknown): obj is T => {

    if (!isObject(obj)) {
        return false;
    }

    const missingProperties = Object.keys(schema)
        .filter(key => obj[key] === undefined)
        .map(key => key as keyof T)
        .map(key => `Document is missing ${ key } ${ schema[key] }`);

    if (missingProperties.length) {
        throw new Error(`Missing properties: [${ missingProperties.join(', ') }]`);
    }

    return missingProperties.length === 0;
};

export const validateServiceAuth = (obj: unknown): obj is IServiceAccountAuth => {
    const schema: Record<keyof IServiceAccountAuth, string> = {
        // eslint-disable-next-line @typescript-eslint/camelcase
        auth_provider_x509_cert_url: '',
        // eslint-disable-next-line @typescript-eslint/camelcase
        auth_uri: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        client_email: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        client_id: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        client_x509_cert_url: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        private_key: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        private_key_id: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        project_id: 'string',
        // eslint-disable-next-line @typescript-eslint/camelcase
        token_uri: 'string',
        type: 'string',
    };

    return validateAllKeys(schema, obj);
};

export const validateWebAuth = (obj: unknown): obj is IWebsiteAuth => {
    const schema: Record<keyof IWebsiteAuth, string> = {
        webkey: 'string',
        website: 'string',
    };

    return validateAllKeys(schema, obj);
};

export const validateClientAuth = (obj: unknown): obj is IClientAuth => {
    const schema: Record<keyof IClientAuth, string> = {
        token: 'string',
    };

    return validateAllKeys(schema, obj);
};

export const validateDatabaseAuth = (obj: unknown): obj is IDatabaseAuth => {
    const schema: Record<keyof IDatabaseAuth, string> = {
        authSource: 'string',
        database: 'string',
        pass: 'string',
        user: 'string',
    };

    return validateAllKeys(schema, obj);
};

export type ValidatorFunction = (x: unknown) => boolean | void;