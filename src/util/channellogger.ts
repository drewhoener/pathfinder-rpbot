import logger from './logging';
import { TextChannel } from 'discord.js';

class ChannelLogger {
    private _message: string;
    private _channel: TextChannel | undefined;

    constructor() {
        this._message = '';
    }

    set channel(ch: TextChannel | undefined) {
        this._channel = ch;
    }

    log = (message: string | null): void => {
        if (this._channel) {
            if (this._message.length && message === null) {
                this._channel.send(`\`\`\`${ this._message }\`\`\``);
                // .then(message => message.react('\u{1F5A8}'));
                this._message = '';
            } else if (message) {
                this._message += `${ message }\n`;
            }
        }

        if (message) {
            logger.info(message);
        }
    };

    info(message: string): void {
        this.log(message);
    }
}

export default ChannelLogger;