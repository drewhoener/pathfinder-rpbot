import logger from '../util/logging.js';
import { Message, MessageReaction, Snowflake, User } from 'discord.js';
import { CommandNames, IClientContext, Reacts } from '../classes/types/context';
import { ICommandSheet } from '../commands/CommandSheet';
import { PendingSheet } from '../db/schema/charactersheets';

const deleteClonkMessage = async (ctx: IClientContext, messageReaction: MessageReaction, user: User): Promise<boolean> => {
    const { client } = ctx;
    if (messageReaction.emoji.toString() !== Reacts.TRASHCAN) {
        return false;
    }
    if (!client.user || user.bot) {
        return false;
    }
    if (messageReaction.partial) {
        messageReaction = await messageReaction.fetch();
    }
    if (messageReaction.message.author.id !== client.user.id) {
        return false;
    }

    if (!messageReaction.users.cache.has(client.user.id)) {
        if (!(await messageReaction.users.fetch()).has(client.user.id)) {
            return false;
        }
    }
    return await messageReaction.message.delete().then(() => {
        logger.info('Trashed message');
        return true;
    }).catch((err) => {
        logger.error(err);
        return false;
    });
};

const changeVote = async (ctx: IClientContext, messageReaction: MessageReaction, user: User): Promise<void> => {
    const { message } = messageReaction;
    if (user.bot || message.deleted) {
        return;
    }
    if (messageReaction.partial) {
        messageReaction = await messageReaction.fetch();
    }
    const users = [];
    for (const reaction of message.reactions.cache.array()) {
        users.push({
            react: reaction,
            users: await reaction.users.fetch(),
        });
    }

    if (!users.some(reactUsersPair => reactUsersPair.react.emoji.toString() === Reacts.POLL && reactUsersPair.users.some(reactedUser => reactedUser.bot))) {
        return;
    }

    let removed = 0;
    for (const reaction of message.reactions.cache.array()) {
        if (reaction.emoji.toString() === messageReaction.emoji.toString()) {
            continue;
        }
        reaction.users.remove(user).then(() => removed++).catch(error => logger.info('Failed to remove react', error));
    }
};

const approveSheet = async (ctx: IClientContext, messageReaction: MessageReaction, user: User): Promise<Message | void> => {
    const { client } = ctx;
    const { message } = messageReaction;
    if (!message.guild || user.bot || message.deleted || !client.user) {
        return;
    }
    if (message.partial) {
        await message.fetch();
    }
    if (message.author.id !== client.user.id) {
        return;
    }
    const { emoji } = messageReaction;
    if (emoji.toString() !== Reacts.APPROVE && emoji.toString() !== Reacts.DENY) {
        return;
    }

    const guildMember = message.guild.members.cache.get(user.id);
    if (!guildMember) {
        return;
    }
    const hasRole = guildMember.roles.cache.has('446705480367079446')
        || guildMember.roles.cache.has('448642903531847680')
        || guildMember.roles.cache.has('560464175680192512')
        || user.id === '173923836054470656';

    console.log(`User has role: ${ hasRole }`);
    if (!hasRole) {
        return;
    }

    const sheetCommandBase = ctx.commands.get(CommandNames.SHEET);
    if (!sheetCommandBase) {
        return;
    }
    const sheetCommand = sheetCommandBase as ICommandSheet;
    const sheet = await PendingSheet.findOne({ approveMessageId: message.id });
    console.log(sheet);
    if (!sheet) {
        return;
    }

    const originalUser = await message.guild.members.fetch(sheet.userId);
    if (emoji.toString() === Reacts.APPROVE) {
        const status: boolean = await sheetCommand.approveSheet(ctx, sheet);
        if (status) {
            const otherReacts: Snowflake[] = message.reactions.cache
                .filter((react: MessageReaction) => react.emoji.toString() !== Reacts.APPROVE)
                .keyArray();
            otherReacts.forEach(key => {
                message.reactions.cache.get(key)?.remove();
            });
            if (originalUser) {
                await originalUser.user.send(`The sheet for your character **${ sheet.characterName }** has been approved!`);
            }
            await sheet.remove();
        }
    }

    if (emoji.toString() === Reacts.DENY) {
        const otherReacts: Snowflake[] = message.reactions.cache
            .filter((react: MessageReaction) => react.emoji.toString() !== Reacts.DENY)
            .keyArray();
        otherReacts.forEach(key => {
            message.reactions.cache.get(key)?.remove();
        });
        if (originalUser) {
            await originalUser.user.send(`The sheet for your character **${ sheet.characterName }** has been denied! If modifications need to be made, fix them and then resubmit`);
        }
        await sheet.remove();
    }
};

module.exports = async (ctx: IClientContext, messageReaction: MessageReaction, user: User): Promise<void> => {
    const result = await deleteClonkMessage(ctx, messageReaction, user).then().catch(err => logger.error(err));
    if (!result) {
        await changeVote(ctx, messageReaction, user).then().catch(err => logger.error(err));
        approveSheet(ctx, messageReaction, user).then().catch(err => logger.error(err));
    }
};
