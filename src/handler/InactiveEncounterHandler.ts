import Encounter, { IEncounter, IEncounterMember } from '../db/schema/encounter';
import moment, { Moment } from 'moment';
import logger from '../util/logging';
import { CommandNames, IClientContext } from '../classes/types/context';
import { TextChannel } from 'discord.js';
import ScheduledJobHandler from '../classes/base/ScheduledJobHandler';

class InactiveEncounterHandler extends ScheduledJobHandler {

    private readonly WARN_INTERVAL_MS = 1000 * 60 * 60 * 24 * 2;
    private readonly CANCEL_INTERVAL_MS = 1000 * 60 * 60 * 24 * 3;

    constructor() {
        super({ hour: 12, minute: 0, second: 0 }, 'Inactive Encounters');
    }

    async executeJob(context: IClientContext): Promise<void> {
        const encounters: IEncounter[] = await Encounter.find({});
        const endCommand = context.commands.get(CommandNames.END);

        let cleaned = 0;
        let notified = 0;
        logger.info('Beginning cleanup of encounters to make some more room');

        for (const encounter of encounters) {
            try {
                const guild = await context.client.guilds.fetch(encounter.guild);
                if (!guild) {
                    logger.info(`I don't think guild ${ encounter.guild } exists, removing the encounter and continuing...`);
                    await encounter.remove();
                    cleaned++;
                    continue;
                }
                const channel = guild.channels.cache.get(encounter.channel);
                if (!channel || !(channel instanceof TextChannel)) {
                    logger.info(`I don't think channel ${ encounter.guild } exists, removing the encounter and continuing...`);
                    await encounter.remove();
                    cleaned++;
                    continue;
                }

                const lastMessage: Moment = moment(encounter.lastMessage);
                const diff = moment().diff(lastMessage);
                if (diff < this.WARN_INTERVAL_MS) {
                    continue;
                }

                if (diff >= this.CANCEL_INTERVAL_MS && encounter.warned) {
                    const message = await channel.send(
                        `${ encounter.members.map((member: IEncounterMember) => `<@${ member.userId }>`).join(' ') }\n` +
                        'This channel has been inactive for 3 or more days and has been shut down.\n' +
                        'Feel free to resume your RP at any time in the future.'
                    );

                    await endCommand?.execute(context, message, [], CommandNames.END);
                    cleaned++;
                    continue;
                }

                if (diff >= this.WARN_INTERVAL_MS && !encounter.warned) {

                    encounter.warned = true;
                    await encounter.save();
                    notified++;

                    for (const member of encounter.members) {
                        try {
                            let guildMember;
                            try {
                                guildMember = await guild.members.fetch(member.userId);
                            } catch (ignored) {
                                continue;
                            }

                            if (!guildMember) {
                                continue;
                            }
                            await guildMember.user.send(
                                'Hi! This is an automated message about one of your RP Sessions!\n\n' +
                                `It looks like you're involved in a session in ${ guild.name }#${ channel.name } that's been inactive for more than 2 days.\n` +
                                'If the channel hasn\'t been used by Noon EST tomorrow, the session will be automatically stopped.\n' +
                                'If the channel closes, you\'ll still receive your RP for time spent playing but if you want to continue, you\'ll need to restart the encounter in the channel.\n' +
                                'Have a great day!'
                            );
                        } catch (er) {
                            logger.error(`Failed to send cleanup warning message to ${ member.userId }`);
                            logger.error(er);
                        }
                    }
                }

            } catch (e) {
                logger.error(`Failed to evaluate cleanup for encounter ${ encounter._id }`);
                logger.error(e);
            }
        }

        logger.info(`Cleaned ${ cleaned } encounters and notified closure for ${ notified }.`);
    }
}

export default InactiveEncounterHandler;