import { GuildChannel, Message, MessageEmbed, PartialMessage } from 'discord.js';
import logger from '../util/logging';
import { IClientContext, intenseBap, Reacts } from '../classes/types/context';
import { prefix } from '../config.json';
import { updateInactiveTime, updateLastMessage } from './clienthandler';
import { ClientContext } from './ClientContext';
import WatchedWord, { IWatchedWordUser, IWordPartial, PartialWordCache } from '../db/schema/watchedword';
import moment from 'moment';
import { capitalize } from '../util/utils';

const ignoreEmoji = [
    '\u{1F5A8}',
    Reacts.WEATHER,
    '\u{1F1E6}',
    Reacts.POLL,
    '\u{274C}',
    Reacts.CLIP,
];

export const parseWatchedWords = async (
    {
        client,
        words,
    }: IClientContext, message: Message | PartialMessage): Promise<Message | void> => {

    if (!client.user || message.deleted || !message.author) {
        return;
    }

    if (message.partial) {
        try {
            message = await message.fetch();
        } catch (err) {
            logger.error('Error fetching partial message');
            logger.error(err);
            return;
        }
    }

    if (message.deleted || !message.guild || message.author.bot || message.content.startsWith(prefix) || !message.content) {
        // logger.info('Message doesn\'t meet requirements for watched word');
        return;
    }
    const content = message.content.toLowerCase();
    const guildPartialMap: PartialWordCache | undefined = words.get(message.guild.id);
    if (!guildPartialMap) {
        return;
    }

    if (!guildPartialMap.watchedChannels.some(ch => ch === message.channel?.id)) {
        return;
    }
    // logger.info('Found channel to track words');
    const match: IWordPartial | undefined = guildPartialMap.words.find(word => {
        // logger.info(word.regex);
        return word.regex.test(content);
    });
    if (!match || !match.tracked) {
        return;
    }

    const word = await WatchedWord.findOne({
        guild: message.guild.id,
        word: match.aliasOf,
    });
    if (!word) {
        return;
    }

    const time = moment(word.lastTime);
    word.lastTime = moment().valueOf();
    word.count++;
    let wordUser: IWatchedWordUser | undefined = word.users.find((user: IWatchedWordUser) => {
        return user.userId === message.author?.id;
    });
    if (!wordUser) {
        wordUser = {
            count: 0,
            lastTime: word.lastTime.valueOf(),
            userId: message.author.id,
            username: message.author.username,
        };
        word.users.push(wordUser);
    }
    // message.channel.send(`\`\`\`${ JSON.stringify(word, null, 4) }\`\`\``.replace(/\\\\/g, '\\'));

    wordUser.username = message.author.username;
    wordUser.count++;

    const embed = new MessageEmbed();
    embed.setTitle(`${ capitalize(word.word) } count: ${ word.count }`)
        .setDescription(
            `Time since last mention: ${ time.fromNow() }\n` +
            `**${ message.author.username }** has mentioned _${ word.word }_ ${ wordUser.count } ${ wordUser.count === 1 ? 'time' : 'times' }`
        );

    try {
        await word.save();
    } catch (e) {
        logger.error(e);
        logger.info(`Failed to save word ${ word.word }`);
    }

    try {
        const dev = await message.guild.members.fetch('173923836054470656');
        if (dev && message.channel instanceof GuildChannel) {
            await dev.user.send('Someone activated your trap in ' + message.channel.name);
        }
    } catch (e) {
        // ignored
    }

    return message.channel.send(embed);
};

export const addReactions = async ({ client }: IClientContext, message: Message | PartialMessage): Promise<void> => {
    if (!client.user || message.deleted || !message.author) {
        return;
    }

    if (message.partial) {
        try {
            message = await message.fetch();
        } catch (err) {
            logger.error('Error fetching partial message');
            logger.error(err);
            return;
        }
    }

    // Handle trashcan and other emoji
    if (message.author.bot && message.author.id === client.user.id) {
        setTimeout(() => {
            if (!message || message.deleted || !message.channel) {
                return;
            }
            message.channel.messages.fetch(message.id)
                .then(async cached => {
                    if (!cached) {
                        return;
                    }
                    if (message.content?.startsWith('\u{0200b}') || message.content?.includes(intenseBap)) {
                        return;
                    }
                    // logger.info(cached.reactions.array().map(val => val.emoji.toString()));
                    if (cached.reactions.cache.find(reaction => reaction.emoji.toString() === Reacts.WEATHER)) {
                        logger.info('Cancelling trashcan emoji due to inclement weather');
                        return;
                    }
                    for (const ignored of ignoreEmoji) {
                        if (cached.reactions.cache.find(reaction => reaction.emoji.toString() === ignored)) {
                            return;
                        }
                    }

                    if (cached.reactions.cache.some(reaction => reaction.emoji.toString() === Reacts.APPROVE) &&
                        cached.reactions.cache.some(reaction => reaction.emoji.toString() === Reacts.DENY)) {
                        return;
                    }

                    await cached.react(Reacts.TRASHCAN);
                })
                .catch(err => logger.error('Trashcan emote skipped because message doesn\'t exist', err));
        }, 30000);
    }
};

export const parseCommands = async ({ client }: IClientContext, message: Message | PartialMessage): Promise<Message | void> => {
    // logger.info('Received message');
    if (!client.user || message.deleted || !message.author) {
        return;
    }

    if (message.partial) {
        try {
            message = await message.fetch();
        } catch (err) {
            logger.error('Error fetching partial message');
            logger.error(err);
            return;
        }
    }

    // Update inactive time and last message
    if (!message.deleted && message.guild && !message.author.bot) {
        // Allow for the info command to update the stuff
        if (!message.content.startsWith(prefix) || message.content.trim().toLowerCase() === '!info') {
            await updateInactiveTime(ClientContext, message);
            await updateLastMessage(ClientContext, message);
        }
    }

    // Make sure message still exists and parse in case it's a command
    if (message.deleted || !message.content.startsWith(prefix) || message.author.bot) return;
    const args: string[] = message.content.slice(prefix.length).split(/\s+/);
    const commandName = args.shift()?.toLowerCase() ?? '';
    const command = ClientContext.commands.get(commandName) || ClientContext.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) {
        return;
    }

    if (!message.guild && command.preventDM) {
        await message.channel.send('This command cannot be run in a DM!');
        return;
    }

    try {
        logger.info(`${ message.author.username } ran command ${ command.name }`);
        await command.execute(ClientContext, message, args, commandName);
    } catch (error) {
        logger.error(error);
        console.error(error);
        await message.reply(`An error occurred running the command! Use ${ prefix }help for a list of commands.`);
    }

    // Handle message timings for ongoing RP
};