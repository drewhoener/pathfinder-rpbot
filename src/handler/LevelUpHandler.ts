import ScheduledJobHandler from '../classes/base/ScheduledJobHandler';
import { IClientContext, IRawCharacterSheet } from '../classes/types/context';
import { MW_API_TIME, requestMythSheetsBulk } from '../util/mythweaversutil';
import { Guild, GuildMember, Role, Snowflake } from 'discord.js';
import logger from '../util/logging';
import { CharacterSheet, ICharacterSheet } from '../db/schema/charactersheets';

type DiscordSheetComposite = { userId: Snowflake; guildId: Snowflake } & IRawCharacterSheet;
type CharacterLevelMap = { [key: string]: DiscordSheetComposite[] };
type LevelMapEntry = [string, { xp: number; color: string }];

const LevelMap: { [key: string]: { xp: number; color: string } } = {
    'L1': { xp: 0, color: '#8b0000' },
    'L2': { xp: 2000, color: '#ff0000' },
    'L3': { xp: 5000, color: '#fa8072' },
    'L4': { xp: 9000, color: '#ffe4c4' },
    'L5': { xp: 15000, color: '#ff8c00' },
    'L6': { xp: 23000, color: '#ffd700' },
    'L7': { xp: 35000, color: '#808000' },
    'L8': { xp: 51000, color: '#adff2f' },
    'L9': { xp: 75000, color: '#3cb371' },
    'L10': { xp: 105000, color: '#00ff7f' },
    'L11': { xp: 155000, color: '#008080' },
    'L12': { xp: 220000, color: '#00ffff' },
    'L13': { xp: 315000, color: '#87cefa' },
    'L14': { xp: 445000, color: '#1e90ff' },
    'L15': { xp: 635000, color: '#191970' },
    'L16': { xp: 890000, color: '#0000ff' },
    'L17': { xp: 1300000, color: '#ba55d3' },
    'L18': { xp: 1800000, color: '#dda0dd' },
    'L19': { xp: 2550000, color: '#ff00ff' },
    'L20': { xp: 3600000, color: '#ff1493' },
};

const getEntries = (): LevelMapEntry[] => {
    return Object.entries(LevelMap).sort((o1, o2) => o2[1].xp - o1[1].xp);
};

class LevelUpHandler extends ScheduledJobHandler {

    constructor() {
        super({ hour: 4, minute: 0, second: 0 }, 'Level Up Manager');
    }

    async prepareRoles({ client }: IClientContext): Promise<void> {
        for (const guild of client.guilds.cache.array()) {
            try {
                for (const roleKey of Object.keys(LevelMap)) {
                    const roleManager = guild.roles;
                    // If the role exists in the server, don't create it
                    const foundRole = roleManager.cache.find(value => value.name === roleKey);
                    if (foundRole) {
                        continue;
                    }
                    await roleManager.create({
                        data: {
                            name: roleKey,
                            color: LevelMap[roleKey].color,
                            mentionable: true,
                        },
                        reason: `Clonk Auto-Levelling System (${ roleKey })`,
                    });
                }
            } catch (e) {
                // I assume it'd be an issue making roles on a guild
            }
        }
    }

    async getRolesForGuild(guild: Guild): Promise<Role[]> {
        const options = Object.keys(LevelMap);
        const allRoles = (await guild.roles.fetch()).cache;
        return [...allRoles.filter(role => options.includes(role.name)).values()];
    }

    getEntryFromXP(entries: LevelMapEntry[], xp: number): LevelMapEntry {
        for (let i = 0; i < entries.length; i++) {
            const entry = entries[i];
            if (xp >= entry[1].xp) {
                return entry;
            }
        }
        return entries[entries.length - 1];
    }

    async getOrFetchUser(guild: Guild, user: Snowflake): Promise<GuildMember> {
        if (guild.members.cache.has(user)) {
            const member = guild.members.cache.get(user);
            if (member) {
                return member;
            }
        }
        return guild.members.fetch(user);
    }

    async executeJob(context: IClientContext): Promise<void> {

        const approvedSheets: ICharacterSheet[] = await CharacterSheet.find({}).catch(() => []);
        if (!approvedSheets || !approvedSheets.length) {
            return;
        }

        const bulkCharacterSheetFunc = (): Promise<IRawCharacterSheet[]> => {
            return requestMythSheetsBulk(
                approvedSheets.map((sheet: ICharacterSheet) => sheet.mythweavers),
                MW_API_TIME * 1.5,
                10
            );
        };

        await this.prepareRoles(context);
        const entries = getEntries();

        const sheets: CharacterLevelMap = await (bulkCharacterSheetFunc().then(results => {
            if (!results || !results.length) {
                return {};
            }

            const map: CharacterLevelMap = {};

            // noinspection DuplicatedCode
            results.forEach(entry => {
                const nameEntry: ICharacterSheet | undefined = approvedSheets.find(elem => elem.mythweavers === entry.URL);
                if (!nameEntry) {
                    return;
                }

                const res: DiscordSheetComposite = {
                    userId: nameEntry.userId,
                    guildId: nameEntry.guild,
                    ...entry,
                    DiscordName: nameEntry.playerName,
                };

                if (!map.hasOwnProperty(nameEntry.userId)) {
                    map[nameEntry.userId] = [];
                }
                map[nameEntry.userId].push(res);
            });

            return map;
        }));


        if (!sheets || !Object.keys(sheets).length) {
            return;
        }

        // logger.info(JSON.stringify(sheets));

        // Map to an array of users and roles required

        for (const guild of context.client.guilds.cache.array()) {
            const keys = Object.keys(LevelMap);
            for (const sheetUser of Object.keys(sheets)) {
                try {
                    const guildMember = await guild.members.fetch(sheetUser);
                    const removable = guildMember.roles.cache.filter(role => keys.includes(role.name));
                    for (const role of removable.array()) {
                        try {
                            await guildMember.roles.remove(role);
                        } catch (er) {
                            // Continue
                        }
                    }
                } catch (e) {
                    // Just continue
                }
            }


            for (const roleKey of keys) {
                const foundRole = guild.roles.cache.find(search => search.name === roleKey);
                if (!foundRole) {
                    continue;
                }

                const members = foundRole.members.array();
                if (members.length) {
                    logger.info(`Removing members from role ${ foundRole.name }`);
                }
                for (const member of members) {
                    // Remove roles from every member
                    try {
                        await member.roles.remove(foundRole);
                        // eslint-disable-next-line no-empty
                    } catch (er) {
                    }
                }
            }
        }

        logger.info('Starting Iteration...');
        for (const value of Object.values(sheets)) {
            if (!value.length) {
                logger.info('No length');
                continue;
            }

            let guild: Guild | undefined;
            let guildMember: GuildMember | undefined;
            for (const sheetEntry of value) {

                try {
                    if (!guild || (guild && guild.id !== sheetEntry.guildId)) {
                        logger.warn('Fetching a different guild than the one we have cached');
                        guild = await context.client.guilds.fetch(sheetEntry.guildId);
                    }
                    if (!guildMember || (guildMember && guildMember.guild.id !== sheetEntry.guildId)) {
                        logger.warn(`Fetching user ${ sheetEntry.DiscordName } from another Guild.`);
                        guildMember = await this.getOrFetchUser(guild, sheetEntry.userId);
                    }

                } catch (e) {
                    continue;
                }

                if (!guild || !guildMember) {
                    logger.info('No guild or member');
                    continue;
                }
                const lfeRole = guild.roles.cache.find(role => role.name === 'LFE');
                if (lfeRole && !guildMember.roles.cache.has(lfeRole.id)) {
                    logger.info(`User ${ guildMember.displayName } does not have LFE, skipping`);
                    continue;
                }

                const xpInt = parseInt(sheetEntry.XPCurrent);
                if (isNaN(xpInt)) {
                    continue;
                }
                const roleInfo = this.getEntryFromXP(entries, xpInt);
                logger.info(`I think character ${ sheetEntry.Name } owned by ${ sheetEntry.DiscordName } is ${ JSON.stringify(roleInfo) }`);
                const levelRole = guild.roles.cache.find(role => role.name === roleInfo[0]);
                if (!levelRole) {
                    logger.info(`Couldn't find the role required for ${ JSON.stringify(roleInfo) }`);
                    continue;
                }
                if (guildMember.roles.cache.has(levelRole.id)) {
                    logger.info(`${ guildMember.displayName } already has the role [ ${ levelRole.name } ]`);
                    continue;
                }
                await guildMember.roles.add(levelRole);
            }
        }
        logger.info('Done');
    }
}

export default LevelUpHandler;