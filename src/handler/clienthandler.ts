import { CommandNames, IClientContext } from '../classes/types/context';
import { Message, TextChannel } from 'discord.js';
import Encounter, { IEncounter } from '../db/schema/encounter';
import moment, { duration, now } from 'moment';
import logger from '../util/logging';
import Poll, { IPoll } from '../db/schema/polls';
import { scheduleJob } from 'node-schedule';
import { fetchPollMessage } from '../util/utils';
import { ICommand } from '../classes/base/Command';
import { loadPartialCaches } from '../db/schema/watchedword';

export const updateInactiveTime = async (ctx: IClientContext, message: Message): Promise<void> => {
    if (message.deleted) {
        logger.warn('Message has been deleted, not updating inactive time');
        return;
    }
    try {
        if (!message.guild) {
            return;
        }
        const encounter: IEncounter | null = await Encounter.findOne({
            guild: message.guild.id,
            channel: message.channel.id,
            members: { $elemMatch: { userId: message.author.id, ignoreInResults: false } },
        });
        if (!encounter) {
            return;
        }
        // console.log(now() - encounter.lastMessage);
        const elapsedTime = duration(now() - encounter.lastMessage);
        // console.log(elapsedTime.asHours().toFixed(2));
        const inactiveHours = Math.abs(elapsedTime.asHours());
        if (inactiveHours < 1 || isNaN(inactiveHours)) {
            // console.log('inactive is less than 1');
            return;
        }
        logger.silly(`Hours to modify: ${ inactiveHours }`);
        logger.silly('Pre Data:');
        logger.silly(encounter.members);
        encounter.members.forEach(member => {
            member.inactiveHours += Math.floor(inactiveHours);
        });
        await encounter.save();
        logger.silly(`Modified hour mod for channel ${ message.channel.id } to ${ Math.floor(inactiveHours) }. Raw Data:`);
        logger.silly(encounter.members);
    } catch (err) {
        logger.warn(`Failed to update inactiveTime for encounter in channel ${ message.channel.id } / ${ (message.channel as TextChannel).name }`);
        logger.error(err);
    }
};

export const updateLastMessage = async (ctx: IClientContext, message: Message): Promise<void> => {
    if (message.deleted) {
        logger.warn('Message has been deleted, not updating last message');
        return;
    }
    // logger.info('Updating last message');
    try {
        if (!message.guild) {
            return;
        }
        const encounter: IEncounter | null = await Encounter.findOne({
            guild: message.guild.id,
            channel: message.channel.id,
            members: { $elemMatch: { userId: message.author.id, ignoreInResults: false } },
        });
        if (!encounter) {
            return;
        }
        encounter.lastMessage = now().valueOf();
        if (encounter.warned) {
            encounter.warned = false;
        }
        await encounter.save();
    } catch (err) {
        logger.warn(`Failed to update lastMessage for encounter in channel ${ message.channel.id } / ${ (message.channel as TextChannel).name }`);
        logger.error(err);
    }
};

export const loadPolls = async (ctx: IClientContext): Promise<void> => {
    const polls: IPoll[] | null = await Poll.find({ pollId: { '$exists': true } });
    for (const poll of polls) {
        const endTime = moment(poll.end);
        logger.info(`Scheduling poll end for ${ poll.question } for ${ endTime.calendar() }`);
        scheduleJob(endTime.toDate(), async () => {
            const pollMessage = await fetchPollMessage(ctx.client, poll.guild, poll.channel, poll.messageId, poll.pollId);
            if (!pollMessage) {
                return;
            }

            const pollCommand: ICommand | undefined = ctx.commands.get(CommandNames.ENDPOLL);
            await pollCommand?.execute(ctx, pollMessage, [poll.pollId], CommandNames.ENDPOLL);
        });
    }
};

export type WordLoadResult = {
    guildsLoaded: number;
    wordsLoaded: number;
};

export const loadWatchedWords = async (ctx: IClientContext): Promise<WordLoadResult> => {
    const wordsCount = await loadPartialCaches(ctx);
    const guildsCount = ctx.words.keyArray().length;

    logger.info('Assembled collection');
    // @ts-ignore
    // logger.info(JSON.stringify(Array.from(ctx.words.get('446704418600124427').words), null, 4));
    return {
        wordsLoaded: wordsCount,
        guildsLoaded: guildsCount,
    };
};