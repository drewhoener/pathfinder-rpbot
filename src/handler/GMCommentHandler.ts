import {
    AwaitMessagesOptions,
    Collection,
    CollectorFilter,
    DMChannel,
    Guild,
    GuildMember,
    Message,
    MessageCollector,
    MessageCollectorOptions,
    MessageEmbed,
    MessageReaction,
    ReactionCollector,
    Snowflake,
    TextChannel,
    User,
} from 'discord.js';
import util from 'util';
import { IHistoryEntry } from '../db/schema/history';
import { ObjectId } from 'mongodb';
import { ITimeResult } from '../db/schema/encounter';
import { codeBlock, sleep } from '../util/utils';
import { Reacts } from '../classes/types/context';
import { EventEmitter } from 'events';
import logger from '../util/logging';
import { blankEmbed } from '../util/embeds';
import Review, { Enjoyment, ISurveyResults, YesNoEmoji } from '../db/schema/review';

export interface IGMCommentHandler {
    historyId: ObjectId | undefined;
    user: ITimeResult;

    start(): Promise<void>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type CollectorFunction = (...args: any[]) => void;
type StoredCollector = ReactionCollector | MessageCollector;
type GMInfo = { id: Snowflake; name: string };

enum SurveyStep {
    SESSION_DESCRIPTION,
    SESSION_RUN_ENJOYMENT,
    SESSION_CONTENT_ENJOYMENT,
    WANT_SESSION_FOLLOWUP,
    WANT_SESSION_FOLLOWUP_WRITE,
    SPECIFIC_FOLLOW_UP,
    LIKED_DISLIKED_CONTENT,
    COMMENTS,
    ANONYMOUS,
    FORCE
}

const enjoymentNumberCodes = {
    [Enjoyment.LOVE]: 'Love',
    [Enjoyment.LIKE]: 'Like',
    [Enjoyment.NEUTRAL]: 'Neutral',
    [Enjoyment.DISLIKE]: 'Dislike',
    [Enjoyment.HATE]: 'Hate',
};

class GMCommentHandler implements IGMCommentHandler {

    historyId: ObjectId | undefined;
    user: ITimeResult;
    private gm: GMInfo;
    private guildMember: GuildMember;
    private messageChannel?: DMChannel;
    private beginCollector?: MessageCollector;
    // 5 minutes
    private beginTimeout = 1000 * 60 * 5;
    // 30 minutes
    private halfHour = 1000 * 60 * 30;
    // 45 mins
    private totalTime = 1000 * 60 * 45;
    // private totalTime = 1000;

    private yesNoRegex = /yes|no|[yn]/mi;
    private startMessage = [
        '**Clink Clank!**',
        '',
        'It seems like you were recently involved in an encounter or quest that ran for a bit. If it\'s not too much trouble, the GM that ran your game would appreciate your feedback.',
        'If you were in an encounter that wasn\'t long enough to give feedback you can ignore this, but it\'s as easy as clicking a few reactions and leaving a short comment.',
        '',
        'I\'ll be around for the next **5 minutes** if you need a second, but if you wait too long the opportunity to give your opinion won\'t be available',
        '_(In the future I hope to be able to let you give feedback on any encounter)_',
        '',
        '**GM Verification**',
        '**------------------**',
        'I think your GM is **__%s__**. If this is not your GM, please don\'t answer this survey, just type **no**',
        '',
        '**Data Anonymity & Privacy**',
        '**------------------**',
        'Your feedback will be completely anonymous (unless you choose to give your name)',
        '__HOWEVER__: If you use this feature to leave hateful comments or needlessly harass the people that run your sessions, the original data from these reviews _can_ be looked up by a Clonk developer.',
        'Feedback is sent directly to your GM and is _**not accessible**_ by any other server members (staff or otherwise) unless that GM decides to share it',
        '',
        '**Leave Feedback: [ Y/N ]**',
    ].join('\n');
    private timeoutMessage = 'It seems I\'ve run out of time to accept feedback, sorry!';
    private rejectMessage = 'If you still have something you want to tell your GM about their session, you\'re more than welcome to message them personally.\nHave a great day!';
    private selfdestruct = false;
    private selfDestructPID?: NodeJS.Timeout;
    private currentStep: SurveyStep = SurveyStep.SESSION_RUN_ENJOYMENT;
    private lockingBus: EventEmitter = new EventEmitter();
    private response: ISurveyResults = {
        anonymous: true,
        gmId: '',
        userId: '',
        username: '',
        description: 'No Description Given',
        sessionRunningEnjoyment: Enjoyment.UNANSWERED,
        contentEnjoyment: Enjoyment.UNANSWERED,
        wantFollowUp: 'Question Not Answered',
        followUpComments: 'N/A',
        likedDisliked: 'N/A',
        comments: 'N/A',
    }
    private collectors: StoredCollector[] = [];

    constructor(encounterHistory: IHistoryEntry | null, user: ITimeResult, guild: Guild | null, gm: GMInfo) {
        if (!guild) {
            throw new Error('Guild must not be null');
        }
        const member: GuildMember | undefined = guild.members.cache.get(user.userId);
        if (!member) {
            throw new Error('Guild Member couldn\'t be found, can\'t send a follow up');
        }
        this.user = user;
        if (encounterHistory) {
            this.historyId = encounterHistory._id;
        }
        this.gm = gm;
        this.response.gmId = this.gm.id;
        this.guildMember = member;
        this.response.userId = user.userId;
        this.response.username = this.guildMember.displayName;
    }

    getOptions(time?: number, max?: number, errors?: string[]): AwaitMessagesOptions {
        return {
            time,
            max: max ?? 1,
            errors,
        };
    }

    async start(): Promise<void> {
        this.messageChannel = await this.guildMember.createDM();
        this.beginCollector = this.messageChannel.createMessageCollector(this.isYesNo, this.getOptions(this.beginTimeout, 10, ['time']));
        await this.messageChannel.send(util.format(this.startMessage, this.gm.name))
            .then(m => m.react(Reacts.CLIP));
        this.beginCollector.on('collect', (message: Message) => {
            if (/no|n/mi.test(message.content)) {
                this.messageChannel?.send(this.rejectMessage);
                this.beginCollector?.stop('no');
                return;
            }
            this.beginCollector?.stop('valid');
        });
        this.beginCollector.on('end', (messages: Collection<string, Message>, reason) => {
            const value = messages.first();
            if (!value || reason !== 'valid') {
                if (reason !== 'no') {
                    this.messageChannel?.send(this.timeoutMessage);
                    return this.messageChannel?.send(this.rejectMessage);
                }
                return;
            }
            return this.startFeedbackSurvey();
        });
        return;
    }

    startSelfdestruct(): NodeJS.Timeout {
        return setTimeout(() => {
            this.selfdestruct = true;
            this.unlockBus(SurveyStep.FORCE);
        }, this.totalTime);
    }

    unlockBus(step: SurveyStep): void {
        if (step === this.currentStep || step === SurveyStep.FORCE) {
            this.lockingBus.emit('unlock');
        }
    }

    async advance(sleepTime = 500): Promise<unknown> {
        if (this.selfdestruct) {
            return Promise.resolve();
        }
        return new Promise(resolve => this.lockingBus.once('unlock', () => {
            setTimeout(resolve, sleepTime);
        }));
    }

    async startFeedbackSurvey(): Promise<void> {
        if (!this.messageChannel) {
            return;
        }
        await this.messageChannel.send('Thanks for agreeing to participate! You have 45 minutes to finish this _(but it should only take 5)_');
        await sleep(1000);
        this.selfDestructPID = this.startSelfdestruct();
        // ---------------------------------------

        this.currentStep = SurveyStep.SESSION_DESCRIPTION;
        let embed = blankEmbed().setColor('#33bb33')
            .setTitle('Please provide a brief description of your session, either the name of the quest or a session detail the GM would recognize')
            .setDescription('When you\'re done, type **done**');
        await this.messageChannel.send(embed);
        const descriptionCollector = this.createQuickMessageCollector(
            this.messageChannel,
            (m: Message): boolean => m.author.id === this.guildMember.id,
            undefined,
            (m: Message) => {
                const content = m.content.replace(/[*_~|`'">]/mig, '').trim().toLowerCase();
                if (content === 'done') {
                    descriptionCollector.stop('finish');
                }
            },
            (collection: Collection<Snowflake, Message>) => {
                const message = collection.array().join('\n').trim();
                this.response.description = message === '' ? this.response.description : message;
                this.unlockBus(SurveyStep.SESSION_DESCRIPTION);
            }
        );
        await this.advance();
        // ---------------------------------------

        const questionGMEnjoyment: Message = await this.createGenericRankedQuestion(this.messageChannel, 'How did you like the way the GM ran the session?');
        this.createGenericReactCollector(
            questionGMEnjoyment,
            (react: MessageReaction) => this.response.sessionRunningEnjoyment = react.emoji.name as Enjoyment,
            undefined,
            SurveyStep.SESSION_RUN_ENJOYMENT
        );
        await this.advance();
        // ---------------------------------------

        const questionContentEnjoyment: Message = await this.createGenericRankedQuestion(this.messageChannel, 'How much did you enjoy the session\'s content?');
        this.createGenericReactCollector(
            questionContentEnjoyment,
            (react: MessageReaction) => this.response.contentEnjoyment = react.emoji.name as Enjoyment,
            undefined,
            SurveyStep.SESSION_CONTENT_ENJOYMENT
        );
        await this.advance();
        // ---------------------------------------

        const questionWantFollowUp: Message = await this.createRankedQuestion(
            this.messageChannel,
            'Would you be interested in a follow up quest or encounter?',
            Object.values(YesNoEmoji),
            undefined,
            `${ YesNoEmoji.YES } Yes\n\n${ YesNoEmoji.NO } No\n\n${ YesNoEmoji.WRITE } I'd rather write a response`
        );
        this.createGenericReactCollector(
            questionWantFollowUp,
            async (react: MessageReaction, user: User) => {
                const choice = react.emoji.name as YesNoEmoji;
                let str = react.emoji.name;
                if (choice === YesNoEmoji.WRITE && this.messageChannel) {
                    this.currentStep = SurveyStep.WANT_SESSION_FOLLOWUP_WRITE;
                    // ----------------
                    await this.messageChannel.send('Write your response below. When you\'re finished, just type **done**');
                    const questionFollowUpCollector = this.createQuickMessageCollector(
                        this.messageChannel,
                        (m: Message): boolean => m.author.id === user.id,
                        undefined,
                        (m: Message) => {
                            const content = m.content.replace(/[*_~|`'">]/mig, '').trim().toLowerCase();
                            if (content === 'done') {
                                questionFollowUpCollector.stop('finish');
                            }
                        },
                        (collection: Collection<Snowflake, Message>) => {
                            const message = collection.array().join('\n').trim();
                            str = message === '' ? 'N/A' : message;
                            this.unlockBus(SurveyStep.WANT_SESSION_FOLLOWUP_WRITE);
                        }
                    );
                    await this.advance(1);
                    this.currentStep = SurveyStep.WANT_SESSION_FOLLOWUP;
                }
                this.response.wantFollowUp = str;
            },
            Object.values(YesNoEmoji),
            SurveyStep.WANT_SESSION_FOLLOWUP
        );
        await this.advance();
        // ---------------------------------------

        this.currentStep = SurveyStep.SPECIFIC_FOLLOW_UP;
        embed = blankEmbed().setColor('#33bb33')
            .setTitle('If there are any specific things you\'d like to see in a potential follow up, write them now')
            .setDescription('When you\'re done, type **done**');
        await this.messageChannel.send(embed);
        const followUpCommentCollector = this.createQuickMessageCollector(
            this.messageChannel,
            (m: Message): boolean => m.author.id === this.guildMember.id,
            undefined,
            (m: Message) => {
                const content = m.content.replace(/[*_~|`'">]/mig, '').trim().toLowerCase();
                if (content === 'done') {
                    followUpCommentCollector.stop('finish');
                }
            },
            (collection: Collection<Snowflake, Message>) => {
                const message = collection.array().join('\n').trim();
                this.response.followUpComments = message === '' ? this.response.followUpComments : message;
                this.unlockBus(SurveyStep.SPECIFIC_FOLLOW_UP);
            }
        );
        await this.advance();
        // ---------------------------------------

        this.currentStep = SurveyStep.LIKED_DISLIKED_CONTENT;
        embed = blankEmbed().setColor('#33bb33')
            .setTitle('If there was anything you specifically liked or disliked, let the GM know!\nThis includes things you\'d like to see more (or less) of in the future!')
            .setDescription('When you\'re done, type **done**');
        await this.messageChannel.send(embed);
        const likedCommentCollector = this.createQuickMessageCollector(
            this.messageChannel,
            (m: Message): boolean => m.author.id === this.guildMember.id,
            1000 * 60 * 5,
            (m: Message) => {
                const content = m.content.replace(/[*_~|`'">]/mig, '').trim().toLowerCase();
                if (content === 'done') {
                    likedCommentCollector.stop('finish');
                }
            },
            (collection: Collection<Snowflake, Message>) => {
                const message = collection.array().join('\n').trim();
                this.response.likedDisliked = message === '' ? this.response.likedDisliked : message;
                this.unlockBus(SurveyStep.LIKED_DISLIKED_CONTENT);
            }
        );
        await this.advance();
        // ---------------------------------------

        this.currentStep = SurveyStep.COMMENTS;
        embed = blankEmbed().setColor('#33bb33')
            .setTitle('Any additional comments for the GM?')
            .setDescription('When you\'re done, type **done**');
        await this.messageChannel.send(embed);
        const generalCommentCollector = this.createQuickMessageCollector(
            this.messageChannel,
            (m: Message): boolean => m.author.id === this.guildMember.id,
            1000 * 60 * 5,
            (m: Message) => {
                const content = m.content.replace(/[*_~|`'">]/mig, '').trim().toLowerCase();
                if (content === 'done') {
                    generalCommentCollector.stop('finish');
                }
            },
            (collection: Collection<Snowflake, Message>) => {
                const message = collection.array().join('\n').trim();
                this.response.comments = message === '' ? this.response.comments : message;
                this.unlockBus(SurveyStep.COMMENTS);
            }
        );
        await this.advance();
        // ---------------------------------------

        this.currentStep = SurveyStep.ANONYMOUS;
        const questionAnonymous: Message = await this.createRankedQuestion(
            this.messageChannel,
            'Would you like to remain anonymous?',
            [YesNoEmoji.YES, YesNoEmoji.NO],
            undefined,
            `${ YesNoEmoji.YES } Yes (Your name will not be sent)\n\n${ YesNoEmoji.NO } No (Your name will be sent)`
        );
        this.createGenericReactCollector(
            questionAnonymous,
            (react: MessageReaction) => this.response.anonymous = (react.emoji.name as YesNoEmoji) === YesNoEmoji.YES,
            undefined,
            SurveyStep.ANONYMOUS
        );
        await this.advance();
        // ---------------------------------------

        clearTimeout(this.selfDestructPID);
        this.collectors.forEach(x => x.stop());
        // Just wait for collectors to finish up...
        await sleep(1000);
        if (this.selfdestruct) {
            await this.messageChannel.send('Oops! Looks like I\'ve run out of time, but you\'re more than welcome to message your GM directly!');
        } else {
            await this.messageChannel.send('Thanks for taking the survey :heart:\nI\'ll send this off right away, I\'m sure your GM appreciates your feedback!');
            logger.info(JSON.stringify(this.response, null, 4));
            logger.info('Done');
            await sleep(1000);
            this.sendFollowUp().then();
        }
        return;
    }

    async sendFollowUp(): Promise<Message[] | undefined> {
        if (!this.messageChannel) {
            return;
        }
        const surveyResult = new Review(this.response);
        try {
            await surveyResult.save();
        } catch (err) {
            logger.error(err);
            return Promise.all([
                this.messageChannel.send('Couldn\'t save the data from your review! If you still want to, you should contact your GM directly.'),
            ]);
        }
        const embed = new MessageEmbed();
        embed.setTitle(`Session Feedback (Author: ${ this.response.anonymous ? 'Anonymous' : this.response.username })`)
            .setDescription(`Session Description:\n${ this.response.description }`)
            .addField('**How did you like the way the GM ran the session?**', this.response.sessionRunningEnjoyment, true)
            .addField('**How much did you enjoy the session\'s content?**', this.response.contentEnjoyment, true)
            .addField('**Would you be interested in a follow up quest or encounter?**', this.response.wantFollowUp, this.response.wantFollowUp === YesNoEmoji.YES || this.response.wantFollowUp === YesNoEmoji.NO)
            .addField('**Is there anything specific you\'d want to see in a potential follow up?**', this.response.followUpComments)
            .addField('**If there was anything you specifically liked or disliked, let the GM know!\nThis includes things you\'d like to see more (or less) of in the future!**', this.response.likedDisliked, false)
            .addField('**Additional Comments**', this.response.comments, false)
            .setFooter(`Survey UUID: ${ surveyResult._id }`);
        return Promise.all([
            this.messageChannel.send(embed).then(async m => {
                await m.react(Reacts.CLIP);
                return m;
            }),
            this.messageChannel.send('\u{0200b}`                     `'),
            this.messageChannel.send(`\u{0200b}**Review From**\n${ codeBlock(this.response.anonymous ? 'Anonymous' : this.response.username) }`),
            this.messageChannel.send(`\u{0200b}**Session Description**\n${ codeBlock(this.response.description) }`),
            this.messageChannel.send(`\u{0200b}**How did you like the way the GM ran the session?**    ${ this.response.sessionRunningEnjoyment }`),
            this.messageChannel.send(`\u{0200b}**How much did you enjoy the session's content?**    ${ this.response.contentEnjoyment }`),
            this.messageChannel.send(util.format('\u{0200b}**Would you be interested in a follow up quest or encounter?**%s',
                (this.response.wantFollowUp === YesNoEmoji.YES || this.response.wantFollowUp === YesNoEmoji.NO) ?
                    `    ${ this.response.wantFollowUp }` : `\n${ this.response.wantFollowUp }`
            )),
            this.messageChannel.send(`\u{0200b}**If there was anything you specifically liked or disliked, let the GM know!\nThis includes things you'd like to see more (or less) of in the future!**\n${ codeBlock(this.response.likedDisliked) }`),
            this.messageChannel.send(`\u{0200b}**Additional Comments**\n${ codeBlock(this.response.comments) }`),
            this.messageChannel.send('\u{0200b}`                     `'),
        ]);
    }

    async createGenericRankedQuestion(channel: TextChannel | DMChannel, question: string, descriptionAddon?: string): Promise<Message> {

        return this.createRankedQuestion(
            channel,
            question,
            [
                Enjoyment.LOVE,
                Enjoyment.LIKE,
                Enjoyment.NEUTRAL,
                Enjoyment.DISLIKE,
                Enjoyment.HATE,
            ],
            undefined,
            descriptionAddon
        );
    }

    async createRankedQuestion(channel: TextChannel | DMChannel, question: string, reacts: string[], color = '#33bb33', descriptionAddon = ''): Promise<Message> {
        const embed = new MessageEmbed()
            .setColor(color)
            .setTitle(question)
            .setDescription(`Please select a reaction\nIf you change your mind, deselect your react before choosing again.\n\n${ descriptionAddon ?? '' }`);
        const message = await channel.send(embed);

        await message.react(Reacts.CLIP);
        for (const react of reacts) {
            try {
                await message.react(react);
            } catch (e) {
                logger.error(e);
            }
        }

        return message;
    }

    createQuickMessageCollector(channel: TextChannel | DMChannel, filter: CollectorFilter, time: number = (1000 * 60 * 2), onCollect: CollectorFunction, onEnd: CollectorFunction): MessageCollector {
        const collector = channel.createMessageCollector(filter, { time });
        collector.on('collect', async (message) => {
            if (message.author.bot) {
                collector.dispose(message);
                return;
            }
            await onCollect(message);
        });
        collector.on('end', (collected, reason) => {
            const last = collected.lastKey();
            if (collected.size >= 1 && last) {
                collected.delete(last);
            }
            onEnd(collected, reason);
        });
        return collector;
    }

    createMessageCollector(channel: TextChannel | DMChannel, filter: CollectorFilter, options: MessageCollectorOptions, onCollect: CollectorFunction, onEnd?: CollectorFunction): MessageCollector {
        const collector = channel.createMessageCollector(filter, options);
        collector.on('collect', async (message) => {
            if (message.author.bot) {
                collector.dispose(message);
                return;
            }
            await onCollect(message);
        });
        if (onEnd) {
            collector.on('end', onEnd);
        }
        this.collectors.push(collector);
        return collector;
    }

    createGenericReactCollector(message: Message, onCollect: CollectorFunction, matches: string[] = Object.values(Enjoyment), step: SurveyStep): ReactionCollector {

        this.currentStep = step;

        return this.createReactCollector(
            message,
            (react: MessageReaction) => matches.some(type => react.emoji.name === type),
            { time: this.halfHour },
            async (react: MessageReaction, user: User) => {
                await onCollect(react, user);
                // logger.info(JSON.stringify(this.response), null, 4);
                this.unlockBus(step);
            },
            undefined,
        );
    }

    createReactCollector(message: Message, filter: CollectorFilter, options: MessageCollectorOptions, onCollect: CollectorFunction, onEnd?: CollectorFunction): ReactionCollector {
        const collector = message.createReactionCollector(filter, options);
        collector.on('collect', async (react, user) => {
            if (user.bot) {
                return;
            }
            await onCollect(react, user);
        });
        if (onEnd) {
            collector.on('end', onEnd);
        }
        this.collectors.push(collector);
        return collector;
    }

    private isYesNo = (m: Message): boolean => this.yesNoRegex.test(m.content);
}

export default GMCommentHandler;