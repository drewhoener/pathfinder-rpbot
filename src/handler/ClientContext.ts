import { CommandNames, IClientContext } from '../classes/types/context';
import { Client, Collection, Snowflake } from 'discord.js';
import Command from '../classes/base/Command';
import ChannelLogger from '../util/channellogger';
import { PartialWordCache } from '../db/schema/watchedword';
import BotConfig from '../classes/types/BotConfig';

export const ClientContext: IClientContext = {
    client: new Client({
        partials: [
            'MESSAGE',
            'CHANNEL',
            'REACTION',
            'GUILD_MEMBER',
        ],
        ws: {
            intents: [
                'GUILDS',
                'GUILD_EMOJIS',
                'GUILD_INTEGRATIONS',
                'GUILD_MEMBERS',
                'GUILD_MESSAGE_REACTIONS',
                'GUILD_MESSAGE_TYPING',
                'GUILD_MESSAGES',
                'DIRECT_MESSAGES',
                'DIRECT_MESSAGE_REACTIONS',
                'DIRECT_MESSAGE_TYPING',
            ],
        },
    }),
    commands: new Collection<CommandNames, Command>(),
    words: new Collection<Snowflake, PartialWordCache>(),
    logger: new ChannelLogger(),
    config: new BotConfig(),
};